var fs = require('fs');
var https = require('https');

// sudo openssl x509 -req -in server-csr.pem -signkey server-key.pem -out server-cert.pem
var privateKey = fs.readFileSync('ssl/www.db-mms.com.key', 'utf8');
var certificate = fs.readFileSync('ssl/www.db-mms.com.crt', 'utf8');
var credentials = {
    key: privateKey,
    cert: certificate
};
var express = require('express');
var rest = express();
var async = require('async');

// your express configuration here
var httpsServer = https.createServer(credentials, rest);


var db = require('./db');
var _ = require('underscore');

////////////////////////////
///////////////////////// //
//                     // //
// Rest configuration  // //
//                     // //
///////////////////////// //
////////////////////////////

rest.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "https://www.db-mms.com");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", 'GET,PUT,POST,DELETE');
    next();
});
var bodyParser = require('body-parser');
rest.use(bodyParser.urlencoded({
    extended: true
}));
rest.use(bodyParser.json());



/////////////////////////////////////////////////////
////////////////////////////////////////////////// //
/////////////////////////////////////////////// // //
//                                           // // //
// System notification rest api              // // //
//                                           // // //
/////////////////////////////////////////////// // //
////////////////////////////////////////////////// //
/////////////////////////////////////////////////////



/**
 * Get sysnotes
 */
rest.get('/api/sysnotes', function(req, res) {

    console.log('Get system notifications', req.query);

    var query = {
        $or: [{
            visible_in_all_schools: true
        }, {
            visible_in_schools: req.query.school
        }],
        visible_to: {
            '$in': req.query.roles
        },
        published: true
    };

    if (req.query.modified) {
        query.modified = {
            '$gt': parseFloat(req.query.modified)
        };
    }

    // Ignore other queries if uid is provided
    if (req.query.uid) {
        query = {
            uid: req.query.uid
        };
    }

    db.sysnotes.find(query).sort({
        modified: -1
    }, function(err, docs) {
        console.log('response', err, docs);
        res.end(JSON.stringify(docs));
    });
});

/**
 * Save a new  sysnote
 */
rest.post('/api/sysnotes', function(req, res) {

    console.log('Create system notification');
    console.log(req.body);

    // Schema
    // Schema
    var sysnote = {
        uid: req.body.uid,
        username: req.body.username,
        subject: req.body.subject,
        type: req.body.type || 'sysnote',
        message: req.body.message,
        message_type: req.body.message_type,
        created: req.body.created || new Date().getTime(),
        modified: req.body.created || new Date().getTime(),
        visible_to: (req.body.visible_to) ? req.body.visible_to : [],
        visible_in_all_schools: req.body.visible_in_all_schools,
        visible_in_schools: (req.body.visible_in_schools) ? req.body.visible_in_schools : [],
        until: req.body.until,
        published: req.body.published
    };

    db.sysnotes.insert(sysnote, function(err, doc) {
        console.log('inserted', err, doc);
        res.end(JSON.stringify(doc));
    });
});

/**
 * Read a single sysnote
 */
rest.get('/api/sysnotes/:id', function(req, res) {
    var id = req.params.id;
    console.log('fetching sysnote: ', id);
    db.sysnotes.findOne({
        _id: db.ObjectId(id)
    }, function(err, doc) {
        res.end(JSON.stringify(doc));
        // doc._id.toString() === '523209c4561c640000000001'
    });
});

/**
 * Update a single sysnote
 */
rest.put('/api/sysnotes/:id', function(req, res) {
    var id = req.params.id;

    // Schema
    var sysnote = {
        uid: req.body.uid,
        username: req.body.username,
        subject: req.body.subject,
        type: req.body.type || 'sysnote',
        message: req.body.message,
        message_type: req.body.message_type,
        created: req.body.created || new Date().getTime(),
        modified: new Date().getTime(),
        visible_to: (req.body.visible_to) ? req.body.visible_to : [],
        visible_in_all_schools: req.body.visible_in_all_schools,
        visible_in_schools: (req.body.visible_in_schools) ? req.body.visible_in_schools : [],
        until: req.body.until,
        published: req.body.published
    };


    console.log('updating sysnote: ', id, sysnote);

    db.sysnotes.update({
        _id: db.ObjectId(id)
    }, sysnote, {
        upsert: true /*create a new doc if not found*/
    }, function(err, doc) {
        console.log('updated', err, sysnote, doc);

        // give the new id in upserted
        if (doc.upserted)
            sysnote._id = doc.upserted;

        res.end(JSON.stringify(sysnote));
    });
});


/**
 * Read a single sysnote
 */
rest.delete('/api/sysnotes/:id', function(req, res) {
    var id = req.params.id;
    db.sysnotes.remove({
        _id: db.ObjectId(id)
    }, function(err, doc) {
        var response = (err) ? err : 'true'
        res.end(response);
    });
});



/////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////// //
//                                                          // //
// Birthday cards  REST API                                 // //
//                                                          // //
////////////////////////////////////////////////////////////// //
/////////////////////////////////////////////////////////////////

/**
 * Query directories by school and/or received card
 */
rest.get('/api/bdaycards_directories', function(req, res) {

    var year = req.query.year;
    var schools = req.query.schools;

    var query = {};

    query = {
        student_in: {
            '$in': schools
        },
        // year: {
        //     $nin: [year]
        // },
    };

    console.log('QUERY:', req.query);

    var output = [];
    async.each(schools, function(school, callback) {
        db.directories.find({
            student_in: {
                '$in': [school]
            }
        }).sort({}, function(err, docs) {

            var until = new Date(parseInt(req.query.until));
            var from = new Date(parseInt(req.query.from));
            _.each(docs, function(student) {
                var dob = new Date(new Date(student.dob).setYear(until.getFullYear()));
                if (dob.getTime() <= until.getTime() && dob.getTime() > from.getTime()) {
                    output.push(student);
                }
            });

            callback();
        });
    }, function(err) {
        res.end(JSON.stringify(output));
    });


});

/////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////// //
//                                                          // //
// Statistics                                               // //
//                                                          // //
////////////////////////////////////////////////////////////// //
/////////////////////////////////////////////////////////////////


var lzwCompress = require('lzwcompress');
require('datejs');

/**
 * Get all statistics from all schools (inlc. caching and merging)
 * 
 * @param  {Object}   req   Request parameters
 * @return {Function} res   Response callback
 */
rest.get('/api/all_school_statistics', function(req, res) {

    var offset = req.param('offset') || 0;
    var nowYmd = Date.today().moveToFirstDayOfMonth().toString("yyyyMM dd").replace(' ', '');
    var snapshotDate = req.param('snapshotDate') || nowYmd;
    var dates = [parseInt(snapshotDate)];

    var last = Date.parseExact(snapshotDate, 'yyyyMMdd');
    for (var i = 0; i <= offset; i++) {
        var d = last.moveToFirstDayOfMonth().add(-1).months();
        dates.push(parseInt(d.toString("yyyyMM dd").replace(' ', '')));
    }


    db.total_statistics.find({ snapshotDate: { "$in": dates } }).sort({
        snapshotDate: -1
    }, function(err, docs) {
        console.log('total statistics response. errors:', err);
        res.end(JSON.stringify(docs));
    });

});


// Start listening
httpsServer.listen(8090);
// rest.listen(8090);
