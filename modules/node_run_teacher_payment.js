/**
 * Module to run teacher payments for all schools in MMS.
 * Used by grunt as tasks called by teacherpayments:run
 */
module.exports = function(options, tpCallback) {

    options = options || {};
    var _ = require('underscore');
    var Backbone = require('backbone');
    var async = require('async');
    var Schools = require('../collections/Schools');
    var db = require('../db');
    var log = options.log || function(msg) {
        console.log(msg);
    };

    var mmsSchools = new Schools();

    var runTp = function(school,sCallback) {
        log('========================================================================')
        log('Start teacher payments for: ' + school.get('title') + '[' + school.id + ']');
        school.getActiveTeachers(function(err, teachers) {

            log('Fetched teachers for: ' + school.get('title') + '[' + school.id + ']');
            async.eachLimit(teachers.models, 1, function(teacher, tCallback) {

                log('Requesting teacher payment for: ' + teacher.get('title') + '[' + teacher.id + ']');
                teacher.runTeacherPayment({
                    mms_nid: school.id
                }, function(err, data) {

                    log('Teacher payment response for: ' + teacher.get('title') + '[' + teacher.id + ']', {
                        error: err,
                        data: data
                    });

                    tCallback();
                });
            }, function() {
                log('Finished all teacher payments for: ' + school.get('title') + '[' + school.id + ']');
                sCallback();
            })
        })
    }

    mmsSchools.getSchoolNodes(function(schools) {

        log('Fetched school nodes', {
            schools: schools.length
        });

        var selectedSchool = mmsSchools.get(options.school)
        if (selectedSchool) {
            runTp(selectedSchool, function() {
                if (tpCallback) tpCallback()
            });
            return;
        } else {
            async.eachLimit(mmsSchools.models, 1, function(school, sCallback) {
                runTp(school,sCallback);

            }, function() {
                log('Finished all teacher payments for all schools');
                if (tpCallback) tpCallback()
            })
        }
        
    });
}
