var request = require('request');
var _ = require('underscore');
var Backbone = require('backbone');
var async = require('async');

var BillingItems = require('../collections/BillingItems');
var Schools = require('../collections/Schools');




function updateBillingItemsForSchool(schoolId, response) {


    var start = new Date();
    var accounting = new BillingItems();

    if (!schoolId) {
        if (response) response('Error : no school id provided');
        return;
    }

    var mmsSchools = new Schools();

    async.series([

        function(callback) {
            /* 1. Fetch school nodes*/
            console.log('getting school nodes');
            mmsSchools.getSchoolNodes(function(schools) {
                console.log('fetched school Nodes', schools.length);
                callback();
            });
        },
    ], function(err, results) {

        console.log('*********************');
        console.log('Finished Initialization');
        console.log('*********************');

        if (schoolId === 'all') {

            var allSchoolErrors = [];            
            async.eachLimit(mmsSchools.models, 1, function(school, schoolCallback) {
                school.getContractNodes(function() {
                    async.eachLimit(school.contractNodes.models, 1, function(contract, cb) {
                        var start = new Date();
                        console.log('Updating billing for contract', contract.get('title'));
                        var nid = contract.get('nid');
                        accounting.updateBillingItems({
                            mms_nid: school.get('nid'),
                            field_contract_bill_ref: nid
                        }, function(err, data) {
                            console.log(err, 'Updated billing for contract:', nid, data);
                            console.log('Duration[sec]: ', (new Date().getTime() - start.getTime()));
                            if (err) errors.push(err);
                            cb();
                        });
                    }, function() {
                        schoolCallback();
                    });
                });

            }, function() {
                var errs = (allSchoolErrors.length > 0) ? allSchoolErrors : null;
                console.log('*********************');
                console.log('finished accounting for all school');
                console.log('*********************');
                if (response) response(errs);
            });

        } else {

            var school = mmsSchools.get(schoolId);
            if (!school) {
                console.log('[Error: School with id ' + schoolId + ' not found.');
                return;
            }
            var errors = [];
            school.getContractNodes(function() {

                async.eachLimit(school.contractNodes.models, 1, function(contract, cb) {
                    var start = new Date();
                    console.log('Updating billing for contract', contract.get('title'));
                    var nid = contract.get('nid');
                    accounting.updateBillingItems({
                        mms_nid: school.get('nid'),
                        field_contract_bill_ref: nid
                    }, function(err, data) {
                        console.log(err, 'Updated billing for contract:', nid, data);
                        console.log('Duration[sec]: ', (new Date().getTime() - start.getTime()));
                        if (err) errors.push(err);
                        cb();
                    });
                }, function() {
                    console.log('finished accounting update for ', school.get('title'));
                    var errs = (errors.length > 0) ? errors : null;
                    if (response) response(errs);
                });
            });

        }

    });

}

module.exports = {
    updateAccounting: updateBillingItemsForSchool
}
