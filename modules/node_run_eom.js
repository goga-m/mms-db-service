/**
 * Module to run teacher payments for all schools in MMS.
 * Used by grunt as tasks called by teacherpayments:run
 */
module.exports = function(options, modCallback) {

    options = options || {};
    var _ = require('underscore');
    var Backbone = require('backbone');
    var async = require('async');
    var Schools = require('../collections/Schools');
    var db = require('../db');
    var log = options.log || function(msg) {
        console.log(msg);
    };

    var mmsSchools = new Schools();

    var runEom = function(school, sCallback) {
        log('========================================================================')
        log('Start eom for: ' + school.get('title') + '[' + school.id + ']');
        school.getContractNodes(function(contracts) {

            // TODO: handle error school.getContractNodes 

            async.eachLimit(contracts.models, 1, function(contract, cCallback) {
                log('Requesting eom for: ' + contract.get('title') + ' [nid:' + contract.get('nid') + ']');

                contract.runEom({
                    mms_nid: school.id,
                    log: log
                }, function(err, data) {

                    log('Response for: ' + contract.get('title') + '[' + contract.id + ']', {
                        error: err,
                        data: data
                    });

                    // TODO: handle err

                    cCallback();
                })
            }, function() {
                log('Finished eom for: ' + school.get('title') + '[' + school.id + ']');
                sCallback();
            })
        })
    }

    mmsSchools.getSchoolNodes(function(schools) {

        log('Fetched school nodes', {
            schools: schools.length
        });

        var fs = require('fs');
        var lastSchoolFilepath = __dirname + '/run_eom_last_school.txt';
        // create file if not exists
        // if (!fs.existsSync(lastSchoolFilepath)) {
            // fs.writeFileSync(lastSchoolFilepath,'');
            // var fd = fs.openSync(__dirname + '/run_eom_last_school', 0777);
            // fs.closeSync(fs.openSync(__dirname + '/run_eom_last_school', 0777));
        // }

        var lastSchoolNid = fs.readFileSync(lastSchoolFilepath).toString().replace(/ /g, '');

        var selectedSchool = mmsSchools.get(options.school)
        if (selectedSchool) {
            runEom(selectedSchool, function() {
                if (modCallback) modCallback()
            });
            return;
        } else {

            var continueAfterLastSchool = false;
            async.eachLimit(mmsSchools.models, 1, function(school, sCallback) {
                // console.log('running', school.id)
                // console.log('lastSchoolNid', lastSchoolNid, 'end', lastSchoolNid === '')
                if (lastSchoolNid !== '') {

                    if (lastSchoolNid === school.id) {
                        console.log('found last school', school)
                        continueAfterLastSchool = true;
                        fs.writeFileSync(lastSchoolFilepath, school.id);
                        // write last school to file
                        runEom(school, sCallback);
                    } else {

                        if (continueAfterLastSchool) {
                            // we're on a school that is after last school, so run eom normally
                            console.log('continueAfterLastSchool', continueAfterLastSchool, school.id);

                            fs.writeFileSync(lastSchoolFilepath, school.id);
                            runEom(school, sCallback);
                        } else {
                            // were in a school that has already run, move on to the next school
                            sCallback();
                        }
                    }

                } else {
                    fs.writeFileSync(lastSchoolFilepath, school.id);
                    runEom(school, sCallback);
                }
            }, function() {
                log('Finished eom for all schools');
                fs.writeFileSync(lastSchoolFilepath, '');
                if (modCallback) modCallback()
            })
        }
    });
}


// 18322 - Meinz
