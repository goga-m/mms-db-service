// app.js
var databaseUrl = "mms"; // "username:password@example.com/mydb"
var collections = ["statistics","accounting","directories",'benchmarks','sysnotes','total_statistics'];
var db = require("mongojs").connect(databaseUrl, collections);

module.exports = db;