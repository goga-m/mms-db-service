var request = require('request');
var _ = require('underscore');
var Backbone = require('backbone');
var async = require('async');
var statSchool = require('./stats/School');
require('datejs');

var db = require('../db');

/**
 * School Model
 */
var School = Backbone.Model.extend({
    idAttribute: 'nid',
    initialize: function() {

        var self = this;

        this.contracts = new Backbone.Collection();

    },
    getContracts: function(callback) {
        console.log('getting contracts for', this.get('title'), this.id);
        var self = this;

        if (self.reqStatus === 'on') {
            console.log('!! Already on request !!');
            return;
        }

        self.reqStatus = 'on';

        request({
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            uri: 'https://www.db-mms.com/erp/server/api',
            form: {
                callback: 'erp_drupal_view',
                view: 'erp_api_school_contracts',
                arguments: {
                    mms_nid: self.id
                }
            }
        }, function(error, response, body) {

            self.reqStatus = 'off';
            console.log('get contracts response', error, response.statusCode);

            if (!error && response.statusCode == 200) {

                var data = JSON.parse(body);
                // console.log(data);
                var formatted = _(data.nodes).map(function(item) {
                    return item.node;
                });

                self.contracts.reset(formatted);

                if (_.isFunction(callback)) {
                    callback.call(self, self);
                }
            }

        });

    },
    getContractNodes: function(callback) {
        var self = this;
        if (!self.contractNodes) self.contractNodes = new Backbone.Collection([],{
            model : require('./Contract')
        });

        console.log('getting contracts nodes for', this.get('title'));

        if (self.reqStatus === 'on') {
            console.log('!! Already on request for contract nodes!!');
            return;
        }

        self.reqStatus = 'on';

        request({
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            uri: 'https://www.db-mms.com/erp/server/api',
            form: {
                callback: 'erp_api_get_contract_nodes',
                mms_nid: self.id
            }
        }, function(error, response, body) {

            self.reqStatus = 'off';

            if (!error && response.statusCode == 200) {

                var data = JSON.parse(body);

                var formatted = _.values(data.data);
                self.contractNodes.reset(formatted);

                if (_.isFunction(callback)) {
                    callback.call(self, self.contractNodes);
                }
            }

        });
    },
    createthisMonthStatistics: function(cb) {
        var self = this;
        var now = Date.today().moveToFirstDayOfMonth().toString("yyyyMM dd").replace(' ', '');
        this.statSchool = new statSchool(this.toJSON());
        this.statSchool.createStatistics(now, function(stat) {

            var statistic = new SchoolStatistic(stat.toJSON());
            statistic.save(cb);

        }, function(errormsg) {
            if (_.isFunction(cb)) cb.call(self, 'error. No teachers or contracts');
        });
    },
    calculateStatistics: function(params, cb) {

        var self = this;
        var now = Date.today().moveToFirstDayOfMonth().toString("yyyyMM dd").replace(' ', '');
        var ymd = params.ymd || now;
        this.statSchool = new statSchool(this.toJSON());
        var timedout;
        
        // var timer = setTimeout(function(){
        //     timeout = true;
        // },3000);

        this.statSchool.createStatistics(ymd, function(stat) {

            // clearTimeout(timer);
            // if(timedout) {
            //     console.log('ERROR: Request timedout [school.calculateStatistics]');
            //     self.calculateStatistics(params,cb);
            //     return;
            // };

            var statistic = new SchoolStatistic(stat.toJSON());
            statistic.save(cb);
            console.log('Calculated and saved statistics for ',self.get('title'))

        }, function(errormsg) {
            if (_.isFunction(cb)) cb.call(self, 'error. No teachers or contracts');
        });
    },
    fetchStatisticsOld: function(ymd, cb, cbError) {

        var self = this;
        request({
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            uri: 'https://www.db-mms.com/erp/server/api',
            form: {
                callback: 'get_statistics_by_school',
                mmsnid: self.id,
                date: ymd
            }
        }, function(error, response, body) {

            if (!error && response.statusCode == 200) {

                var res = JSON.parse(body);

                if (!res.data) {
                    if (_.isFunction(cbError)) {
                        cbError.call(self);
                    }
                    return;
                }

                var statistic = new SchoolStatistic(JSON.parse(res.data.data));

                if (_.isFunction(cb)) {
                    cb.call(self, statistic);
                }
            }

        });
    },
    fetchAllOldStatistics: function(cb) {
        var school = this;
        // var stats = new Backbone.Collection();
        var today = Date.today();
        var months = 36;
        var ymds = [];
        for (var i = 1; i <= months; i++) {
            var now = Date.today().moveToFirstDayOfMonth().add(-i).months().toString("yyyyMM dd").replace(' ', '');
            ymds.push(now);
        }

        async.eachLimit(ymds, 10, function(ymd, callback) {

            school.fetchStatisticsOld(ymd, function(statistic) {
                // stats.add(statistic);
                statistic.save(function(error, docs) {
                    console.log(school.get('title'), 'statistic for [' + ymd + '] is updated.');
                    callback();
                });
            }, function() {
                console.log(school.get('title'), 'Could not find statistics for ', ymd);
                callback();
            });


        }, function(err) {
            if (_.isFunction(cb)) cb.call();
            console.log('finished statistics for ymds: ', ymds);
        });
    },
    getStatistics: function(snapshotDate, cb, error) {
        
        var self = this;
        var school = this;

        // if (!snapshotDate) {
        //     if (_.isFunction(error)) error.call(self, 'No snapshotDate provided');
        //     return;
        // }

        var query = [{
            schoolId: self.id
        }];

        if (snapshotDate) {
            query = [{
                schoolId: self.id
            }, {
                snapshotDate: snapshotDate
            }];
        }
        console.log('model/School')
        // db.statistics.save(this.toJSON(), cb);
        db.statistics.find({
            $and: query
        }, function(err, data) {

            var now = Date.today().moveToFirstDayOfMonth().toString("yyyyMM dd").replace(' ', '');
            if (data && data.length === 0 && now === snapshotDate) {
                console.log('NO STATS FOR ',self.get('title',now))
                // console.log('calculating school statistics');
                school.createthisMonthStatistics(function(error, result) {
                    console.log('created this month statistic! for ',self.get('title'))
                    if (_.isFunction(cb)) cb.call(self, result);
                });
            }
            else if (_.isFunction(cb)) cb.call(self, _.map(data, function(item) {
                return JSON.parse(item.data);
            }));
        });
    },
    getActiveTeachers: function(callback) {
        var self = this;
        if (!self.activeTeachers) self.activeTeachers = new Backbone.Collection([],{
            model : require('./Teacher')
        });

        request({
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            uri: 'https://www.db-mms.com/erp/server/api',
            form: {
                callback: 'erp_drupal_view',
                view: 'erp_api_school_teachers',
                arguments: {
                    mms_nid: self.id,
                }
            }
        }, function(error, response, body) {

            if (!error && response.statusCode == 200) {

                var data = JSON.parse(body);

                var formatted = _(data.nodes).map(function(item) {
                    return item.node;
                });
                self.activeTeachers.reset(formatted);

                if (_.isFunction(callback)) {
                    callback.call(self, null, self.activeTeachers);
                }
            } else {
                callback.call(self, error);
            }

        });
    },
});

/**
 * School Statistic model for saving..
 * @param  {[type]} options){    }}
 * @return {[type]}
 */
var SchoolStatistic = Backbone.Model.extend({
    save: function(cb, options) {
        var self = this;

        db.statistics.update({
            snapshotDate: self.get('snapshotDate'),
            schoolId: self.get('schoolId')
        }, {
            snapshotDate: self.get('snapshotDate'),
            data: JSON.stringify(self.toJSON()),
            schoolId: self.get('schoolId')
        }, {
            upsert: true /*create a new doc if not found*/
        }, cb);

        // // UPDATE EXISTING
        // // if update flag is set then remove the old entry and save a new one again.
        // // This happens only for current snapshotDate.
        // var now = Date.today().moveToFirstDayOfMonth().toString("yyyyMM dd").replace(' ', '');
        // if (options && options.update && this.get('snapshotDate') === now) {
        //     console.log('UPDATING SCHOOL STATISTIC',this.get('schoolId'),this.get('snapshotDate'));
        //     // db.statistics.remove({
        //     //     $and: [{
        //     //         schoolId: this.get('schoolId')
        //     //     }, {
        //     //         snapshotDate: this.get('snapshotDate')
        //     //     }]
        //     // });

        //     db.statistics.save({
        //         snapshotDate: self.get('snapshotDate'),
        //         data: JSON.stringify(self.toJSON()),
        //         schoolId: self.get('schoolId')
        //     }, cb);

        //     db.statistics.update({
        //         snapshotDate: self.get('snapshotDate'),
        //         schoolId: self.get('schoolId')
        //     }, {
        //         snapshotDate: self.get('snapshotDate'),
        //         data: JSON.stringify(self.toJSON()),
        //         schoolId: self.get('schoolId')
        //     }, {
        //      upsert : true /*create a new doc if not found*/
        //     }, function(err, res) {
        //         console.log(err, res);
        //     });




        //     return;
        // }

        // // SAVE NEW
        // db.statistics.findOne({
        //     $and: [{
        //         schoolId: this.get('schoolId')
        //     }, {
        //         snapshotDate: this.get('snapshotDate')
        //     }]
        // }, function(err, data) {
        //     // console.log('to save statistic',self.toJSON());
        //     if (!data) {
        //         db.statistics.insert({
        //             snapshotDate: self.get('snapshotDate'),
        //             data: JSON.stringify(self.toJSON()),
        //             schoolId: self.get('schoolId')
        //         }, cb);
        //     } else if (_.isFunction(cb)) cb.call(self, 'already exists');

        // });
    },

});



module.exports = School;
