var request = require('request');
var _ = require('underscore');
var Backbone = require('backbone');
var async = require('async');


/**
 * Teacher Model
 */
var Teacher = Backbone.Model.extend({
    idAttribute: 'nid',
    runTeacherPayment : function(options,cb) {
        var self = this;
        request({
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            uri: 'https://www.db-mms.com/erp/server/api',
            form: {
                callback: 'node_run_teacher_payment',
                mms_nid: options.mms_nid,
                teacher_nid: self.id
            }
        }, function(error, response, body) {

            if (!error && response.statusCode == 200) {
                var data = JSON.parse(body);
                var formatted = _.values(data.data);
                if (_.isFunction(cb)) {
                    cb.call(self,error,formatted);
                }
            }

        });
    }
});

module.exports = Teacher;
