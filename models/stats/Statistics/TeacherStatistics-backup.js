/*
 * Copyright 2012 MMS
 * 
 */

provide("Models.TeacherStatistics");

require('Models.Model');  
require('Models.Profile');
require('Models.School');  
require('Models.Contract');  
require('Models.Lesson');  
require('Models.TeacherPayment');  

TeacherStatistics = Backbone.Model.extend({
    initialize: function(school,teacher, ymd) {
        this.clear();
        this.set({
            teacherName: teacher.get('firstName')+' '+teacher.get('lastName'),
            teacherId: teacher.id,
            snapshotDate: ymd,
            schoolId : school.id,
            snapshotMonthFirst: Date.parseExact(ymd, 'yyyyMMdd').moveToFirstDayOfMonth().toString("yyyyMM dd").replace(' ', ''),
            snapshotMonthLast: Date.parseExact(ymd, 'yyyyMMdd').moveToLastDayOfMonth().toString("yyyyMM dd").replace(' ', '')
        });

        console.log('initializing teacherStatistics');
        console.log(this);
        var data = this.fetch({
            mmsnid : this.get('schoolId'),
            date : this.get('snapshotDate'),
            reference_nid : this.get('teacherId')
        });
        console.log('fetched data from server');
        console.log(data);
        
        if(_.isObject(data)) {
            //found.
            this.id  = data.statid;
            this.set(JSON.parse(data.data));
            return;
        }

        //Start Calculation.
        var numberOfProspects = 0;
        var numberOfTrialLessons = 0;
        var numberOfConversions = 0;//new students
        var numberOfStudents = 0;
        var numberOfCancellations = 0;
        var numberOfFinishingStudents = 0;
        var groupLessons = 0;
        var groupContracts = 0;
        var openPlaces = 0;
        var groupAverage = 0;
        var cancellationAverage = 0;
        var studentAverage = 0;
        var averageContractDuration = 0;
        var groupContracts = 0;

        var self = this;

        var groupLessonStats = teacher.getTeacherGroupLessonStats(school, this.get('snapshotMonthFirst'),this.get('snapshotMonthLast'));
        openPlaces = groupLessonStats['open_places'];
        groupLessons = groupLessonStats['group_lessons'];

        var contracts = school.getContracts().where({'teacherId' : self.get('teacherId')});
        _(contracts).each(function(contract, index) {

            if (contract.get('status')!="NO BILL"){
                var startingDate = contract.get('startingDate');
                var endingDate  = contract.get('endingDate');


                if(startingDate == self.get('snapshotMonthFirst')) {
                    numberOfConversions++;
                }

                if(endingDate == self.get('snapshotMonthLast') && (contract.get('status')!="TO CANCEL" || contract.get('status')!="CANCELLED")) {
                    numberOfFinishingStudents++;
                }

                if(startingDate <= self.get('snapshotMonthFirst') && endingDate >= self.get('snapshotMonthLast')) {
                    numberOfStudents++;

                    //find the number of months in the contract
                    averageContractDuration = averageContractDuration +  contract.get('duration');

                    if (contract.get('tarifHQ').indexOf('community')>-1 || contract.get('tarifHQ').indexOf('comfort')>-1) {
                        groupContracts++;
                    }                

                }
            }
        });

        self.set({
          'numberOfConversions' :numberOfConversions,//new students
          'numberOfStudents' :numberOfStudents,
          'numberOfCancellations' :numberOfCancellations,
          'numberOfFinishingStudents' :numberOfFinishingStudents,
          'groupContracts' :groupContracts,
          'groupLessons' :groupLessons,
          'openPlaces' :openPlaces, 
          'groupAverage' :groupAverage,
          'cancellationAverage' :cancellationAverage,
          'studentAverage' :studentAverage,
          'averageContractDuration': Math.round(averageContractDuration/contracts.length)
        });
        this.calculateTrials(school,ymd);
        this.calculateTeacherPaymentStatistics(school,ymd);
        this.save();
    },
    fetch: function(options) {
        var options = options || {};
        var self = this;
        options.callback = options.callback || 'get_statistics_by_reference'
        $.ajax({
            url: '/app/callback',
            data: {
                'data': options
            },
            type: 'POST',
            async: options.async || false,
            success: function(response) {
                if (response.status != 'success') {
                    console.debug('Error while fetching from Server:' + response.status);
                    return;
                }
                response_data = response.data; // Succesfull fetch.
                if(_.isFunction(options.success)) {
                    options.success.call(self,response.data);
                }
            }
        });
        return response_data;
    },
    save: function(options) {
        var options = options || {};
        console.log('saving statistic ' + this.get('snapshotDate'));
        var self = this;
        var dataOptions = {
            statid: this.id, // if null it will be considered as new. else it will be updated.
            callback: 'save_statistic',
            mmsnid: this.get('schoolId'),
            reference_nid : this.id,
            date: parseInt(this.get('snapshotDate'), 10),
            update_flag: 0,
            type: 'teacher_statistic',
            data: this.toJSON(),
        };
        var ajaxOptions = {
            url: options.callback || '/app/callback',
            data: {
                'data': dataOptions
            },
            type: 'POST',
            async: options.async || false,
            success: function(response) {
                if (response.status != 'success') {
                    console.debug('Error while fetching from Server:' + response.status);
                    return;
                }
                self.id = response.data.id;
                self.set(response.data);
                if (_.isFunction(options.success)) {
                    options.success.call(self, response.data);
                }
            }
        }
        console.log('SchoolStatistics: saving');
        $.ajax(ajaxOptions);
        // return response.data;
    },    
    calculateTrials : function(school,ymd) {
        //get all trial lessons of this month
        // school.getTrials().where({'date':ymd});
        if (this.get('numberOfTrialLessons')){
            return this.get('numberOfTrialLessons');
        } else {
            // console.log('calculating Trials');
            var thisMonthTrials = school.getTrials().where({'creationMonth':ymd , 'teacherId':this.id});
            this.set('numberOfTrialLessons', thisMonthTrials.length);
            return this.get('numberOfTrialLessons'); 
        }
    },
    calculateTeacherPaymentStatistics : function(school,ymd){

        var contractsAmount = 0;    
        var numberOfSubstitutions = 0;    
        var substitutionsAmount = 0;    
        var numberOfSubstitutionsSubtractions = 0;  
        var substitutionsSubtractions = 0;  
        var numberOfHourlyPayments = 0; 
        var hourlyPaymentsAmount = 0; 
        var hoursBeforeAmount = 0;  
        var numberOfHoursBefore = 0;  
        var vouchersAmount = 0; 
        var numberOfVouchers = 0; 
        var teacherPaymentTotal = 0;
        var totalAmountCheck = 0;
        //do the calculations
        //we need to get the teacher payment and teacher payment items for this month
        //get the numbers
        // ymd = Date.parseExact(ymd,'yyyyMMdd').moveToMonth(-1).toString("yyyyMM dd").replace(' ', '');
        // console.log('month:'+ymd);
        var self = this;
        var teacherPayments = school.getTeacherPayments(this.get('teacherId'), ymd);
        _(teacherPayments).each(function(tp, index) {
            if (tp){
                if (tp.get('paymentCategory')=='Contract' && tp.get('amount')) {
                    contractsAmount += parseFloat(tp.get('amount'));
                }
                if (tp.get('paymentCategory')=='Hourly') {
                    hourlyPaymentsAmount += parseFloat(tp.get('amount'));
                    numberOfHourlyPayments++;
                }
                if (tp.get('paymentCategory')=='Replaced') {
                    substitutionsSubtractions += parseFloat(tp.get('amount'));
                    numberOfSubstitutionsSubtractions++;
                }
                if (tp.get('paymentCategory')=='Substitutions') {
                    substitutionsAmount += parseFloat(tp.get('amount'));
                    numberOfSubstitutions++
                }
                if (tp.get('paymentCategory')=='Hours Before') {
                    hoursBeforeAmount += parseFloat(tp.get('amount'));
                    numberOfHoursBefore++
                } 
                teacherPaymentTotal += parseFloat(tp.get('amount'));
                if (tp.get('totalAmountCheck')>0) { totalAmountCheck = tp.get('totalAmountCheck'); }
            }

        });
        // console.log ("--- teacherPayments ---");
        // console.log (teacherPayments);

        this.set({
            'contractsAmount' : contractsAmount,    
            'numberOfSubstitutions' : numberOfSubstitutions,    
            'substitutionsAmount' : substitutionsAmount,    
            'numberOfSubstitutionsSubtractions' : numberOfSubstitutionsSubtractions,  
            'substitutionsSubtractions' : substitutionsSubtractions,  
            'numberOfHourlyPayments' : numberOfHourlyPayments, 
            'hourlyPaymentsAmount' : hourlyPaymentsAmount, 
            'hoursBeforeAmount' : hoursBeforeAmount,  
            'numberOfHoursBefore' : numberOfHoursBefore,  
            'vouchersAmount' : vouchersAmount, 
            'numberOfVouchers' : numberOfVouchers, 
            'teacherPaymentTotal' : teacherPaymentTotal,
            'totalAmountCheck': totalAmountCheck
        });

    }

});
