/*
 * Copyright 2012 MMS
 *
 */

var _ = require('underscore');
var Backbone = require('backbone');
var async = require('async');

var Model = require('../Model');
var Profile = require('../Profile');
var School = require('../School');
var Contract = require('../Contract');
var Lesson = require('../Lesson');
var TeacherPayment = require('../TeacherPayment');



TeacherStatistics = Backbone.Model.extend({
    initialize: function(school, teacher, ymd) {
        var self = this;
        this.clear();
        this.set({
            teacherName: teacher.get('firstName') + ' ' + teacher.get('lastName'),
            teacherId: teacher.id,
            snapshotDate: ymd,
            snapshotMonthFirst: Date.parseExact(ymd, 'yyyyMMdd').moveToFirstDayOfMonth().toString("yyyyMM dd").replace(' ', ''),
            snapshotMonthLast: Date.parseExact(ymd, 'yyyyMMdd').moveToLastDayOfMonth().toString("yyyyMM dd").replace(' ', '')
        });

        /*Fetch necessary data*/
        async.parallel([

                function(callback) {
                    teacher.getTeacherGroupLessonStats(school, self.get('snapshotMonthFirst'), self.get('snapshotMonthLast'), function(res) {
                        self.teacherGroupLessonStats = res;
                        callback();
                    });
                },
                function(callback) {
                    teacher.getTeacherProfile(school, function(res) {
                        self.teacherProfile = res;
                        callback();
                    });
                },
                function(callback) {
                    school.getTeacherPayments(teacher.id, ymd, function(res) {
                        self.teacherPayments = res;
                        callback();
                    });
                },
                function(callback) {
                    teacher.getAverageContractDuration({}, function(average, averageRenewals) {
                        self.set('averageContractDurationActive', average);
                        self.set('averageNumberOfRenewalsActive', averageRenewals);
                        callback();
                    });
                },
                function(callback) {
                    teacher.getAverageContractDuration({
                        status: 'cancelled'
                    }, function(average, averageRenewals) {
                        self.set('averageContractDurationCancelled', average);
                        self.set('averageNumberOfRenewalsCancelled', averageRenewals);
                        callback();
                    });
                },
                function(callback) {
                    teacher.getAverageContractDurationPerSchool({
                        // method : 'getAverageContractDurationPerInstrument',
                    }, function(data) {
                        self.set('averageContractDurationPerSchool', data)
                        callback();
                    })

                },
                function(callback) {
                    teacher.getAverageContractDurationPerSchool({
                        method: 'getAverageContractDurationPerInstrument',
                    }, function(data) {
                        self.set('averageContractDurationPerSchoolPerInstrument', data)
                        callback();
                    })
                },
                function(callback) {
                    teacher.getTeacherProspectsByStatus({
                        mms_nid: school.id
                    }, function(err, data) {
                        self.totalProspects = data;
                        self.set('numberOfTotalProspects', data.length);
                        callback();
                    });
                },
                function(callback) {
                    teacher.getTeacherProspectsByStatus({
                        status: 'archived',
                        mms_nid: school.id
                    }, function(err, data) {
                        self.archivedProspects = data;
                        self.set('numberOfArchivedProspects', data.length);
                        callback();
                    });
                },
                function(callback) {
                    teacher.getTeacherProspectsByStatus({
                        status: 'signed',
                        mms_nid: school.id
                    }, function(err, data) {
                        self.signedProspects = data;
                        self.set('numberOfSignedProspects', data.length);
                        callback();
                    });
                },
            ],
            // optional callback
            function(err, results) {
                console.log('Initialized TeacherStatistics');
                console.log('Starting calculation for teacher:', teacher.node.title)
                self.calculateRealTotalProspects();
                self.calculate(school, teacher, ymd, self.teacherGroupLessonStats);
            });

    },
    calculateRealTotalProspects: function() {
        var self = this;
        var total = new Backbone.Collection([], {
            model: Backbone.Model.extend({
                idAttribute: 'Nid'
            })
        })

        total.add(self.totalProspects.toJSON());
        total.add(self.archivedProspects.toJSON());
        total.add(self.signedProspects.toJSON());

        self.set('numberOfRealTotalProspects', total.length)
        console.log('calculateRealTotalProspects', self.get('numberOfRealTotalProspects'));
        console.log('TOTAL', JSON.stringify(total.toJSON()))
    },    
    calculate: function(school, teacher, ymd, groupLessonStats) {

        var numberOfProspects = 0;
        var numberOfTrialLessons = 0;
        var numberOfConversions = 0; //new students
        var numberOfStudents = 0;
        var numberOfCancellations = 0;
        var numberOfFinishingStudents = 0;
        var groupLessons = 0;
        var groupContracts = 0;
        var openPlaces = 0;
        var groupAverage = 0;
        var cancellationAverage = 0;
        var studentAverage = 0;
        var averageContractDuration = 0;

        var self = this;

        // var groupLessonStats = teacher.getTeacherGroupLessonStats(school, this.get('snapshotMonthFirst'),this.get('snapshotMonthLast'));
        openPlaces = groupLessonStats['open_places'];
        groupLessons = groupLessonStats['group_lessons'];

        var contracts = school.getContracts().where({
            'teacherId': self.get('teacherId')
        });
        _(contracts).each(function(contract, index) {

            if (contract.get('status') != "NO BILL") {
                var startingDate = contract.get('startingDate');
                var endingDate = contract.get('endingDate');


                if (startingDate == self.get('snapshotMonthFirst')) {
                    numberOfConversions++;
                }

                if (endingDate == self.get('snapshotMonthLast') && (contract.get('status') != "TO CANCEL" || contract.get('status') != "CANCELLED")) {
                    numberOfFinishingStudents++;
                }

                if (startingDate <= self.get('snapshotMonthFirst') && endingDate >= self.get('snapshotMonthLast')) {
                    numberOfStudents++;

                    //find the number of months in the contract
                    averageContractDuration = averageContractDuration + contract.get('duration');

                    if (contract.get('tarifHQ').indexOf('community') > -1 || contract.get('tarifHQ').indexOf('comfort') > -1) {
                        groupContracts++;
                    }

                }
            }
        });

        self.set({
            'numberOfConversions': numberOfConversions, //new students
            'numberOfStudents': numberOfStudents,
            'numberOfCancellations': numberOfCancellations,
            'numberOfFinishingStudents': numberOfFinishingStudents,
            'groupContracts': groupContracts,
            'groupLessons': groupLessons,
            'openPlaces': openPlaces,
            'groupAverage': groupAverage,
            'cancellationAverage': cancellationAverage,
            'studentAverage': studentAverage,
            'averageContractDuration': parseFloat(((self.get('averageContractDurationActive') + self.get('averageContractDurationCancelled')) / 2).toFixed(2)),
            'averageNumberOfRenewals': parseFloat(((self.get('averageNumberOfRenewalsActive') + self.get('averageNumberOfRenewalsCancelled')) / 2).toFixed(2))
        });
        this.calculateTrials(school, ymd);
        this.calculateTeacherPaymentStatistics(school, ymd, self.teacherPayments);

        self.trigger('ready');
    },
    calculateTrials: function(school, ymd) {
        //get all trial lessons of this month
        // school.getTrials().where({'date':ymd});
        if (this.get('numberOfTrialLessons')) {
            return this.get('numberOfTrialLessons');
        } else {
            // console.log('calculating Trials');
            var thisMonthTrials = school.getTrials().where({
                'creationMonth': ymd,
                'teacherId': this.id
            });
            this.set('numberOfTrialLessons', thisMonthTrials.length);
            return this.get('numberOfTrialLessons');
        }
    },
    calculateTeacherPaymentStatistics: function(school, ymd, teacherPayments) {

        var contractsAmount = 0;
        var numberOfSubstitutions = 0;
        var substitutionsAmount = 0;
        var numberOfSubstitutionsSubtractions = 0;
        var substitutionsSubtractions = 0;
        var numberOfHourlyPayments = 0;
        var hourlyPaymentsAmount = 0;
        var hoursBeforeAmount = 0;
        var numberOfHoursBefore = 0;
        var vouchersAmount = 0;
        var numberOfVouchers = 0;
        var teacherPaymentTotal = 0;
        var totalAmountCheck = 0;
        //do the calculations
        //we need to get the teacher payment and teacher payment items for this month
        //get the numbers
        // ymd = Date.parseExact(ymd,'yyyyMMdd').moveToMonth(-1).toString("yyyyMM dd").replace(' ', '');
        // console.log('month:'+ymd);
        var self = this;
        _(teacherPayments).each(function(tp, index) {
            if (tp) {
                if (tp.get('paymentCategory') == 'Contract' && tp.get('amount')) {
                    contractsAmount += parseFloat(tp.get('amount'));
                }
                if (tp.get('paymentCategory') == 'Hourly') {
                    hourlyPaymentsAmount += parseFloat(tp.get('amount'));
                    numberOfHourlyPayments++;
                }
                if (tp.get('paymentCategory') == 'Replaced') {
                    substitutionsSubtractions += parseFloat(tp.get('amount'));
                    numberOfSubstitutionsSubtractions++;
                }
                if (tp.get('paymentCategory') == 'Substitutions') {
                    substitutionsAmount += parseFloat(tp.get('amount'));
                    numberOfSubstitutions++;
                }
                if (tp.get('paymentCategory') == 'Hours Before') {
                    hoursBeforeAmount += parseFloat(tp.get('amount'));
                    numberOfHoursBefore++;
                }
                teacherPaymentTotal += parseFloat(tp.get('amount'));
                if (tp.get('totalAmountCheck') > 0) {
                    totalAmountCheck = tp.get('totalAmountCheck');
                }
            }

        });
        // console.log ("--- teacherPayments ---");
        // console.log (teacherPayments);

        this.set({
            'contractsAmount': contractsAmount,
            'numberOfSubstitutions': numberOfSubstitutions,
            'substitutionsAmount': substitutionsAmount,
            'numberOfSubstitutionsSubtractions': numberOfSubstitutionsSubtractions,
            'substitutionsSubtractions': substitutionsSubtractions,
            'numberOfHourlyPayments': numberOfHourlyPayments,
            'hourlyPaymentsAmount': hourlyPaymentsAmount,
            'hoursBeforeAmount': hoursBeforeAmount,
            'numberOfHoursBefore': numberOfHoursBefore,
            'vouchersAmount': vouchersAmount,
            'numberOfVouchers': numberOfVouchers,
            'teacherPaymentTotal': teacherPaymentTotal,
            'totalAmountCheck': totalAmountCheck
        });

    }

});

module.exports = TeacherStatistics;
