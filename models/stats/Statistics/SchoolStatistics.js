/*
 * Copyright 2012 MMS
 *
 */
var _ = require('underscore');
var Backbone = require('backbone');
var request = require('request');
var async = require('async');

require('datejs');

var TeacherStatistics = require('./TeacherStatistics');


Number.prototype.toCurrencyString = function() {
    return Math.floor(parseFloat(this) * 100) / 100;
    // return "" + Math.floor(this).toLocaleString() + (this % 1).toFixed(2).toLocaleString().replace(/^0/,'');
};



var SchoolStatistics = Backbone.Model.extend({
    initialize: function(school, ymd, update_flag) {
        var self = this;
        this.clear();
        this.set({
            schoolName: school.get('title'),
            schoolId: school.id,
            snapshotDate: ymd,
            snapshotMonthFirst: Date.parseExact(ymd, 'yyyyMMdd').moveToFirstDayOfMonth().toString("yyyyMM dd").replace(' ', ''),
            snapshotMonthLast: Date.parseExact(ymd, 'yyyyMMdd').moveToLastDayOfMonth().toString("yyyyMM dd").replace(' ', '')
        });

        // this.fetch({
        //     mmsnid : school.id,
        //     date : ymd
        // },function(data) {
        //     console.log('SchoolStatistics:fetched data',data);
        //     // Check if this statistics exists in database.
        //     if(update_flag) {
        //         this.calculate(school,ymd);
        //         return;
        //     }
        //     if(_.isObject(data)) {
        //         //found.
        //         this.id  = data.statid;
        //         this.set(JSON.parse(data.data));
        //     }
        //     else {
        //         this.calculate(school, ymd);
        //         // this.save();
        //     }

        // });

        /*Fetch necessary data*/
        async.parallel([
                function(callback) {
                    school.getContracts(function(contracts) {
                        self.contracts = contracts;
                        callback();
                    });
                },
                function(callback) {
                    school.getAccountingStatistics(ymd, function(sales) {
                        self.sales = sales;
                        callback();
                    });
                },
                function(callback) {
                    school.getSchoolGroupLessonStats(self.get('snapshotMonthFirst'), self.get('snapshotMonthLast'), function(groupLessonStats) {
                        self.groupLessonStats = groupLessonStats;
                        callback();
                    });
                },
                function(callback) {
                    school.getStudents(function(students) {
                        self.students = students;
                        callback();
                    });
                },
                function(callback) {
                    school.getTeachers(function(teachers) {
                        self.teachers = teachers;
                        callback();
                    });
                },
                function(callback) {
                    school.getProspects(function(prospects) {
                        self.prospects = prospects;
                        callback();
                    });
                },
                function(callback) {
                    school.getTrials(function(trials) {
                        self.trials = trials;
                        callback();
                    });
                },
                function(callback) {
                    school.getAverageContractDuration({}, function(average, averageRenewals) {
                        self.set('averageContractDurationActive', average);
                        self.set('averageNumberOfRenewalsActive', averageRenewals);
                        callback();
                    });
                },
                function(callback) {
                    school.getAverageContractDuration({
                        status: 'cancelled'
                    }, function(average, averageRenewals) {
                        self.set('averageContractDurationCancelled', average);
                        self.set('averageNumberOfRenewalsCancelled', averageRenewals);
                        callback();
                    });
                },
                function(callback) {
                    school.getProspectsByStatus({}, function(err, data) {
                        self.totalProspects = data;
                        self.set('numberOfTotalProspects', data.length)
                        callback();
                    })
                },
                function(callback) {
                    school.getProspectsByStatus({
                        status: 'signed'
                    }, function(err, data) {
                        self.signedProspects = data;
                        self.set('numberOfSignedProspects', data.length)
                        callback();
                    })
                },
                function(callback) {
                    school.getProspectsByStatus({
                        status: 'archived'
                    }, function(err, data) {
                        self.archivedProspects = data;
                        self.set('numberOfArchivedProspects', data.length)
                        callback();
                    })
                },
                function(callback) {
                    school.getAverageContractDurationPerInstrument({}, function(err, data) {
                        self.set('averageContractDurationPerInstrument', data);
                        callback();
                    })
                },
            ],
            // optional callback
            function(err, results) {


                // Calculate Real Total prospects
                self.calculateRealTotalProspects();
                if (self.teachers.length === 0) {
                    self.trigger('error');
                    return;
                }
                console.log('Calculating SchoolStatistics for', school.id, school.get('title'), self.students.length, self.teachers.length, self.prospects.length);
                self.calculate(school, ymd, self.contracts, self.sales, self.groupLessonStats);
                // self.trigger('ready');
            });




    },
    calculateRealTotalProspects: function() {
        var self = this;
        var total = new Backbone.Collection([], {
            model: Backbone.Model.extend({
                idAttribute: 'Nid'
            })
        })

        total.add(self.totalProspects.toJSON());
        total.add(self.archivedProspects.toJSON());
        total.add(self.signedProspects.toJSON());

        self.set('numberOfRealTotalProspects', total.length)
        console.log('calculateRealTotalProspects', self.get('numberOfRealTotalProspects'));
        console.log('TOTAL', JSON.stringify(total.toJSON()))
    },
    fetch: function(options, cb) {
        // var options = options || {};
        // var self = this;
        // options.callback = options.callback || 'get_statistics_by_school'
        // $.ajax({
        //     url: '/app/callback',
        //     data: {
        //         'data': options
        //     },
        //     type: 'POST',
        //     async: options.async || false,
        //     success: function(response) {
        //         if (response.status != 'success') {
        //             console.debug('Error while fetching from Server:' + response.status);
        //             return;
        //         }
        //         self.id = response.data.id;
        //         response_data = response.data; // Succesfull fetch.
        //         if(_.isFunction(options.success)) {
        //             options.success.call(self,response.data);
        //         }
        //     }
        // });
        // return response_data;
        //
        //
        //
        // var self = this;
        // request({
        //     headers: {'content-type' : 'application/x-www-form-urlencoded'},
        //     method : 'POST',
        //     uri : 'https://www.db-mms.com/erp/server/api',
        //     form : {
        //         callback: 'erp_api_stats_callback',
        //         stats_callback : 'get_statistics_by_school',
        //         mmsnid : self.id,
        //         date : options.date
        //     }
        // },function(error, response, body){
        //     console.log(body);
        //     if (!error && response.statusCode == 200) {
        //         // var data = JSON.parse(body);

        //     //     if( _.isFunction(cb) ){
        //     //         cb.call(self,data);
        //     //     }
        //     }

        // });
        if (_.isFunction(cb)) {
            cb.call(self, null);
        }
    },
    save: function(options) {
        // var options = options || {};
        // console.log('saving statistic ' + this.get('snapshotDate'));
        // var self = this;
        // var dataOptions = {
        //     statid: options.statid || this.id, // if null it will be considered as new. else it will be updated.
        //     callback: options.callback || 'save_statistic',
        //     mmsnid: options.mmsnid || this.get('schoolId'),
        //     date: options.date || parseInt(this.get('snapshotDate'), 10),
        //     update_flag: options.update_flag || 0,
        //     type: options.type || 'school_statistic',
        //     data: JSON.stringify(this),
        // };
        // var ajaxOptions = {
        //     url: options.callback || '/app/callback',
        //     data: {
        //         'data': dataOptions
        //     },
        //     type: 'POST',
        //     async: options.async || false,
        //     success: function(response) {
        //         if (response.status != 'success') {
        //             console.debug('Error while fetching from Server:' + response.status);
        //             return;
        //         }
        //         self.id = response.data.id;
        //         self.set(response.data);
        //         if (_.isFunction(options.success)) {
        //             options.success.call(self, response.data);
        //         }
        //     }
        // }
        // console.log('SchoolStatistics: saving');
        // $.ajax(ajaxOptions);
        // return response.data;
    },
    calculate: function(school, ymd, contracts, sales, groupLessonStats) {
        var self = this;


        // var contracts = self.contracts;
        // var sales = school.getAccountingStatistics(ymd);
        // var groupLessonStats = school.getSchoolGroupLessonStats(this.get('snapshotMonthFirst'), this.get('snapshotMonthLast'));

        var numberOfStudents = 0;
        //contracts & general statistics
        var totalNumberOfContracts = 0;
        var numberOfNextMonthContracts = 0;
        var numberOfStatusActiveContracts = 0;
        var numberOfStatusPausedContracts = 0;
        var numberOfStatusToCancelContracts = 0;
        var numberOfStatusNoBillContracts = 0;
        var numberOfStatusCancelledContracts = 0;
        var numberOfStartingContracts = 0;
        var numberOfFinishingContracts = 0;
        var numberOfNextMonthFinishingContracts = 0;
        var numberOfPausedContracts = 0;
        var numberOfVoucherContracts = 0;
        var numberOfKlingKlongContracts = 0;
        var numberOfMMSContracts = 0;
        var numberOfVibraContracts = 0;
        var numberOfProContracts = 0;
        var numberOfCanceledContracts = 0;
        var numberOfContractsWithTshirtVouchersRedeemed = 0;
        var numberOfHoursBefore = 0;
        var numberOfContractsWithSpecialDiscounts = 0;
        var numberOfProspects = 0;
        var numberOfConversions = 0; //prospects with trial lesson that contract's starting date is this month
        //instruments
        var numberOfDrumsContracts = 0;
        var numberOfGuitarContracts = 0;
        var numberOfSchoolContracts = 0;
        var numberOfKeyboardContracts = 0;
        var numberOfVocalsContracts = 0;
        var numberOfBassContracts = 0;
        var numberOfSaxophonContracts = 0;
        var numberOfBandContracts = 0;
        var numberOfDJContracts = 0;
        var numberOfMusicTechnologyContracts = 0;
        var numberOfMusicTheoryContracts = 0;
        //*** Group related statistics
        var groupLessons = 0;
        var groupContracts = 0;
        var openPlaces = 0;

        var groupRating = 0;
        var groupRatio = 0;
        var groupEfficiencyIndex = 0;
        var studentContractRatio = 0;

        //*** Finance related statistics (sales and teacher payments)
        var salesLM = 0;
        var averageContractValue = 0;
        var totalTeacherPayment = 0;
        var averageTeacherPayment = 0;
        var averageTeacherPaymentPerContract = 0;
        var profitMargin = 0;

        var mmsContractsSales = 0;
        var vibraContractsSales = 0;
        var kkContractsSales = 0;
        var contractsSales = 0;

        var mmsApplicationFees = 0;
        var vibraApplicationFees = 0;
        var kkApplicationFees = 0;

        var mmsReceivable = 0;
        var vibraReceivable = 0;
        var kkReceivable = 0;

        var mmsOpenAccountingTM = 0;
        var vibraOpenAccountingTM = 0;
        var kkOpenAccountingTM = 0;
        var salesMMS = 0;
        var salesVibra = 0;
        var salesKK = 0;
        var totalSales = 0;
        var outstandingAmounts = 0;
        var applicationFees = 0;
        var monthyAccounting = 0;
        var received = 0;
        var cashReceived = 0;
        var autoPayReceived = 0;
        var receivable = 0;
        var overdue = 0;
        // 'teacherPayments' : teacherPayments.toFixed(2),
        var franchiseFee = 0;
        //'netIncome' : netIncome.toFixed(2),
        var monthlySales = 0;
        //*** Demographics related statistics
        var maleStudents = 0;
        var femaleStudents = 0;
        var malesAgeGroup1 = 0; //0-6
        var malesAgeGroup2 = 0; //6-13
        var malesAgeGroup3 = 0; //13-18
        var malesAgeGroup4 = 0; //19-25
        var malesAgeGroup5 = 0; //26-34
        var malesAgeGroup6 = 0; //34-50
        var malesAgeGroup7 = 0; //50+
        var femalesAgeGroup1 = 0;
        var femalesAgeGroup2 = 0;
        var femalesAgeGroup3 = 0;
        var femalesAgeGroup4 = 0;
        var femalesAgeGroup5 = 0;
        var femalesAgeGroup6 = 0;
        var femalesAgeGroup7 = 0;
        //*** Marketing/channels
        var numberOfFriendSources = 0;
        var numberOfFacebookSources = 0;
        var numberOfMmsDaySources = 0;
        var numberOfInternetSources = 0;
        var numberOfOtherSources = 0;
        var otherMarketingSources = {};
        var referencedByStudents = {}; //TODO: print the table (view/block)

        var numberOfFlyer = 0;
        var numberOfMembershipPartner = 0;
        var numberOfRadio = 0;
        var numberOfMMMag = 0;
        var numberOfAdvert = 0;
        var numberOfNewspaper = 0;
        var numberOfADAC = 0;
        var numberOfSClub = 0;

        //course type
        var numberOfHobbyCourse = 0;
        var numberOfProCourse = 0;

        var contractDuration = 0;
        //tarifs
        var numberOfComfort = 0;
        var numberOfCommunity = 0;
        var numberOfCustom30 = 0;
        var numberOfCustom45 = 0;
        var numberOfCustom60 = 0;
        var numberOfCustomplus30 = 0;
        var numberOfCustomplus45 = 0;
        var numberOfGroupon = 0;
        var numberOfKlingKlong = 0;
        var numberOfOldComfort = 0;
        var numberOfOldCustom30 = 0;
        var numberOfOldCustom45 = 0;
        var numberOfProfessionalProgram = 0;
        var numberOfV1030 = 0;
        var numberOfV1045 = 0;
        var numberOfVeryOldComfort = 0;
        var numberOfVeryOldCustom30 = 0;
        var numberOfVeryOldCustom45 = 0;
        //trials
        var numberOfTrialLessons = 0;

        //sales calculation
        // var sales = school.getAccountingStatistics(ymd);

        var studentsInSchool = [];
        var studentsWithMoreThanOneContracts = [];
        var numberOfStudentsWithMoreThanOneContracts = 0;
        var totalNumberOfStudents = 0;

        averageContractValue = sales.get('acv');
        mmsContractsSales = sales.get('mmsContractsSales');
        vibraContractsSales = sales.get('vibraContractsSales');
        kkContractsSales = sales.get('kkContractsSales');
        contractsSales = sales.get('contractsSales');

        mmsApplicationFees = sales.get('mmsApplicationFees');
        vibraApplicationFees = sales.get('vibraApplicationFees');
        kkApplicationFees = sales.get('kkApplicationFees');

        mmsReceivable = sales.get('mmsReceivable');
        vibraReceivable = sales.get('vibraReceivable');
        kkReceivable = sales.get('kkReceivable');

        mmsOpenAccountingTM = sales.get('mmsOpenAccountingTM');
        vibraOpenAccountingTM = sales.get('vibraOpenAccountingTM');
        kkOpenAccountingTM = sales.get('kkOpenAccountingTM');

        salesMMS = sales.get('salesMMS');
        salesVibra = sales.get('salesVibra');
        salesKK = sales.get('salesKK');
        totalSales = sales.get('totalSales');
        outstandingAmounts = sales.get('outstandingAmounts');
        //applicationFees are calculated on the contract
        //applicationFees = sales.get('applicationFees');
        monthyAccounting = sales.get('monthyAccounting');
        cashReceived = sales.get('cashReceived');
        autoPayReceived = sales.get('autoPayReceived');
        received = sales.get('received');
        receivable = sales.get('receivable');
        overdue = sales.get('overdue');
        // 'teacherPayments' : teacherPayments.toFixed(2),
        franchiseFee = sales.get('franchiseFee');
        //'netIncome' : netIncome.toFixed(2),
        monthlySales = sales.get('monthlySales');


        //Calculate the group contracts and lessons for this school and month
        // var groupLessonStats = school.getSchoolGroupLessonStats(this.get('snapshotMonthFirst'), this.get('snapshotMonthLast'));
        openPlaces = groupLessonStats['open_places'];
        groupLessons = groupLessonStats['group_lessons'];

        console.log('[SchoolStatistics] Calculating Group Lesson Stats:' + openPlaces + ' ' + groupLessons);

        var snapshotNextMonthFirst = Date.parseExact(ymd, 'yyyyMMdd').moveToFirstDayOfMonth().add({
            months: 1
        }).toString("yyyyMM dd").replace(' ', '');
        var snapshotNextMonthLast = Date.parseExact(ymd, 'yyyyMMdd').moveToLastDayOfMonth().add({
            months: 1
        }).toString("yyyyMM dd").replace(' ', '');

        // console.log(self.get('snapshotMonthFirst')+' next month  '+snapshotNextMonthFirst);
        async.eachSeries(contracts.models, function(contract, callback) {

            var fromDate = self.get('snapshotMonthFirst');
            var toDate = self.get('snapshotMonthLast');

            contract.wasCancelledOn(fromDate, toDate, function(wasCancelledOnDate) {




                var startingDate = contract.get('startingDate');
                var endingDate = contract.get('endingDate');




                //this is for voucher/groupon contracts
                if (contract.get('status') == "NO BILL") {
                    numberOfStatusNoBillContracts++;

                    if (startingDate == self.get('snapshotMonthFirst')) { //check only the ones started on this month
                        numberOfVoucherContracts++;
                        // console.log("NO BILL "+contract.get('title'));
                    }
                } else {


                    if (startingDate >= snapshotNextMonthFirst && startingDate <= snapshotNextMonthLast) {
                        numberOfNextMonthContracts++;
                        // console.log("NEXT MONTH "+contract.get('title'));
                    }
                    if (startingDate == self.get('snapshotMonthFirst')) {
                        // console.log("STARTING "+contract.get('title'));

                        numberOfStartingContracts++;
                        numberOfHoursBefore = numberOfHoursBefore + contract.get('hoursBeforeContract');
                        applicationFees = applicationFees + contract.get('applicationFee');
                    }

                    if (endingDate == self.get('snapshotMonthLast') && (contract.get('status') == "TO CANCEL" || contract.get('status') == "CANCELED")) {
                        // console.log("FINISHING "+contract.get('title'));
                        numberOfFinishingContracts++;
                    }

                    if (endingDate == self.get('snapshotNextMonthLast') && (contract.get('status') == "TO CANCEL" || contract.get('status') == "CANCELED")) {
                        // console.log("FINISHING "+contract.get('title'));
                        numberOfNextMonthFinishingContracts++;
                    }
                    if (startingDate <= self.get('snapshotMonthFirst') && (endingDate >= self.get('snapshotMonthLast') || (endingDate <= self.get('snapshotMonthLast') && contract.get('status') == 'PAUSED'))) {
                        totalNumberOfContracts++;
                        // console.log("TOTAL "+contract.get('title')+" "+contract.id+" "+contract.get('status')+" "+startingDate+" "+endingDate);
                        if (contract.get('status') == "ACTIVE") {
                            numberOfStatusActiveContracts++;
                        } else if (contract.get('status') == "PAUSED") {
                            numberOfStatusPausedContracts++;
                        } else if (contract.get('status') == "TO CANCEL") {
                            numberOfStatusToCancelContracts++;
                        } else if (contract.get('status') == "CANCELED") {
                            numberOfStatusCancelledContracts++;
                        }

                        if (_.indexOf(studentsInSchool, contract.get('studentId')) > -1) {
                            numberOfStudentsWithMoreThanOneContracts++;
                            studentsWithMoreThanOneContracts.push(contract.get('studentId'));
                        } else {
                            totalNumberOfStudents++;
                            studentsInSchool.push(contract.get('studentId'));
                        }
                        if (contract.get('tarifHQ').indexOf('community') > -1 || contract.get('tarifHQ').indexOf('comfort') > -1) {
                            groupContracts++;
                        }


                        switch (contract.get('tarifHQ')) {

                            case 'comfort':
                                numberOfComfort++;
                                break;
                            case 'community':
                                numberOfCommunity++;
                                break;
                            case 'custom 30':
                                numberOfCustom30++;
                                break;
                            case 'custom 45':
                                numberOfCustom45++;
                                break;
                            case 'custom 60':
                                numberOfCustom60++;
                                break;
                            case 'custom plus 30':
                                numberOfCustomplus30++;
                                break;
                            case 'custom plus 45':
                                numberOfCustomplus45++;
                                break;
                            case 'Groupon':
                                numberOfGroupon++;
                                break;
                            case 'Kling Klong':
                                numberOfKlingKlong++;
                                break;
                            case 'old comfort':
                                numberOfOldComfort++;
                                break;
                            case 'old custom 30':
                                numberOfOldCustom30++;
                                break;
                            case 'old custom 45':
                                numberOfOldCustom45++;
                                break;
                            case 'Professional Program':
                                numberOfProfessionalProgram++;
                                break;
                            case 'V 10 / 30':
                                numberOfV1030++;
                                break;
                            case 'V 10 / 45':
                                numberOfV1045++;
                                break;
                            case 'very old comfort':
                                numberOfVeryOldComfort++;
                                break;
                            case 'very old custom 30':
                                numberOfVeryOldCustom30++;
                                break;
                            case 'very old custom 45':
                                numberOfVeryOldCustom45++;
                                break;


                        }
                        //find the number of months in the contract
                        contractDuration = contractDuration + contract.get('duration');

                        //calculate the rest of the numbers for the currently valid (active,paused, to cancel) contracts
                        //if the contract is a _packaged_ voucher
                        //if (contract.get('')) { numberOfVoucherContracts++; }
                        //check if contract is pp
                        //if () { numberOfProContracts++; }
                        //check contract ending date against tarif duration to see if it was cancelled

                        //TODO: have to fix this so that it knows whether a contract was cancelled or not during a certain time
                        if (wasCancelledOnDate) {
                            numberOfCanceledContracts++;
                        }
                        // if(contract.get('status')=='TO CANCEL') {
                        //     numberOfCanceledContracts++;
                        // }

                        if (contract.get('tshirtVoucherRedeemed') == 'Yes') {
                            numberOfContractsWithTshirtVouchersRedeemed++;
                        }
                        //course type : Hobby vs Pro
                        if (contract.get('course') == 'Hobby') {
                            numberOfHobbyCourse++;
                        }
                        if (contract.get('course') == 'Professional') {
                            numberOfProCourse++;
                        }

                        if (contract.get('specialDiscountId')) {
                            numberOfContractsWithSpecialDiscounts++;
                        }
                        //instruments
                        if (contract.get('instruments') == 'Drums') {
                            numberOfDrumsContracts++;
                            numberOfMMSContracts++;
                        }
                        if (contract.get('instruments') == 'Guitar') {
                            numberOfGuitarContracts++;
                            numberOfMMSContracts++;
                        }
                        if (contract.get('instruments') == 'School') {
                            numberOfSchoolContracts++;
                            numberOfMMSContracts++;
                        }
                        if (contract.get('instruments') == 'Keyboard') {
                            numberOfKeyboardContracts++;
                            numberOfMMSContracts++;
                        }
                        if (contract.get('instruments') == 'Vocals') {
                            numberOfVocalsContracts++;
                            numberOfMMSContracts++;
                        }
                        if (contract.get('instruments') == 'Bass') {
                            numberOfBassContracts++;
                            numberOfMMSContracts++;
                        }
                        if (contract.get('instruments') == 'Kling Klong') {
                            numberOfKlingKlongContracts++;
                            numberOfKlingKlongContracts++;
                        }
                        if (contract.get('instruments') == 'Saxophon') {
                            numberOfSaxophonContracts++;
                            numberOfMMSContracts++;
                        }
                        if (contract.get('instruments') == 'Band') {
                            numberOfBandContracts++;
                            numberOfMMSContracts++;
                        }
                        if (contract.get('instruments') == 'DJ') {
                            numberOfDJContracts++;
                            numberOfVibraContracts++;
                        }
                        if (contract.get('instruments') == 'Music Technology') {
                            numberOfMusicTechnologyContracts++;
                            numberOfVibraContracts++;
                        }
                        if (contract.get('instruments') == 'Music Theory') {
                            numberOfMusicTheoryContracts++;
                            numberOfMMSContracts++;
                        }

                        //demographics
                        var age = contract.getAge(school);
                        if (contract.getGender(school) == 'Male') {
                            maleStudents++;
                            if (age < 6) {
                                malesAgeGroup1++;
                            } else if (age < 13) {
                                malesAgeGroup2++;
                            } else if (age < 19) {
                                malesAgeGroup3++;
                            } //13-18
                            else if (age < 26) {
                                malesAgeGroup4++;
                            } //19- 25
                            else if (age < 35) {
                                malesAgeGroup5++;
                            } //26-34
                            else if (age < 50) {
                                malesAgeGroup6++;
                            } //34-50
                            else {
                                malesAgeGroup7++;
                            } //50+
                        } else {
                            femaleStudents++;
                            if (age < 6) {
                                femalesAgeGroup1++;
                            } else if (age < 13) {
                                femalesAgeGroup2++;
                            } else if (age < 19) {
                                femalesAgeGroup3++;
                            } else if (age < 26) {
                                femalesAgeGroup4++;
                            } else if (age < 35) {
                                femalesAgeGroup5++;
                            } else if (age < 50) {
                                femalesAgeGroup6++;
                            } else {
                                femalesAgeGroup7++;
                            }
                        }

                        //marketing
                        var students = school.getStudents();

                        var refById = contract.getReferee(school);
                        if (refById) {
                            var referrer = students._byId[refById];
                            // Be sure is not null.
                            if (referrer) {
                                var rbs = referrer.get('firstName') + ' ' + referrer.get('lastName');
                                //init as 0 on first time
                                if (referencedByStudents[rbs]) {
                                    referencedByStudents[rbs] = parseInt(referencedByStudents[rbs], 10) + 1;
                                }
                                if (!referencedByStudents[rbs]) {
                                    referencedByStudents[rbs] = 1;
                                }
                            }
                        }

                        var marketingSource = contract.getMarketingSource(school);
                        if (marketingSource) {

                            switch (marketingSource) {
                                case 'flyer':
                                    numberOfFlyer++;
                                    break;
                                case 'membership partner':
                                    numberOfMembershipPartner++;
                                    break;
                                case 'radio':
                                    numberOfRadio++;
                                    break;
                                case 'MMMag':
                                    numberOfMMMag++;
                                    break;
                                case 'advert':
                                    numberOfAdvert++;
                                    break;
                                case 'newspaper article':
                                    numberOfNewspaper++;
                                    break;
                                case 'ADAC':
                                    numberOfADAC++;
                                    break;
                                case 'S-Club':
                                    numberOfSClub++;
                                    break;
                                case 'friend':
                                    numberOfFriendSources++;
                                    break;
                                case 'internet':
                                    numberOfInternetSources++;
                                    break;
                                case 'MMS Day':
                                    numberOfMmsDaySources++;
                                    break;
                                case 'facebook':
                                    numberOfFacebookSources++;
                                    break;
                                case 'other':
                                    numberOfOtherSources++;
                                    var oms = contract.getOtherMarketingSource(school);
                                    // Be sure is not null.
                                    if (oms) {
                                        //init as 0 on first time
                                        if (otherMarketingSources[oms]) {
                                            otherMarketingSources[oms] = parseInt(otherMarketingSources[oms], 10) + 1;
                                        }
                                        if (!otherMarketingSources[oms]) {
                                            otherMarketingSources[oms] = 1;
                                        }
                                    }
                                    break;
                            }
                        }

                    }

                }


                callback();
            });


        }, function(err) {
            console.log('finished all contracts');



            var studentsList = '';
            _(studentsWithMoreThanOneContracts).each(function(studentid, index) {
                studentsList += ',' + studentid;
            });
            console.log('[SchoolStatistics] Calculating students with more than one contracts:' + studentsWithMoreThanOneContracts.length);

            self.set({
                'totalNumberOfContracts': parseInt(totalNumberOfContracts),
                'totalNumberOfStudents': totalNumberOfStudents,
                'numberOfStatusActiveContracts': numberOfStatusActiveContracts,
                'numberOfStatusPausedContracts': numberOfStatusPausedContracts,
                'numberOfStatusToCancelContracts': numberOfStatusToCancelContracts,
                'numberOfStatusNoBillContracts': numberOfStatusNoBillContracts,
                'numberOfStatusCancelledContracts': numberOfStatusCancelledContracts,

                'numberOfStudentsWithMoreThanOneContracts': numberOfStudentsWithMoreThanOneContracts,
                'numberOfFinishingContracts': numberOfFinishingContracts,
                'numberOfNextMonthFinishingContracts': numberOfNextMonthFinishingContracts,
                'numberOfNextMonthContracts': numberOfNextMonthContracts,
                'numberOfStartingContracts': numberOfStartingContracts,
                'numberOfPausedContracts': numberOfPausedContracts,
                'numberOfVoucherContracts': numberOfVoucherContracts,
                'numberOfKlingKlongContracts': numberOfKlingKlongContracts,
                'numberOfMMSContracts': numberOfMMSContracts,
                'numberOfVibraContracts': numberOfVibraContracts,
                'numberOfProContracts': numberOfProContracts,
                'numberOfCanceledContracts': numberOfCanceledContracts,
                'numberOfContractsWithTshirtVouchersRedeemed': numberOfContractsWithTshirtVouchersRedeemed,
                'numberOfHoursBefore': numberOfHoursBefore,
                'numberOfContractsWithSpecialDiscounts': numberOfContractsWithSpecialDiscounts,
                //group statistics
                'groupContracts': parseInt(groupContracts),
                'groupLessons': parseInt(groupLessons),
                'openPlaces': parseInt(openPlaces),
                //instruments
                'numberOfDrumsContracts': numberOfDrumsContracts,
                'numberOfGuitarContracts': numberOfGuitarContracts,
                'numberOfSchoolContracts': numberOfSchoolContracts,
                'numberOfKeyboardContracts': numberOfKeyboardContracts,
                'numberOfVocalsContracts': numberOfVocalsContracts,
                'numberOfBassContracts': numberOfBassContracts,
                'numberOfSaxophonContracts': numberOfSaxophonContracts,
                'numberOfBandContracts': numberOfBandContracts,
                'numberOfDJContracts': numberOfDJContracts,
                'numberOfMusicTechnologyContracts': numberOfMusicTechnologyContracts,
                'numberOfMusicTheoryContracts': numberOfMusicTheoryContracts,
                //sales
                'averageContractValue': averageContractValue,
                'mmsContractsSales': mmsContractsSales,
                'vibraContractsSales': vibraContractsSales,
                'kkContractsSales': kkContractsSales,
                'contractsSales': contractsSales,

                'mmsApplicationFees': mmsApplicationFees,
                'vibraApplicationFees': vibraApplicationFees,
                'kkApplicationFees': kkApplicationFees,

                'mmsReceivable': mmsReceivable,
                'vibraReceivable': vibraReceivable,
                'kkReceivable': kkReceivable,

                'mmsOpenAccountingTM': mmsOpenAccountingTM,
                'vibraOpenAccountingTM': vibraOpenAccountingTM,
                'kkOpenAccountingTM': kkOpenAccountingTM,

                'salesMMS': salesMMS,
                'salesVibra': salesVibra,
                'salesKK': salesKK,
                'totalSales': totalSales,
                'outstandingAmounts': outstandingAmounts,
                'applicationFees': applicationFees,
                'contractDuration': Math.round(contractDuration / contracts.length),
                'averageContractDuration': parseFloat(((self.get('averageContractDurationActive') + self.get('averageContractDurationCancelled')) / 2).toFixed(2)),
                'averageNumberOfRenewals': parseFloat(((self.get('averageNumberOfRenewalsActive') + self.get('averageNumberOfRenewalsCancelled')) / 2).toFixed(2)),
                'monthyAccounting': monthyAccounting,
                'cashReceived': cashReceived,
                'autoPayReceived': autoPayReceived,
                'received': received,
                'receivable': receivable,
                'overdue': overdue,
                //'teacherPayments' : teacherPayments.toFixed(2),
                'franchiseFee': franchiseFee,
                //'netIncome' : netIncome.toFixed(2),
                'monthlySales': monthlySales,
                //tarifs
                'numberOfComfort': numberOfComfort,
                'numberOfCommunity': numberOfCommunity,
                'numberOfCustom30': numberOfCustom30,
                'numberOfCustom45': numberOfCustom45,
                'numberOfCustom60': numberOfCustom60,
                'numberOfCustomplus30': numberOfCustomplus30,
                'numberOfCustomplus45': numberOfCustomplus45,
                'numberOfGroupon': numberOfGroupon,
                'numberOfKlingKlong': numberOfKlingKlong,
                'numberOfOldComfort': numberOfOldComfort,
                'numberOfOldCustom30': numberOfOldCustom30,
                'numberOfOldCustom45': numberOfOldCustom45,
                'numberOfProfessionalProgram': numberOfProfessionalProgram,
                'numberOfV1030': numberOfV1030,
                'numberOfV1045': numberOfV1045,
                'numberOfVeryOldComfort': numberOfVeryOldComfort,
                'numberOfVeryOldCustom30': numberOfVeryOldCustom30,
                'numberOfVeryOldCustom45': numberOfVeryOldCustom45,
                //demographics
                'maleStudents': maleStudents,
                'femaleStudents': femaleStudents,
                'malesAgeGroup1': malesAgeGroup1,
                'malesAgeGroup2': malesAgeGroup2,
                'malesAgeGroup3': malesAgeGroup3,
                'malesAgeGroup4': malesAgeGroup4,
                'malesAgeGroup5': malesAgeGroup5,
                'malesAgeGroup6': malesAgeGroup6,
                'malesAgeGroup7': malesAgeGroup7,
                'femalesAgeGroup1': femalesAgeGroup1,
                'femalesAgeGroup2': femalesAgeGroup2,
                'femalesAgeGroup3': femalesAgeGroup3,
                'femalesAgeGroup4': femalesAgeGroup4,
                'femalesAgeGroup5': femalesAgeGroup5,
                'femalesAgeGroup6': femalesAgeGroup6,
                'femalesAgeGroup7': femalesAgeGroup7,
                //course type
                'numberOfHobbyCourse': numberOfHobbyCourse,
                'numberOfProCourse': numberOfProCourse,
                'referencedByStudents': referencedByStudents,
                //marketing/channels
                'numberOfFriendSources': numberOfFriendSources,
                'numberOfInternetSources': numberOfInternetSources,
                'numberOfMmsDaySources': numberOfMmsDaySources,
                'numberOfOtherSources': numberOfOtherSources,
                'numberOfFacebookSources': numberOfFacebookSources,
                'numberOfFlyer': numberOfFlyer,
                'numberOfMembershipPartner': numberOfMembershipPartner,
                'numberOfRadio': numberOfRadio,
                'numberOfMMMag': numberOfMMMag,
                'numberOfAdvert': numberOfAdvert,
                'numberOfNewspaper': numberOfNewspaper,
                'numberOfADAC': numberOfADAC,
                'numberOfSClub': numberOfSClub,
                'otherMarketingSources': otherMarketingSources,
                'lastUpdate': new Date().getTime()
            });

            //this.calculateStudents(school);
            console.log('calculating trials');
            self.calculateTrials(school, ymd);

            console.log('calculating calculateProspects');
            self.calculateProspects(school, ymd);

            //self.calculateAverageContractValue();
            console.log('fetching getTeacherCharts');
            self.getTeacherCharts(school, ymd, self.teachers, function() {

                console.log('fetched getTeacherCharts');

                self.getGroupRatio();
                self.getGroupEfficiencyIndex();
                self.getPossibleOpenPlaces();
                self.calculateAverageTeacherPaymentPercentage();
                self.getGroupRating();

                console.log('[SchoolStatistics] Created model');
                self.trigger('ready');

            });



        });


    },
    calculateTrials: function(school, ymd) {
        //get all trial lessons of this month
        // school.getTrials().where({'date':ymd});
        if (this.get('numberOfTrialLessons')) {
            return this.get('numberOfTrialLessons');
        } else {

            var trials = this.trials
            var thisMonthTrials = trials.where({
                'creationMonth': ymd
            });

            this.set('numberOfTrialLessons', thisMonthTrials.length);
            console.log('[SchoolStatistics] Calculating Trials:' + thisMonthTrials.length);
            // return this.get('numberOfTrialLessons');

        }
    },
    calculateProspects: function(school, ymd) {
        //get all prospects created this month
        if (this.get('numberOfProspects')) {
            return this.get('numberOfProspects');
        } else {

            var prospects = school.getProspects();

            var thisMonthProspects = prospects.where({
                'creationMonth': ymd
            });

            console.log('[SchoolStatistics] Calculating thisMonthProspects: ' + thisMonthProspects.length);
            this.set('numberOfProspects', thisMonthProspects.length);
        }
    },
    calculateStudents: function(school, students) { //TODO: calculate students according to ymd (base it on contract)
        var self = this;
        // var students = school.getStudents();

        //demographics
        var numberOfMaleStudents = 0;
        var numberOfFemaleStudents = 0;
        //marketing/channels
        var numberOfFriendSources = 0;
        var numberOfFacebookSources = 0;
        var numberOfMmsDaySources = 0;
        var numberOfInternetSources = 0;
        var numberOfOtherSources = 0;
        var otherMarketingSources = {};
        var referencedByStudents = {}; //TODO: print the table (view/block)

        var numberOfFlyer = 0;
        var numberOfMembershipPartner = 0;
        var numberOfRadio = 0;
        var numberOfMMMag = 0;
        var numberOfAdvert = 0;
        var numberOfNewspaper = 0;
        var numberOfADAC = 0;
        var numberOfSClub = 0;


        students.each(function(student, index) {

            if (student.get('gender') == 'Male') {
                numberOfMaleStudents++;
            }

            if (student.get('gender') == 'Female') {
                numberOfFemaleStudents++;
            }
            var refById = student.get('referencedByStudentId');
            if (refById) {
                var referrer = students._byId[refById];
                // Be sure is not null.
                if (referrer) {
                    var rbs = referrer.get('firstName') + ' ' + referrer.get('lastName');
                    //init as 0 on first time
                    if (referencedByStudents[rbs]) {
                        referencedByStudents[rbs] = parseInt(referencedByStudents[rbs], 10) + 1;
                    }
                    if (!referencedByStudents[rbs]) {
                        referencedByStudents[rbs] = 1;
                    }
                }
            }
            var marketingSource = student.get('marketingSource');
            if (marketingSource) {

                switch (marketingSource) {
                    case 'flyer':
                        numberOfFlyer++;
                        break;
                    case 'membership partner':
                        numberOfMembershipPartner++;
                        break;
                    case 'radio':
                        numberOfRadio++;
                        break;
                    case 'MMMag':
                        numberOfMMMag++;
                        break;
                    case 'advert':
                        numberOfAdvert++;
                        break;
                    case 'newspaper article':
                        numberOfNewspaper++;
                        break;
                    case 'ADAC':
                        numberOfADAC++;
                        break;
                    case 'S-Club':
                        numberOfSClub++;
                        break;
                    case 'friend':
                        numberOfFriendSources++;
                        break;
                    case 'internet':
                        numberOfInternetSources++;
                        break;
                    case 'MMS Day':
                        numberOfMmsDaySources++;
                        break;
                    case 'facebook':
                        numberOfFacebookSources++;
                        break;
                    case 'other':
                        numberOfOtherSources++;
                        var oms = student.get('otherMarketingSource');
                        // Be sure is not null.
                        if (oms) {
                            //init as 0 on first time
                            if (otherMarketingSources[oms]) {
                                otherMarketingSources[oms] = parseInt(otherMarketingSources[oms], 10) + 1;
                            }
                            if (!otherMarketingSources[oms]) {
                                otherMarketingSources[oms] = 1;
                            }
                        }
                        break;
                }
            }
        });

        self.set({
            //demographics
            'numberOfMaleStudents': numberOfMaleStudents,
            'numberOfFemaleStudents': numberOfFemaleStudents,
            'referencedByStudents': referencedByStudents,
            //marketing/channels
            'numberOfFriendSources': numberOfFriendSources,
            'numberOfInternetSources': numberOfInternetSources,
            'numberOfMmsDaySources': numberOfMmsDaySources,
            'numberOfOtherSources': numberOfOtherSources,
            'numberOfFacebookSources': numberOfFacebookSources,
            'numberOfFlyer': numberOfFlyer,
            'numberOfMembershipPartner': numberOfMembershipPartner,
            'numberOfRadio': numberOfRadio,
            'numberOfMMMag': numberOfMMMag,
            'numberOfAdvert': numberOfAdvert,
            'numberOfNewspaper': numberOfNewspaper,
            'numberOfADAC': numberOfADAC,
            'numberOfSClub': numberOfSClub,
            'otherMarketingSources': otherMarketingSources
        });

    },
    getTeacherCharts: function(school, ymd, teachers, cb) {
        //TODO: make a teacher statistic object to put in the list
        //get all school teachers, calculate their stats
        // var teachers = school.getTeachers();
        var self = this;
        // var teacherStatistics = eval('(' + this.get('teacherStatistics') + ')');
        // var teacherStatisticsCollection = Backbone.Collection.extend();
        // console.log('teachers');
        // console.log(teachers);

        var totalTeacherPayment = 0;
        var totalTeacherContractPayments = 0;
        var averageTeacherPayment = 0;
        var averageTeacherPaymentPerContract = 0;
        var profitMargin = 0;
        var numberOfPayedTeachers = 0;
        var allTeachers = [];
        async.eachLimit(teachers.models, 1, function(teacher, tCallback) {
            var teacherCharts = {};

            //TODO:
            //1. remove franchisee from teacher payment calculations
            //2. calculate VAT

            var teacherStats = new TeacherStatistics(school, teacher, ymd);

            teacherStats.on('ready', function() {

                // var teachName = teacherStats.get('teacherName');
                //[teachName]
                teacherCharts['id'] = teacher.id || teacher.node.nid;

                console.log('ready teacher...', teacher.node.title, teacherCharts['id']);

                //full teacher model
                teacherCharts['profile'] = teacher;
                teacherCharts['teacherProfile'] = teacher.get('teacherProfile');
                teacherCharts['numberOfConversions'] = teacherStats.get('numberOfConversions');
                teacherCharts['numberOfStudents'] = teacherStats.get('numberOfStudents');
                teacherCharts['numberOfCancellations'] = teacherStats.get('numberOfCancellations');
                teacherCharts['numberOfFinishingStudents'] = teacherStats.get('numberOfFinishingStudents');
                teacherCharts['groupLessons'] = teacherStats.get('groupLessons');
                teacherCharts['groupContracts'] = teacherStats.get('groupContracts');
                teacherCharts['openPlaces'] = teacherStats.get('openPlaces');
                teacherCharts['groupAverage'] = teacherStats.get('groupAverage');
                teacherCharts['cancellationAverage'] = teacherStats.get('cancellationAverage');
                teacherCharts['studentAverage'] = teacherStats.get('studentAverage');
                teacherCharts['averageContractDuration'] = teacherStats.get('averageContractDuration');
                teacherCharts['averageContractDurationActive'] = teacherStats.get('averageContractDurationActive');
                teacherCharts['averageContractDurationCancelled'] = teacherStats.get('averageContractDurationCancelled');
                teacherCharts['averageNumberOfRenewals'] = teacherStats.get('averageNumberOfRenewals');
                teacherCharts['averageNumberOfRenewalsActive'] = teacherStats.get('averageNumberOfRenewalsActive');
                teacherCharts['averageNumberOfRenewalsCancelled'] = teacherStats.get('averageNumberOfRenewalsCancelled');

                teacherCharts['firstName'] = teacher.get('firstName');
                teacherCharts['lastName'] = teacher.get('lastName');

                teacherCharts['teacherPaymentReferenceNid'] = teacherStats.get('teacherPaymentReferenceNid');
                teacherCharts['numberOfSubstitutions'] = teacherStats.get('numberOfSubstitutions');
                teacherCharts['numberOfSubstitutionsSubtractions'] = teacherStats.get('numberOfSubstitutionsSubtractions');
                teacherCharts['hourlyPaymentsAmount'] = teacherStats.get('hourlyPaymentsAmount');
                teacherCharts['numberOfHoursBefore'] = teacherStats.get('numberOfHoursBefore');
                teacherCharts['numberOfVouchers'] = teacherStats.get('numberOfVouchers');
                teacherCharts['contractsAmount'] = teacherStats.get('contractsAmount');
                teacherCharts['substitutionsAmount'] = teacherStats.get('substitutionsAmount');
                teacherCharts['substitutionsSubtractions'] = teacherStats.get('substitutionsSubtractions');
                teacherCharts['numberOfHourlyPayments'] = teacherStats.get('numberOfHourlyPayments');
                teacherCharts['hoursBeforeAmount'] = teacherStats.get('hoursBeforeAmount');
                teacherCharts['vouchersAmount'] = teacherStats.get('vouchersAmount');
                teacherCharts['activeStudents'] = teacherStats.get('activeStudents');
                teacherCharts['cancelledStudents'] = teacherStats.get('cancelledStudents');
                teacherCharts['pausedStudents'] = teacherStats.get('pausedStudents');
                teacherCharts['packagedStudents'] = teacherStats.get('packagedStudents');
                teacherCharts['teacherPaymentTotal'] = teacherStats.get('teacherPaymentTotal');
                teacherCharts['totalAmountCheck'] = teacherStats.get('totalAmountCheck');


                //*** groups
                teacherCharts['groupRatio'] = parseFloat((teacherStats.get('groupContracts') > 0 && teacherStats.get('numberOfStudents') > 0) ? (teacherStats.get('groupContracts') / teacherStats.get('numberOfStudents')).toCurrencyString() : 0);
                teacherCharts['groupIndex'] = parseFloat((teacherStats.get('groupLessons') > 0 && teacherStats.get('groupContracts') > 0) ? (teacherStats.get('groupContracts') / teacherStats.get('groupLessons')).toCurrencyString() : 0);
                //(GR * GI)
                teacherCharts['groupRating'] = Math.floor(parseFloat(teacherCharts['groupRatio']) * parseFloat(teacherCharts['groupIndex']) * 100) / 100;
                //contracts - group contracts *4
                teacherCharts['possibleOpenPlaces'] = (teacherStats.get('numberOfStudents') - teacherStats.get('groupContracts')) * 4;

                //*** finances calculated in BlockData
                // teacherCharts['paymentPerHour'] = teacherStats.get();
                // teacherCharts['possiblePerHour'] = teacherStats.get();
                // teacherCharts['possibleTotalPayment'] = teacherStats.get();

                // console.log(teacherCharts);
                teacherCharts['averageContractDurationPerSchool'] = teacherStats.get('averageContractDurationPerSchool');
                // teacherCharts['averageContractDurationPerSchoolPerInstrument'] = teacherStats.get('averageContractDurationPerSchoolPerInstrument');
                teacherCharts['numberOfTotalProspects'] = teacherStats.get('numberOfTotalProspects');
                teacherCharts['numberOfArchivedProspects'] = teacherStats.get('numberOfArchivedProspects');
                teacherCharts['numberOfSignedProspects'] = teacherStats.get('numberOfSignedProspects');
                teacherCharts['numberOfRealTotalProspects'] = teacherStats.get('numberOfRealTotalProspects');

                //school calculations on teachers
                if (parseInt(teacherCharts['teacherPaymentTotal']) > 0) {
                    numberOfPayedTeachers++;
                    totalTeacherPayment += Math.floor(parseFloat(teacherCharts['teacherPaymentTotal']) * 100) / 100;
                    totalTeacherContractPayments += Math.floor(parseFloat(teacherCharts['contractsAmount']) * 100) / 100;
                }
                allTeachers.push(teacherCharts)
                tCallback();
            });
        }, function() {

            var averageTeacherPayment = 0;
            var averageTeacherPaymentPerContract = 0;
            var profitMargin = 0;
            averageTeacherPayment = Math.floor((totalTeacherPayment / numberOfPayedTeachers) * 100) / 100;
            averageTeacherPaymentPerContract = Math.floor((totalTeacherContractPayments / self.get('totalNumberOfContracts')) * 100) / 100;
            profitMargin = parseFloat(self.get('averageContractValue')) - parseFloat(averageTeacherPaymentPerContract);

            self.set('numberOfPayedTeachers', numberOfPayedTeachers);
            self.set('totalTeacherPayment', totalTeacherPayment);
            self.set('totalTeacherContractPayments', totalTeacherContractPayments);
            self.set('averageTeacherPayment', averageTeacherPayment);
            self.set('averageTeacherPaymentPerContract', averageTeacherPaymentPerContract);
            self.set('profitMargin', profitMargin);
            self.set('teacherCharts', allTeachers);

            if (cb) cb.call(self)
        });

        // teachers.each(function(teacher, index) {


        //     allTeachers.push(teacherCharts);

        //     if (index + 1 === teachers.length) {

        //         var averageTeacherPayment = 0;
        //         var averageTeacherPaymentPerContract = 0;
        //         var profitMargin = 0;
        //         averageTeacherPayment = Math.floor((totalTeacherPayment / numberOfPayedTeachers) * 100) / 100;
        //         averageTeacherPaymentPerContract = Math.floor((totalTeacherContractPayments / self.get('totalNumberOfContracts')) * 100) / 100;
        //         profitMargin = parseFloat(self.get('averageContractValue')) - parseFloat(averageTeacherPaymentPerContract);

        //         self.set('numberOfPayedTeachers', numberOfPayedTeachers);
        //         self.set('totalTeacherPayment', totalTeacherPayment);
        //         self.set('totalTeacherContractPayments', totalTeacherContractPayments);
        //         self.set('averageTeacherPayment', averageTeacherPayment);
        //         self.set('averageTeacherPaymentPerContract', averageTeacherPaymentPerContract);
        //         self.set('profitMargin', profitMargin);

        //         console.log('[SchoolStatistics.getTeacherCharts] end');
        //         self.set('teacherCharts', allTeachers);

        //         // call end callback
        //         cb.call(self);

        //     }


        // });


        // this.set('teacherStatistics', JSON.stringify(teacherStatisticsCollection));
    },
    // getTeacherCharts: function(school, ymd, teachers,cb) {
    //     //TODO: make a teacher statistic object to put in the list
    //     //get all school teachers, calculate their stats
    //     // var teachers = school.getTeachers();
    //     var allTeachers = [];
    //     var self = this;
    //     // var teacherStatistics = eval('(' + this.get('teacherStatistics') + ')');
    //     // var teacherStatisticsCollection = Backbone.Collection.extend();
    //     // console.log('teachers');
    //     // console.log(teachers);

    //     var totalTeacherPayment  = 0;
    //     var totalTeacherContractPayments = 0;
    //     var averageTeacherPayment = 0;
    //     var averageTeacherPaymentPerContract = 0;
    //     var profitMargin = 0;
    //     var numberOfPayedTeachers = 0;

    //     teachers.each(function(teacher, index) {
    //         var teacherCharts = {};

    //         //TODO:
    //         //1. remove franchisee from teacher payment calculations
    //         //2. calculate VAT

    //         var teacherStats = new TeacherStatistics(school, teacher, ymd);

    //         teacherStats.on('ready',function(){
    //             console.log('ready teacher...');
    //             // var teachName = teacherStats.get('teacherName');
    //             //[teachName]
    //             teacherCharts['id'] = teacher.id || teacher.node.nid;
    //             //full teacher model
    //             teacherCharts['profile'] = teacher;
    //             teacherCharts['teacherProfile'] = teacher.get('teacherProfile');
    //             teacherCharts['numberOfConversions'] = teacherStats.get('numberOfConversions');
    //             teacherCharts['numberOfStudents'] = teacherStats.get('numberOfStudents');
    //             teacherCharts['numberOfCancellations'] = teacherStats.get('numberOfCancellations');
    //             teacherCharts['numberOfFinishingStudents'] = teacherStats.get('numberOfFinishingStudents');
    //             teacherCharts['groupLessons'] = teacherStats.get('groupLessons');
    //             teacherCharts['groupContracts'] = teacherStats.get('groupContracts');
    //             teacherCharts['openPlaces'] = teacherStats.get('openPlaces');
    //             teacherCharts['groupAverage'] = teacherStats.get('groupAverage');
    //             teacherCharts['cancellationAverage'] = teacherStats.get('cancellationAverage');
    //             teacherCharts['studentAverage'] = teacherStats.get('studentAverage');
    //             teacherCharts['averageContractDuration'] = teacherStats.get('averageContractDuration');
    //             teacherCharts['averageContractDurationActive'] = teacherStats.get('averageContractDurationActive');
    //             teacherCharts['averageContractDurationCancelled'] = teacherStats.get('averageContractDurationCancelled');
    //             teacherCharts['averageNumberOfRenewals'] = teacherStats.get('averageNumberOfRenewals');
    //             teacherCharts['averageNumberOfRenewalsActive'] = teacherStats.get('averageNumberOfRenewalsActive');
    //             teacherCharts['averageNumberOfRenewalsCancelled'] = teacherStats.get('averageNumberOfRenewalsCancelled');

    //             teacherCharts['firstName'] = teacher.get('firstName');
    //             teacherCharts['lastName'] = teacher.get('lastName');

    //             teacherCharts['teacherPaymentReferenceNid'] = teacherStats.get('teacherPaymentReferenceNid');
    //             teacherCharts['numberOfSubstitutions'] = teacherStats.get('numberOfSubstitutions');
    //             teacherCharts['numberOfSubstitutionsSubtractions'] = teacherStats.get('numberOfSubstitutionsSubtractions');
    //             teacherCharts['hourlyPaymentsAmount'] = teacherStats.get('hourlyPaymentsAmount');
    //             teacherCharts['numberOfHoursBefore'] = teacherStats.get('numberOfHoursBefore');
    //             teacherCharts['numberOfVouchers'] = teacherStats.get('numberOfVouchers');
    //             teacherCharts['contractsAmount'] = teacherStats.get('contractsAmount');
    //             teacherCharts['substitutionsAmount'] = teacherStats.get('substitutionsAmount');
    //             teacherCharts['substitutionsSubtractions'] = teacherStats.get('substitutionsSubtractions');
    //             teacherCharts['numberOfHourlyPayments'] = teacherStats.get('numberOfHourlyPayments');
    //             teacherCharts['hoursBeforeAmount'] = teacherStats.get('hoursBeforeAmount');
    //             teacherCharts['vouchersAmount'] = teacherStats.get('vouchersAmount');
    //             teacherCharts['activeStudents'] = teacherStats.get('activeStudents');
    //             teacherCharts['cancelledStudents'] = teacherStats.get('cancelledStudents');
    //             teacherCharts['pausedStudents'] = teacherStats.get('pausedStudents');
    //             teacherCharts['packagedStudents'] = teacherStats.get('packagedStudents');
    //             teacherCharts['teacherPaymentTotal'] = teacherStats.get('teacherPaymentTotal');
    //             teacherCharts['totalAmountCheck'] = teacherStats.get('totalAmountCheck');


    //             //*** groups
    //             teacherCharts['groupRatio'] = parseFloat((teacherStats.get('groupContracts') > 0 && teacherStats.get('numberOfStudents') > 0) ? (teacherStats.get('groupContracts') / teacherStats.get('numberOfStudents')).toCurrencyString() : 0);
    //             teacherCharts['groupIndex'] = parseFloat((teacherStats.get('groupLessons') > 0 && teacherStats.get('groupContracts') > 0) ? (teacherStats.get('groupContracts') / teacherStats.get('groupLessons')).toCurrencyString() : 0);
    //             //(GR * GI)
    //             teacherCharts['groupRating'] = Math.floor(parseFloat(teacherCharts['groupRatio']) * parseFloat(teacherCharts['groupIndex']) * 100) / 100;
    //             //contracts - group contracts *4
    //             teacherCharts['possibleOpenPlaces'] = (teacherStats.get('numberOfStudents') - teacherStats.get('groupContracts'))*4;

    //             //*** finances calculated in BlockData
    //             // teacherCharts['paymentPerHour'] = teacherStats.get();
    //             // teacherCharts['possiblePerHour'] = teacherStats.get();
    //             // teacherCharts['possibleTotalPayment'] = teacherStats.get();

    //             // console.log(teacherCharts);
    //             teacherCharts['averageContractDurationPerSchool'] = teacherStats.get('averageContractDurationPerSchool');
    //             // teacherCharts['averageContractDurationPerSchoolPerInstrument'] = teacherStats.get('averageContractDurationPerSchoolPerInstrument');
    //             teacherCharts['numberOfTotalProspects'] = teacherStats.get('numberOfTotalProspects');
    //             teacherCharts['numberOfArchivedProspects'] = teacherStats.get('numberOfArchivedProspects');
    //             teacherCharts['numberOfSignedProspects'] = teacherStats.get('numberOfSignedProspects');

    //             //school calculations on teachers
    //              if (parseInt(teacherCharts['teacherPaymentTotal'])>0) {
    //                 numberOfPayedTeachers++;
    //                 totalTeacherPayment += Math.floor(parseFloat(teacherCharts['teacherPaymentTotal']) * 100) / 100;
    //                 totalTeacherContractPayments += Math.floor(parseFloat(teacherCharts['contractsAmount']) * 100) / 100;
    //              }

    //             allTeachers.push(teacherCharts);

    //             if(index+1 === teachers.length) {

    //                 var averageTeacherPayment = 0;
    //                 var averageTeacherPaymentPerContract = 0;
    //                 var profitMargin = 0;
    //                 averageTeacherPayment = Math.floor((totalTeacherPayment/numberOfPayedTeachers)* 100) / 100;
    //                 averageTeacherPaymentPerContract = Math.floor((totalTeacherContractPayments/self.get('totalNumberOfContracts'))* 100) / 100;
    //                 profitMargin = parseFloat(self.get('averageContractValue')) - parseFloat(averageTeacherPaymentPerContract);

    //                 self.set('numberOfPayedTeachers', numberOfPayedTeachers);
    //                 self.set('totalTeacherPayment', totalTeacherPayment);
    //                 self.set('totalTeacherContractPayments', totalTeacherContractPayments);
    //                 self.set('averageTeacherPayment', averageTeacherPayment);
    //                 self.set('averageTeacherPaymentPerContract', averageTeacherPaymentPerContract);
    //                 self.set('profitMargin', profitMargin);

    //                 console.log('[SchoolStatistics.getTeacherCharts] end');
    //                 self.set('teacherCharts', allTeachers);

    //                 // call end callback
    //                 cb.call(self);

    //             }

    //         });
    //     });


    //     // this.set('teacherStatistics', JSON.stringify(teacherStatisticsCollection));


    // },
    //things to calculate
    getGroupRatio: function() { //All teachers/system
        var groupRatio = Math.floor(((this.get('groupContracts') > 0 && this.get('totalNumberOfContracts') > 0) ?
            (this.get('groupContracts') / this.get('totalNumberOfContracts')) : 0) * 100) / 100;
        this.set('groupRatio', groupRatio);
    },
    getGroupEfficiencyIndex: function() { //All teachers/system
        var groupEfficiencyIndex = Math.floor(((this.get('groupLessons') > 0 && this.get('groupContracts') > 0) ?
            (this.get('groupContracts') / this.get('groupLessons')) : 0) * 100) / 100;
        this.set('groupEfficiencyIndex', groupEfficiencyIndex);
    },

    // Student Contract Ratio = total number of contracts over total number of students
    getStudentContractRatio: function() {
        var studentContractRatio = Math.floor(((this.get('totalNumberOfContracts') > 0 && this.get('totalNumberOfStudents') > 0) ?
            (this.get('totalNumberOfContracts') / this.get('totalNumberOfStudents')) : 0) * 100) / 100;
        this.set('studentContractRatio', studentContractRatio);
    },
    getGroupRating: function() {
        var groupRating = Math.floor(this.get('groupRatio') * this.get('groupEfficiencyIndex') * 100) / 100;
        this.set('groupRating', groupRating);
    },
    getGrowth: function() {
        return Math.floor(((this.get('totalNumberOfContracts') - schoolStatisticsOfPreviousMonth.get('totalNumberOfContracts')) * 100 / schoolStatisticsOfPreviousMonth.get('totalNumberOfContracts')) * 100) / 100;
    },
    getPossibleOpenPlaces: function() {
        var possibleOpenPlaces = this.get('totalNumberOfContracts') - this.get('groupContracts') * 4;
        this.set('possibleOpenPlaces', possibleOpenPlaces);
    },
    calculateAverageTeacherPaymentPercentage: function() {
        //average teacher payment per contract / average contract value
        var averageTeacherPaymentPercentage = (100 * this.get('averageTeacherPaymentPerContract') / this.get('averageContractValue')).toCurrencyString();
        this.set('averageTeacherPaymentPercentage', averageTeacherPaymentPercentage);
    },
    calculateAverageContractValue: function() {
        var acv = Math.floor(((parseInt(this.get('totalSales') - this.get('applicationFees'), 10) / this.get('totalNumberOfContracts'))) * 100) / 100;
        this.set('averageContractValue', acv);
    },
    // getAverageTeacherPayment: function() {
    //     return Math.floor(averageTeacherPayment * 100) / 100;
    // },
    // getAverageTeacherContractPayment: function() {
    //     return Math.floor(averageTeacherContractPayment * 100) / 100;
    // },
    // getProfitMargin: function() {
    //     return Math.floor((parseInt(this.get('totalSales') - this.get('applicationFees'), 10) / this.get('totalNumberOfContracts')) * 100) / 100 - Math.floor(averageTeacherContractPayment * 100) / 100;
    // },


});


//    var schoolStatisticsCollection = new SchoolStatisticsCollection(school, date);
//    //schoolStatisticsCollection.url = schoolStatisticsCollection.url + 'schools_student_numbers?field_date_value_op=between&field_date_value[value][year]='+filters.now.year+'&field_date_value[value][month]='+filters.now.month+'&field_date_value[min][year]='+filters.min.year+'&field_date_value[min][month]='+filters.min.month+'&field_date_value[max][year]='+filters.max.year+'&field_date_value[max][month]='+filters.max.month+'&field_mms_nid='+filters.selectedSchools;
//    var response = schoolStatisticsCollection.fetch();
// this.SchoolId = response.field_mms_nid[0]['value'],
// this.SchoolName = response.field_school[0]['value'],
// this.date = response.field_date[0]['value'],
// this.NewStudents = field_new_students['0']['value'],
// this.Cancellations = field_cancellations['0']['value'],
// this.Paused = field_paused['0']['value'],
// this.Vouchers = field_vouchers['0']['value'],
// this.KlingKlong = field_klingklong['0']['value'],
// this.MMSProgram = field_mms_program['0']['value'],
// this.VibraProgram = field_vibra_program['0']['value'],
// this.PPProgram = field_pp_program['0']['value'],
// this.StudentsNumber = field_students_number['0']['value'],
// this.FinishingStudents = field_finishing_students['0']['value'],
// this.TshirtVouchers = field_tshirt_vouchers['0']['value'],
// this.HoursBefore = field_hours_before['0']['value'],
// this.SpecialDiscounts = field_special_discounts[0]['value'],


module.exports = SchoolStatistics;
