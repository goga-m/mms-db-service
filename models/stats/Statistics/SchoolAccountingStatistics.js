/*
 * Copyright 2012 MMS
 *
 */

/*
* Received: amount received on a month. So, only what was paid to the franchisee
* Receivable: Amount expected on a month, all money that should be paid to the franchisee on that month. 
* Overdue: The total open accounting of past.
* Teacher payments in a month
* 
* Franchise fee: 10% of Received 
* Net income:  Received minus the total expenses (Teacher payments + Franchisee fee) 
* 
* Monthly sales: money for this month only (whether paid or not)  = Received+ Receivable
* 
* PS: Cash Based Accounting should be done properly through our Receipts system in order for these numbers to come out right.
*/


var _ = require('underscore');
var Backbone = require('backbone');
var async = require('async');

var Model = require('../Model');
var Prospect = require('../Prospect');
var Contract = require('../Contract');
var Billing = require('../Billing');

SchoolAccountingStatistics = Backbone.Model.extend({
    initialize: function(school, ymd) {
        var self = this;
        self.school = school;
        this.clear();
        this.set({
            schoolName: school.get('title'),
            schoolId: school.id,
            snapshotDate: ymd
        });

        console.log('schoolAccounting id:',school.id,school.node.nid)
        /*Fetch necessary data*/
        async.parallel([
            function(callback){
                self.school.getContracts(function(res){
                    self.contracts = res;
                    callback();
                });
            },
            function(callback){
                self.school.accountingCashReceived(ymd,function(res){
                    self.accountingCashReceived = res;
                    callback();
                });
            },
            function(callback){
                self.school.accountingAutoPayReceived(ymd,function(res){
                    self.accountingAutoPayReceived = res;
                    callback();
                });
            },
            function(callback){
                self.school.accountingReceivable(ymd,function(res){
                    self.accountingReceivable = res;
                    callback();
                });
            },
            function(callback){
                self.school.accountingOverdue(ymd,function(res){
                    self.accountingOverdue = res;
                    callback();
                });
            },
            function(callback){
                self.school.getOpenAccounting(ymd,function(res){
                    self.getOpenAccounting = res;
                    callback();
                });
            },
            function(callback){
                self.school.getMonthlyAccounting(ymd,function(res){
                    self.getMonthlyAccounting = res;
                    callback();
                });
            }
        ],
        // optional callback
        function(err, results){
            console.log('SchoolAccountingStatistics results');
            console.log('contracts: ',self.contracts.length)
            console.log('accountingCashReceived: ',self.accountingCashReceived)
            console.log('accountingAutoPayReceived: ',self.accountingAutoPayReceived.length)
            console.log('accountingReceivable: ',self.accountingReceivable.length);
            console.log('accountingOverdue: ',self.accountingOverdue.length);
            console.log('getOpenAccounting: ',self.getOpenAccounting.length);
            console.log('getMonthlyAccounting: ',self.getMonthlyAccounting.length);

            self.calculate(school,ymd);
        });


     // console.debug('[Models.SchoolAccountingStatistics]');
     // console.debug(this.get('snapshotDate')+' totalSales:'+this.get('totalSales')+' outstandingAmounts:'+this.get('outstandingAmounts'));

    },
    calculate: function(school, ymd) {
        var self = this, salesMMS = 0, salesVibra = 0, salesKK = 0, totalSales = 0, outstandingAmounts = 0, applicationFees = 0;

        var acv = 0; //average contract value (latest 28-9-2013)
        var mmsContractsSales = 0;
        var vibraContractsSales = 0;
        var kkContractsSales = 0;
        var contractsSales = 0;
        var mmsApplicationFees = 0;
        var vibraApplicationFees = 0;
        var kkApplicationFees = 0;
        var mmsReceivable = 0;
        var vibraReceivable = 0;
        var kkReceivable = 0;
        var mmsOpenAccountingTM = 0;
        var vibraOpenAccountingTM = 0;
        var kkOpenAccountingTM = 0;

        var outstandingAmounts = 0;
        var mmsOverdue = 0;
        var vibraOverdue = 0;
        var kkOverdue = 0;

        var expectedAmount = 0;
        var receivedAmount = 0;
        var outstandingAmount = 0;

        
        var c = self.contracts;
        var cashReceived = self.accountingCashReceived;

        var autoPayReceived = 0;
        var autoPayReceivedBillsCollection = self.accountingAutoPayReceived;
        _(autoPayReceivedBillsCollection.models).each(function(bill, index){
            autoPayReceived = parseFloat(autoPayReceived) + parseFloat(bill.get('amount'));
        });

        var received = cashReceived + autoPayReceived;
        // amount received on a month. So, only what was paid to the franchisee
/*        var received = 0;
        var receivedBillsCollection = school.accountingReceived(ymd);
        _(receivedBillsCollection.models).each(function(bill, index){
            received = parseFloat(received) + parseFloat(bill.get('amount'));
        });
*///                    console.log('received= '+received);
        // Amount expected on a month, all money that should be paid to the franchisee on that month. 
        var receivable = 0;
        var receivableBillsCollection = self.accountingReceivable;
        receivableBillsCollection.each(function(bill, index){
            console.log('bill',(c.map(function(item){return item.id})),bill.get('contractReferenceId')    );
            // var contract = c._byId[bill.get('contractReferenceId')];
            var contract = c.filter(function(item){
                return (item.id == bill.get('contractReferenceId'))
            })[0]

            var contractProgram = contract.contractProgram();
            receivable = parseFloat(receivable) + parseFloat(bill.get('balance'));
            if (contractProgram == 'MMS') { mmsReceivable = parseFloat(mmsReceivable) + parseFloat(bill.get('balance')); }
            if (contractProgram == 'Vibra') { vibraReceivable = parseFloat(vibraReceivable) + parseFloat(bill.get('balance')); }
            if (contractProgram == 'KK') { kkReceivable = parseFloat(kkReceivable) + parseFloat(bill.get('balance')); }

        });
        // The total open accounting of past.
        var overdue = 0;
        var overdueBillsCollection = self.accountingOverdue;
        _(overdueBillsCollection.models).each(function(bill, index){
            // var contract = c._byId[bill.get('contractReferenceId')];

            var contract = c.filter(function(item){
                return (item.id == bill.get('contractReferenceId'))
            })[0]

            var contractProgram = contract.contractProgram();
            overdue = parseFloat(overdue) + parseFloat(bill.get('balance'));
            if (contractProgram == 'MMS') { mmsOverdue = parseFloat(mmsOverdue) + parseFloat(bill.get('balance')); }
            if (contractProgram == 'Vibra') { vibraOverdue = parseFloat(vibraOverdue) + parseFloat(bill.get('balance')); }
            if (contractProgram == 'KK') { kkOverdue = parseFloat(kkOverdue) + parseFloat(bill.get('balance')); }
        });
        var totalSalesWithoutExceptions = 0;
        var numberOfContractsWithoutExceptions = 0;

        //n a month
        var teacherPayments = school.accountingTeacherPayments(ymd);
        // 10% of Received 
        var franchiseFee = 0;
        //  Received minus the total expenses (Teacher payments + Franchisee fee) 
        var netIncome = 0;
        // money for this month only (whether paid or not)  = Received+ Receivable
        var monthlySales = 0;

        var openBills = self.getOpenAccounting;
        var monthlyBills = self.getMonthlyAccounting;
        var monthyAccounting = 0;

        _(openBills.models).each(function(bill, index){
            // var contract = c._byId[bill.get('contractReferenceId')];
            var contract = c.filter(function(item){
                return (item.id == bill.get('contractReferenceId'))
            })[0]
            var contractProgram = contract.contractProgram();

            outstandingAmounts = parseFloat(outstandingAmounts) + parseFloat(bill.get('balance'));

            if (contractProgram == 'MMS') { mmsOpenAccountingTM = parseFloat(mmsOpenAccountingTM) + parseFloat(bill.get('balance')); }
            if (contractProgram == 'Vibra') { vibraOpenAccountingTM = parseFloat(vibraOpenAccountingTM) + parseFloat(bill.get('balance')); }
            if (contractProgram == 'KK') { kkOpenAccountingTM = parseFloat(kkOpenAccountingTM) + parseFloat(bill.get('balance')); }

            // bill.get('type');
            // bill.get('paymentStatus');
            // bill.get('notes');
            // bill.get('paymentDate');  
        });
        _(monthlyBills.models).each(function(bill, index){
            //use contractReferenceId the see if the contract is MMS, Vibra, KK 
            // var contract = c._byId[bill.get('contractReferenceId')];
            var contract = c.filter(function(item){
                return (item.id == bill.get('contractReferenceId'))
            })[0]            
            var contractProgram = contract.contractProgram();
            if (contractProgram == 'Vibra') { salesVibra = parseFloat(salesVibra) + parseFloat(bill.get('amount')); }
            else if (contractProgram == 'KK') { salesKK = parseFloat(salesKK) + parseFloat(bill.get('amount')); }
            else /*if (contractProgram == 'MMS')*/ { salesMMS = parseFloat(salesMMS) + parseFloat(bill.get('amount')); }

            if (bill.get('type')=='One time handling fee'){
                if (contractProgram == 'Vibra') { vibraApplicationFees = parseFloat(vibraApplicationFees) + parseFloat(bill.get('amount')); }
                else if (contractProgram == 'KK') { kkApplicationFees = parseFloat(kkApplicationFees) + parseFloat(bill.get('amount')); }
                else /*if (contractProgram == 'MMS')*/ { mmsApplicationFees = parseFloat(mmsApplicationFees) + parseFloat(bill.get('amount')); }
                applicationFees = parseFloat(applicationFees) + parseFloat(bill.get('amount'));
            } else {
                if (contractProgram == 'Vibra') { vibraContractsSales = parseFloat(vibraContractsSales) + parseFloat(bill.get('amount')); }
                else if (contractProgram == 'KK') { kkContractsSales = parseFloat(kkContractsSales) + parseFloat(bill.get('amount')); }
                else /*if (contractProgram == 'MMS')*/ { mmsContractsSales = parseFloat(mmsContractsSales) + parseFloat(bill.get('amount')); }
                contractsSales = parseFloat(contractsSales) + parseFloat(bill.get('amount'));
            }
            totalSales = parseFloat(totalSales) + parseFloat(bill.get('amount'));
            console.log("ACV: paymentFrequency="+contract.get('paymentFrequency')+" type="+bill.get('type'));
            if (bill.get('type')!='One time handling fee'){
                totalSalesWithoutExceptions = parseFloat(totalSalesWithoutExceptions) + parseFloat(bill.get('amount'));
                numberOfContractsWithoutExceptions++;
                console.log("in for ACV");
            }  
        });
 
        monthlySales = received+receivable+applicationFees;
        var franchiseeFeePercentage = school.get('franchise_fee')==0?10/100:school.get('franchise_fee');
console.log("ACV="+Math.floor(((parseInt(totalSalesWithoutExceptions, 10) / numberOfContractsWithoutExceptions)) * 100) / 100);
        

        self.set({
            'acv' : Math.floor(((parseInt(totalSalesWithoutExceptions, 10) / numberOfContractsWithoutExceptions)) * 100) / 100,
            //sales
            'mmsContractsSales' : mmsContractsSales.toFixed(2),
            'vibraContractsSales' : vibraContractsSales.toFixed(2),
            'kkContractsSales' : kkContractsSales.toFixed(2),
            'contractsSales' : contractsSales.toFixed(2),

            'mmsApplicationFees' : mmsApplicationFees.toFixed(2),
            'vibraApplicationFees' : vibraApplicationFees.toFixed(2),
            'kkApplicationFees' : kkApplicationFees.toFixed(2),

            'mmsReceivable' : mmsReceivable.toFixed(2),
            'vibraReceivable' : vibraReceivable.toFixed(2),
            'kkReceivable' : kkReceivable.toFixed(2),

            'mmsOverdue' : mmsOverdue.toFixed(2),
            'vibraOverdue' : vibraOverdue.toFixed(2),
            'kkOverdue' : kkOverdue.toFixed(2),

            'mmsOpenAccountingTM' : mmsOpenAccountingTM.toFixed(2),
            'vibraOpenAccountingTM' : vibraOpenAccountingTM.toFixed(2),
            'kkOpenAccountingTM' : kkOpenAccountingTM.toFixed(2),

            'salesMMS': salesMMS.toFixed(2),
            'salesVibra': salesVibra.toFixed(2),
            'salesKK': salesKK.toFixed(2),
            'totalSales': totalSales.toFixed(2),
            'outstandingAmounts': outstandingAmounts.toFixed(2),
            'applicationFees': applicationFees.toFixed(2),
            'monthyAccounting' : monthyAccounting.toFixed(2),
            'cashReceived' : cashReceived.toFixed(2),
            'autoPayReceived' : autoPayReceived.toFixed(2),
            'received' : received.toFixed(2),
            'receivable' : receivable.toFixed(2),
            'overdue' : overdue.toFixed(2),
            // 'teacherPayments' : teacherPayments.toFixed(2),
            'franchiseFee' : (franchiseeFeePercentage* received).toFixed(2),
            //'netIncome' : netIncome.toFixed(2),
            'monthlySales' : monthlySales.toFixed(2),
        });
        console.log('SchoolAccounting statistics is ready')
        self.trigger('ready');

    }

});

module.exports = SchoolAccountingStatistics;