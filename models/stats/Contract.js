/*
 * Copyright 2012 MMS
 *
 */

var _ = require('underscore');
var Backbone = require('backbone');
var request = require('request');

require('datejs');
var TimeSpan = require('timespan').TimeSpan;

var Model = require('./Model');

Contract = Model.extend({
  init : function() {
    this.populate();
  },
  populate : function() {
  
    // All node attributes from server are inside this.node object.
    // Attribute Mapping.
    this.set({
        title                       : this.node.title,
        tarifId                     : this.node.field_tarif ? this.node.field_tarif[0]['nid']:null,
        tarifHQ                     : this.lookupTarif(this.node.title),
        hoursBeforeContract         : this.node.field_hours_before_contract ? parseInt('0'+this.node.field_hours_before_contract[0]['value'],10):null,
        voucherId                   : this.node.field_contract_voucher ? this.node.field_contract_voucher[0]['nid']:null,
        status                      : this.node.field_contract_status ? this.node.field_contract_status[0]['value']:null,

        startingDate                : this.node.field_contract_starting_date ? this.transformDate(this.node.field_contract_starting_date[0]['value']):null,
        endingDate                  : this.node.field_contract_end_date ? this.transformDate(this.node.field_contract_end_date[0]['value']):null,
        nextCancellationDate        : this.node.field_contract_next_cancel_date ? this.transformDate(this.node.field_contract_next_cancel_date[0]['value']):null,
        overrideEndDate             : this.node.field_end_override_date ? this.transformDate(this.node.field_end_override_date[0]['value']):null,
        pauseStartDate              : this.node.field_contract_pause_startdate ? this.transformDate(this.node.field_contract_pause_startdate[0]['value']):null,
        outOfPauseDate              : this.node.field_contract_outofpause_date ? this.transformDate(this.node.field_contract_outofpause_date[0]['value']):null,

        course                      : this.node.field_contract_course ? this.node.field_contract_course[0]['value']:null,
        tshirtVoucherRedeemed       : this.node.field_tshirt_voucher ? this.node.field_tshirt_voucher[0]['value']:null,
        studentId                   : this.node.field_contact_student ? this.node.field_contact_student[0]['nid']:null,
        teacherId                   : this.node.field_contract_teacher ? this.node.field_contract_teacher[0]['nid']:null,
        instrumentsId               : this.node.field_contract_instruments ? this.node.field_contract_instruments[0]['nid']:null,
        instruments                 : this.node.field_contract_instruments ? this.lookupInstrument(this.node.field_contract_instruments[0]['nid']):null,
        lessonsStarting             : this.node.field_lessons_starting_date ? this.node.field_lessons_starting_date[0]['value']:null,
        credits                     : this.node.field_contract_credits ? this.node.field_contract_credits[0]['value']:null,
        specialDiscountId           : this.node.field_special_discount ? this.node.field_special_discount[0]['nid']:null,
        paymentType                 : this.node.field_payment_type ? this.node.field_payment_type[0]['value']:null,
        applicationFee              : this.node.field_handling_charge ? parseFloat('0'+this.node.field_handling_charge[0]['value']): 0,
        paymentFrequency            : this.node.field_payment_frequency ? this.node.field_payment_frequency[0]['value']:null,
        overridenContractPrice      : this.node.field_contract_price_override ? this.node.field_contract_price_override[0]['value']:null,
        teacherPaymentNotAffected   : this.node.field_affect_tp ? this.node.field_affect_tp[0]['value']:null,
        notes                       : this.node.field_contract_notes ? this.node.field_contract_notes[0]['value']:null,
        contractPartnerFirstName    : this.node.field_guardian_fname ? this.node.field_guardian_fname[0]['value']:null,
        contractPartnerLastName     : this.node.field_guardian_lname ? this.node.field_guardian_lname[0]['value']:null,
        contractPartnerTelephone    : this.node.field_telephone ? this.node.field_telephone[0]['value']:null,
        contractPartnerMobile       : this.node.field_mobile ? this.node.field_mobile[0]['value']:null,
        contractPartnerEmail        : this.node.field_email ? this.node.field_email[0]['value']:null,
        contractPartnerGender       : this.node.field_gender ? this.node.field_gender[0]['value']:null,
        contractPartnerDateOfBirth  : this.node.field_dob ? this.node.field_dob[0]['value']:null,
        collectionProcess           : this.node.field_collection_process ? this.node.field_collection_process[0]['value']:null
    });

    var days = new TimeSpan(Date.parseExact(this.get('endingDate'), 'yyyyMMdd') - Date.parseExact(this.get('startingDate'), 'yyyyMMdd')).days;
    var dur = Math.round(days/30);
    this.set('duration', (dur <= 0)?1:dur);
    this.id = this.node.nid;

    // console.debug('[Models.Contract] Contract Model(%d) Populated',this.id);
  },
  // getBills: function () {
  //   // If contracts are in memory escape fetching from server.
  //   if(this.collectionBills) {
  //     return this.collectionBills;
  //   }
  //   var bills = this.fetch({
  //     callback: 'get_contract_bills',
  //     contract_nid: this.id
  //   });
  //   this.collectionBills = this.addCollection({
  //     model: Billing,
  //     items: _.values(bills)
  //   });
  //   return this.collectionBills;
  // },
  wasCancelledOn: function (fromDate,toDate,cb) {
    //if contract is cancelled or to cancel then
    var ret = false;
    if ((this.get('status')=='TO CANCEL') || (this.get('status')=='CANCELLED')){ 
        // get the contract history entry of the cancellation
        // var contractHistoryEntries = this.fetch({
        //   callback: 'get_contract_history_status',
        //   contract_nid: this.id,
        //   status : 'TO CANCEL'
        // });
        var self = this;

        request({
          headers: {'content-type' : 'application/x-www-form-urlencoded'},
          method : 'POST',
          uri : 'https://www.db-mms.com/erp/server/api',
          form : {
            callback: 'get_contract_history_status',
            contract_nid: this.id,
            status : 'TO CANCEL'
          }
        },function(error, response, body){
          if (!error && response.statusCode == 200) {
            var data = JSON.parse(body);
            var contractHistoryEntries = _.values(data.data);
            // console.log('contractHistoryEntries',contractHistoryEntries);
            if( _.isFunction(cb) ){
                //and check if it was between these dates
                if (contractHistoryEntries) {
                    _(contractHistoryEntries).each(function(entry, index) {
                        var cancelDate = /*self.transformDate(entry.field_contract_history_dates[0]['value']);//*/Date.parseExact(self.transformDate(entry.field_contract_history_dates[0]['value']),'yyyyMMdd').moveToFirstDayOfMonth().toString("yyyyMM dd").replace(' ', '');
                        // if ((fromDate <= cancelDate) && (toDate >= cancelDate)){
                        //     ret = true;
                        // }
                        if (parseInt(fromDate,10) == parseInt(cancelDate,10)){
                            ret = true;
                        }
                    });
                }
                console.log('ret :',ret);
                cb.call(self,ret);
            }
          }

        });
    } else {
        if(_.isFunction(cb)) {
            cb.call(self,ret);
        }
    }
  },
  lookupInstrument: function(id){
    var lookupTable = {
        39 : 'Drums',
        40 : 'Guitar',
        41 : 'Keyboard',
        44 : 'Vocals',
        45 : 'Bass',
        370 : 'Kling Klong',
        1516 : 'Saxophon',
        3573 : 'DJ',
        3574 : 'Band',
        4591 : 'Music Technology',
        4596 : 'Music Theory'
        };
    return lookupTable[id];
  },
  lookupTarif: function(tarif){
    var mmsPackage = '';
    if (tarif.indexOf("comfort")>0) {
        mmsPackage = "comfort";
    }
    if (tarif.indexOf("community")>0) {
        mmsPackage = "community";
    }
    if (tarif.indexOf("custom 30")>0) {
        mmsPackage = "custom 30";
    }
    if (tarif.indexOf("custom 45")>0) {
        mmsPackage = "custom 45";
    }
    if (tarif.indexOf("custom 60")>0) {
        mmsPackage = "custom 60";
    }
    if (tarif.indexOf("custom plus 30")>0) {
        mmsPackage = "custom plus 30";
    }
    if (tarif.indexOf("custom plus 45")>0) {
        mmsPackage = "custom plus 45";
    }
    if (tarif.indexOf("Groupon")>0) {
        mmsPackage = "Groupon";
    }
    if (tarif.indexOf("Kling Klong")>0) {
        mmsPackage = "Kling Klong";
    }
    if (tarif.indexOf("old comfort")>0) {
        mmsPackage = "old comfort";
    }
    if (tarif.indexOf("old custom 30")>0) {
        mmsPackage = "old custom 30";
    }
    if (tarif.indexOf("old custom 45")>0) {
        mmsPackage = "old custom 45";
    }
    if (tarif.indexOf("Professional Program")>0) {
        mmsPackage = "Professional Program";
    }
    if (tarif.indexOf("V 10 / 30")>0) {
        mmsPackage = "V 10 / 30";
    }
    if (tarif.indexOf("V 10 / 45")>0) {
        mmsPackage = "V 10 / 45";
    }
    if (tarif.indexOf("very old comfort")>0) {
        mmsPackage = "very old comfort";
    }
    if (tarif.indexOf("very old custom 30")>0) {
        mmsPackage = "very old custom 30";
    }
    if (tarif.indexOf("very old custom 45")>0) {
        mmsPackage = "very old custom 45";
    }
    return mmsPackage;

  },
  contractProgram:  function(){
    if (this.get('instruments') == 'Kling Klong'){ return 'KK'; }
    if (this.get('instruments') == 'DJ' || this.get('instruments') == 'Music Technology'){ return 'Vibra'; }
    return 'MMS';
  },
  getGender: function(school){
    var student = school.getStudents()._byId[this.get('studentId')];
    return (student)?student.get('gender'):'Male';
  },
  getAge: function(school){
    var student = school.getStudents()._byId[this.get('studentId')];
    return (student)?student.getAge():15;
  },
  getReferee: function(school){
    var student = school.getStudents()._byId[this.get('studentId')];
    var refById = (student)?student.get('referencedByStudentId'):null;
    return refById;
    // if (refById) {
    //     var referrer = students._byId[refById];
    //     // Be sure is not null.
    //     if(referrer) { 
    //         var rbs = referrer.get('firstName')+' '+referrer.get('lastName'); 
    //         //init as 0 on first time
    //         if(referencedByStudents[rbs]) {
    //             referencedByStudents[rbs] =  parseInt(referencedByStudents[rbs],10) + 1;
    //         }
    //         if(!referencedByStudents[rbs]) {
    //             referencedByStudents[rbs] = 1;
    //         }   
    //     }
    // }
  },
  getMarketingSource: function(school){
    var student = school.getStudents()._byId[this.get('studentId')];
    return (student)?student.get('marketingSource'):'Internet';
  },
  getOtherMarketingSource: function(school){
    var student = school.getStudents()._byId[this.get('studentId')];
    return (student)?student.get('otherMarketingSource'):'';
  },

});


module.exports = Contract;

