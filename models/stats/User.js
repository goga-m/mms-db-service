/*
 * Copyright 2012 MMS
 *
 */

var _ = require('underscore');
var Backbone = require('backbone');

var Profile = require('./Profile');
var SchoolsCollection = require('./Collections/SchoolsCollection');

User = Model.extend({
  initialize: function() {

    var data = this.fetch({
      callback: 'initialize_application'
    });

    this.set({
      country: data.country,
      isHQ: data.isHQ,
      language: data.language,
      selectedSchool: data.selected_school,
      id: data.uid,
      roles: data.roles
    });
    console.log('user initialized');
    console.log(data);
    
    this.profile = new Profile(data.profile);
    var schools = new SchoolsCollection(data.schools);
    this.schools = schools.update(data.schools);

  },
  getFranchisees: function() {
    if (this.collectionFranchisees) {
      return this.collectionFranchisees;
    }
    var franchisees = {};
    franchisees = this.fetch({
      callback: 'get_all_franchisees',
    });
    this.collectionFranchisees = this.addCollection({
      model: Profile,
      items: _.values(franchisees)
    });
    //console.log(this.collectionFranchisees);
    return this.collectionFranchisees;
  },

  // queryStatistics : function(schools,from,to){
  //   var self = this;
  //   var selectedSchools = [];
  //   var dateRange = [];

  //   //  Get schools by id.
  //    _(schools).each(function(school_id,index){
  //     // console.log(school_id);
  //     selectedSchools.push(self.schools._byId[school_id]);
  //   });

  //   // Get Date range by month.
  //   var min = Date.parseExact(from, 'yyyyMMdd').clearTime();
  //   var max = Date.parseExact(to, 'yyyyMMdd').clearTime();
  //   while(min <= max) {
  //     // convert each date into ymd format.
  //     dateRange.push(new Date(min).toString("yyyyMM dd").replace(' ', ''));
  //     min.add(1).months();
  //   }
  //   var statsPerSchool = [];
  //   _(dateRange).each(function(ymd,index){
  //      _(selectedSchools).each(function(school,index){
  //         statsPerSchool.push(school.getStatistics(ymd)[0]);
  //      });
  //   });
  //   return statsPerSchool;
  // }

});

module.exports = User;