/*
 * Copyright 2012 MMS
 *
 */
var _ = require('underscore');
var Backbone = require('backbone');

var Model = require('./Model');

Billing = Model.extend({
  init: function () {
    this.Populate();
  }, 
  Populate: function () {

    // All node attributes from server are inside this.node object.
    //  Attribute Mapping.
    if (this.node.field_contract_bill_ref) {
      this.set({ 
        contractReferenceId : this.node.field_contract_bill_ref[0]['nid'],
        issueDate : this.node.field_contract_bill_issue_date?this.transformDate(this.node.field_contract_bill_issue_date[0]['value']):null,
        type :  this.node.field_contract_billing_type?this.node.field_contract_billing_type[0]['value']:null,
        amount : this.node.field_contract_bill_amount?this.node.field_contract_bill_amount[0]['value']:null,
        balance : this.node.field_contract_bill_balance?this.node.field_contract_bill_balance[0]['value']:null,
        receiptAmount : this.node.field_contract_bill_amount?this.node.field_contract_bill_amount[0]['value']:null,
        // -- Payment status can be: Outstanding, OK, Overdue, Debt Collection
        paymentStatus : this.node.field_bill_payment_status?this.node.field_bill_payment_status[0]['value']:null,
        notes : this.node.field_contract_bill_notes?this.node.field_contract_bill_notes[0]['value']:null,
        paymentDate : this.node.field_contract_bill_issue_date?this.transformDate(this.node.field_contract_bill_issue_date[0]['value']):null
      });
    } else {
      this.id = this.node.nid;
      this.set({ 
        contractReferenceId : this.node.contractReferenceId,
        issueDate : this.transformDate(this.node.issueDate),
        type :  this.node.type,
        amount : this.node.amount,
        balance : this.node.balance,
        receiptAmount : this.node.receiptAmount,
        // -- Payment status can be: Outstanding, OK, Overdue, Debt Collection
        paymentStatus : this.node.paymentStatus,
        notes : this.node.notes,
        paymentDate : this.transformDate(this.node.paymentDate),
      });
 
    }
    //I consider that the last update that has happened to the bill node was paying it off
    if (this.get('paymentStatus')=='ok' && this.get('balance')==0){
      this.set('paymentDate', this.transformDate(this.node.updated));
    }
     // console.debug('[Models.Billing] Billing Model Populated');
     // console.debug(this.get('type')+' issueDate:'+this.get('issueDate')+' amount:'+this.get('amount')+' balance:'+this.get('balance'));

  }

});

module.exports = Billing;