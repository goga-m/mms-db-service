/*
 * Copyright 2012 MMS
 *
 */

var _ = require('underscore');
var Backbone = require('backbone');

var Model = require('./Model');

Teacher = Model.extend({
  init :function() {

    this.set({
          name: profile.get('title'),
          profileId: profile.id
      });

    this.set({
      status : this.node.field_teacher_status?this.node.field_teacher_status[0]['value']:null,
      teachesIn  : this.node.field_teacher_mms?this.node.field_teacher_mms[0]['value']:null,
      isVATLiable  : this.node.field_vat_viable?this.node.field_vat_viable[0]['value']:null,
      payPerHour : this.node.field_teacher_pp_hourly_pay?this.node.field_teacher_pp_hourly_pay[0]['value']:null,
      smallBio : this.node.field_teacher_bio?this.node.field_teacher_bio[0]['value']:null,
      //TODO: teacher instruments lookup required just like contracts
      instruments  : this.node.field_teacher_instruments?this.node.field_teacher_instruments:null,
      trainingStatus : this.node.field_teacher_training_status?this.node.field_teacher_training_status[0]['value']:null,
      files  : this.node.field_teacher_files?this.node.field_teacher_files[0]['value']:null
    });
  },
  getLessons: function(school,ymd){
    return this.get('lessons');
  },
  getTrials: function(school,ymd){
    return this.get('lessons');
  },
  getStudents: function(school,ymd){
    return this.get('lessons');
  },
  getContracts: function(school,ymd){
    return this.get('lessons');
  },
  getProspects: function(school,ymd){
    return this.get('lessons');
  },
  getLogs: function(school,ymd){
    return this.get('lessons');
  },
  getPPGroups:function(school,ymd){},
  getKKGroups:function(school,ymd){},
  getGroupLessons:function(school,ymd){}
});


module.exports = Teacher;