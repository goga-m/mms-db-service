/*
 * Copyright 2012 MMS
 *
 */

var _ = require('underscore');
var Backbone = require('backbone');

var Model = require('./Model');

Alert = Backbone.Model.extend({
  initialize: function(options) {
    this.set({
      from: options.from?options.from:'System',
      to: options.to, //should be an array that could be [ all, school, franchisees, teachers, students, country ]
      category: options.category?options.category:'notification', //[notification/task/feature]
      severity: options.severity?options.severity:'information',  //[information/important]
      message: options.message,      
      lifespan: options.lifespan?options.lifespan:1, //1 day is default
      created :  this.transformDate(Date.today()),//"yyyyMM dd").replace(' ', ''),
      updated :  this.transformDate(Date.today()),
    });
    this.store();
  },
  delete: function() {
  },
  store: function() {
    //store this in the 
    if (this.get('to')){
      //store this in the profile->field_alerts[0]['value']  
    } else {
      //
    }
  },
  read: function() {
  }
});

module.exports = Alert;