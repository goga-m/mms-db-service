/*
 * Copyright 2012 MMS
 *
 */

var _ = require('underscore');
var Backbone = require('backbone');

var Model = require('./Model');

LiveFeed = Backbone.Model.extend({
  initialize: function(options) {

    this.set({
      schools : options.schools?options.schools:app.mySchools.models,
      created :  Date.today().getTime()//"yyyyMM dd").replace(' ', ''),
    });
    this.getStartingPoint();
    this.update();
  },
  getStartingPoint: function() {
    var numberOfContractsBoM = 0;
    var numberOfNewContractsBoM = 0;
    var numberOfCancellationsBoM = 0;
    var numberOfTrialsBoM = 0;
    var numberOfProspectsBoM = 0;
    var numberOfLessonsBoM = 0;
    var self = this;
    //get the previous month stats for this.schools
    _(this.get('schools')).each(function(school, index) {
      var lastMonthYmd = Date.today().moveToFirstDayOfMonth().add(-1).months().toString("yyyyMM dd").replace(' ', '');
      var lastMonthStats = school.getStatistics(lastMonthYmd)[0];
      numberOfContractsBoM = numberOfContractsBoM + lastMonthStats.get('totalNumberOfContracts');
      numberOfNewContractsBoM = numberOfNewContractsBoM + lastMonthStats.get('numberOfStartingContracts');
      numberOfCancellationsBoM = numberOfCancellationsBoM + lastMonthStats.get('numberOfCanceledContracts');
      numberOfTrialsBoM = numberOfTrialsBoM + lastMonthStats.get('numberOfTrialLessons');
      numberOfProspectsBoM = numberOfProspectsBoM + lastMonthStats.get('numberOfProspects');
      // numberOfLessonsBoM = numberOfLessonsBoM + lastMonthStats.get('numberOfLessonsBoM');
    });

    self.set({
      'numberOfContractsBoM' : numberOfContractsBoM,
      'numberOfNewContractsBoM' : numberOfNewContractsBoM,
      'numberOfCancellationsBoM' : numberOfCancellationsBoM,
      'numberOfTrialsBoM' : numberOfTrialsBoM,
      'numberOfProspectsBoM' : numberOfProspectsBoM
      // 'numberOfLessonsBoM' : numberOfLessonsBoM;      
    });
  },
  diff : function(val1,val2, invert) {
    val1 = parseInt(val1,10);
    val2 = parseInt(val2,10);
    invert = invert || false ;
    var pcDiff =  (val1 - val2) / ( (val1 + val2) / 2 )  * 100;
    var absDiff = val1 - val2;

    if(isNaN(pcDiff)) {
        return false;
    }
    else {
        pcDiff =  pcDiff.toFixed(1) + '%';
    }

    sign = '';
    var arrow = '';
      if(absDiff === 0) {
          sign = 'neutral';
      }
    
    if(invert === false) {
        
      if(absDiff < 0) {
          sign = 'negative';
          arrow = '<i class="icon-arrow-down"></i>';
      }
      if(absDiff > 0) {
          arrow = '<i class="icon-arrow-up"></i>';
          sign = 'positive';
      }
    }

    if(invert === true) {
      if(absDiff > 0) {
          sign = 'negative';
          arrow = '<i class="icon-arrow-up"></i>';
      }
      if(absDiff < 0) {
          arrow = '<i class="icon-arrow-down"></i>';
          sign = 'positive';
      }
    }

    return {
      sign:sign,
      pcDiff : pcDiff,
      absDiff : absDiff,
      arrow: arrow
    };

  },
  update: function() {
    var self = this;
    var numberOfContracts = self.get('numberOfContracts');
    var numberOfNewContracts = self.get('numberOfNewContracts');
    var numberOfCancellations = self.get('numberOfCancellations');
    var numberOfTrials = self.get('numberOfTrials');
    var numberOfProspects = self.get('numberOfProspects');
    // var numberOfLessons = self.get('numberOfLessons');
    var mmsNids = '';
    _(self.get('schools')).each(function(school, index) {
      mmsNids = '\'' + school.id + '\'' + (mmsNids!==''?', '+mmsNids:'');
    });
    mmsNids = '('+mmsNids+')';
    //callback to get these numbers 
    liveNumbers = self.fetch({
      callback: 'get_live_feed',
      mms_nids_where: mmsNids
    });

    // console.log(liveNumbers.count_contracts_query);
    self.set({
      numberOfContracts: liveNumbers.numberOfContracts,
      numberOfNewContracts: liveNumbers.numberOfNewContracts,
      numberOfCancellations: liveNumbers.numberOfCancellations,
      numberOfTrials: liveNumbers.numberOfTrials,
      numberOfProspects: liveNumbers.numberOfProspects,
      // numberOfLessons: liveNumbers.numberOfLessons,
      updated :  Date.today().getTime()
    });

  },
    fetch: function (options) {
    var response_data = false;
    $.ajax({
      url: '/app/callback',
      data: {
        'data': options
      },
      type: 'POST',
      async: false,
      success: function (response) {
        if(response.status != 'success') {
          console.debug('Error while fetching from Server:' + response.status);
          return;
        }
        response_data = response.data; // Succesfull fetch.
      }
    });
    return response_data;
  }
});

module.exports = LiveFeed;