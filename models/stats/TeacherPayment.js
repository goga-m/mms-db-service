/*
 * Copyright 2012 MMS
 *
 */

var _ = require('underscore');
var Backbone = require('backbone');

var Model = require('./Model');

TeacherPayment = Model.extend({
  init: function () {
    this.Populate();
  }, 
  Populate: function () {
      this.clear(); 

    // All node attributes from server are inside this.node object.
    //  Attribute Mapping.
    // if (this.node.field_teacher_payment_ref) {
    //   this.set({ 
    //     // contractReferenceId : this.node.field_contract_bill_ref[0]['nid'],
    //     // issueDate : this.node.field_contract_bill_issue_date?this.transformDate(this.node.field_contract_bill_issue_date[0]['value']):null,
    //     // type :  this.node.field_contract_TeacherPayment_type?this.node.field_contract_TeacherPayment_type[0]['value']:null,
    //     // amount : this.node.field_contract_bill_amount?this.node.field_contract_bill_amount[0]['value']:null,
    //     // balance : this.node.field_contract_bill_balance?this.node.field_contract_bill_balance[0]['value']:null,
    //     // // -- Payment status can be: Outstanding, OK, Overdue, Debt Collection
    //     // paymentStatus : this.node.field_bill_payment_status?this.node.field_bill_payment_status[0]['value']:null,
    //     // notes : this.node.field_contract_bill_notes?this.node.field_contract_bill_notes[0]['value']:null,
    //     // paymentDate : this.node.field_contract_bill_issue_date?this.transformDate(this.node.field_contract_bill_issue_date[0]['value']):null
    //   });
    // } else {
      this.id = this.node.nid;
      this.set({ 
        nid : this.node.nid,
        paymentCategory : this.node.paymentCategory,
        paymentReferenceNid : this.node.paymentReferenceNid,
        postDate : this.transformDate(this.node.postDate),
        updatedDate : this.transformDate(this.node.updatedDate),
        totalAmountCheck : this.node.totalAmountCheck,
        dateOfPayment : this.node.dateOfPayment/*this.transformDate(this.node.dateOfPayment)*/,
        paymentNotes : this.node.paymentNotes,
        teacherId : this.node.teacherId,
        vat : this.node.vat,
        contractReferenceId : this.node.contractReferenceId,
        amount : this.node.amount,
        lessonSubstitutedNid : this.node.lessonSubstitutedNid,
        paymentItemNotes : this.node.paymentItemNotes,

      });
    // }

     // console.log('[Models.TeacherPayment] TeacherPayment Model Populated');
     // console.log(this);
     // console.debug('['+this.id+'] 'this.get('paymentCategory')+' issueDate:'+this.get('dateOfPayment')+' tpCheckAmount:'+this.get('totalAmountCheck')+' amount:'+this.get('amount'));

  }

}); 


module.exports = TeacherPayment;