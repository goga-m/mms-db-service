var _ = require('underscore');
var Backbone = require('backbone');
var request = require('request');

Model = Backbone.Model.extend({
  initialize: function (options) {
    this.initNode(options);
    this.init(options);
  },
  initNode: function () {
    //  Fetch node from server.
    if(this.has('id')) {
      this.node = this.fetch({
        callback: 'fetch_node',
        nid: this.get('nid')
      });
      // if(this.has('flag')) {
      //   this.flag = this.get('flag');
      // }
    }
    //  Node is already loaded. Just Populate.
    else {
      this.node = _.clone(this.attributes);
    }
    this.clear(); // clear model.attributes from node's stuff.
    this.id = this.node.nid;
  },
  // fetch: function (options) {
  //   var response_data = false;
  //   $.ajax({
  //     url: '/app/callback',
  //     data: {
  //       'data': options
  //     },
  //     type: 'POST',
  //     async: false,
  //     success: function (response) {
  //       if(response.status != 'success') {
  //         console.debug('Error while fetching from Server:' + response.status);
  //         return;
  //       }
  //       response_data = response.data; // Succesfull fetch.
  //     }
  //   });
  //   return response_data;
  // },
  fetch: function (options) {
    console.log('fetch options',options);
    var responseData = false;

    request({
      headers: {'content-type' : 'application/x-www-form-urlencoded'},
      method : 'POST',
      uri : 'https://www.db-mms.com/erp/server/api',
      async:false,
      form : {
        callback: 'erp_api_test_data',
      }
    },function(error, response, body){
      

      if (!error && response.statusCode == 200) {
        responseData = body;
        console.log('fetched',responseData);
      }

    });

    return responseData;
  },
  save: function (options) {
    var response_data = false;
    $.ajax({
      url: '/app/callback',
      data: {
        'data': options
      },
      type: 'POST',
      success: function (response) {
        if(response.status != 'success') {
          console.debug('Error while fetching from Server:' + response.status);
          return;
        }
        response_data = response.data; // Succesfull fetch.
      }
    });
    return response_data;
  },
  saveStatistics: function (options) {
    if(this.has('statistics')) {
      this.save({
        callback: 'save_profile_statistics',
        nid: this.id,
        statistics: this.get('statistics')
      });
      console.debug('[Models.Model] Saved Statistics.');
    }
    console.debug('[Models.Model] Error: Statistics does not exist.');
  },
  addCollection: function (options) {
    var temp_collection;
    if(options.model) {
      temp_collection = Backbone.Collection.extend({
        model: options.model
      });
    } else {
      temp_collection = Backbone.Collection.extend();
    }
    return new temp_collection(options.items);
  },
  transformDate: function (dateString){
    return new Date(dateString).toString("yyyyMM dd").replace(' ','');
  }

});

module.exports = Model;