/*
 * Copyright 2012 MMS
 *
 */

var _ = require('underscore');
var Backbone = require('backbone');

var Model = require('./Model');

Lesson = Model.extend({
  init : function() {
    this.populate();
  },
  populate : function() {
  
    // All node attributes from server are inside this.node object.
    // Attribute Mapping.
    this.set({
        title : this.node.title,    
        isTrial  : (this.node.type == 'lesson') ? true:false,
        isSerial  : (this.node.type == 'lesson') ? false:true,
        type : this.node.field_lesson_is_pp?this.node.field_lesson_is_pp[0]['value']:null,
        instance : 0, //the instance of a serial lesson - this acts as a fast index to the lesson
        date: this.node.field_lesson_dates?this.transformDate(this.node.field_lesson_dates[0]['value']):null,
        prospectId: this.node.field_lesson_student_profile?this.node.field_lesson_student_profile[0]['nid']:null,
        //TODO: there are more than one teachers in a lesson. Maybe use a collection for that
        teacherId: this.node.field_lesson_teacher?this.node.field_lesson_teacher[0]['nid']:null,
        room: this.node.field_lesson_room?this.node.field_lesson_room[0]['nid']:null,
        trialStudents: this.node.field_lesson_st_profile?this.node.field_lesson_st_profile[0]['nid']:null, 
        //TODO: there are more than one students in a lesson. Maybe use a collection for that
        students: this.node.field_serial_lesson_student?this.node.field_serial_lesson_student[0]['nid']:null,
        studentsGroup: this.node.field_student_group?this.node.field_student_group[0]['nid']:null,
        note : this.node.field_serial_lesson_note?this.node.field_serial_lesson_note[0]['value']:null,
        instrument : this.node.field_lesson_instrument?this.node.field_lesson_instrument[0]['nid']:null,
        possibleEndDate : this.node.field_lesson_end_date?this.transformDate(this.node.field_lesson_end_date[0]['value']):null,
        openPlaces: this.node.field_lesson_open_places?this.node.field_lesson_open_places[0]['value']:null,
        created :  this.transformDate(new Date(this.node.created*1000)),//"yyyyMM dd").replace(' ', ''),
        updated :  this.transformDate(new Date(this.node.updated*1000)),
    });
        this.set('creationMonth', Date.parseExact(this.get('created'),'yyyyMMdd').moveToFirstDayOfMonth().toString("yyyyMM dd").replace(' ', ''));
      
 //   console.debug('[Models.Lesson] Lesson Model(%d), created on '+this.get('created')+' '+this.get('creationMonth')+' Populated',this.id);
  }
});

module.exports = Lesson;