/*
 * Copyright 2012 MMS
 *
 */

var _ = require('underscore');
var Backbone = require('backbone');

var Model = require('./Model');

Prospect = Model.extend({
  init : function() {
    this.populate();
  },
  populate : function() {
  
    // All node attributes from server are inside this.node object.
    // Attribute Mapping.
    this.set({
        title : this.node.title,
        schoolId : this.node.field_related_franchises?this.node.field_related_franchises[0]['nid']:null,    
        status : this.node.field_prospect_student_status ? this.node.field_prospect_student_status[0]['value']:null,
        firstName : this.node.field_fname ? this.node.field_fname[0]['value']:null,
        lastName : this.node.field_lname ? this.node.field_lname[0]['value']:null,
        gender : this.node.field_gender ? this.node.field_gender[0]['value']:null,
        telephone : this.node.field_telephone ? this.node.field_telephone[0]['value']:null,
        mobile : this.node.field_mobile ? this.node.field_mobile[0]['value']:null,
        email : this.node.field_email ? this.node.field_email[0]['value']:null,
        //notes : this.node.field_prospect_notes ? this.node.field_prospect_notes[0]['value']:null,
        created :  this.transformDate(new Date(this.node.created*1000)),//"yyyyMM dd").replace(' ', ''),
        updated :  this.transformDate(new Date(this.node.updated*1000)),
    });
        this.set('creationMonth', Date.parseExact(this.get('created'),'yyyyMMdd').moveToFirstDayOfMonth().toString("yyyyMM dd").replace(' ', ''));
    //TODO: calculate time in prospect mode before changing status
    //TODO: see if he is signed and the day the student was created
    //TODO: trial information and teacher information from trial
    // console.debug('[Models.Prospect] Prospect Model(%d) Populated',this.id);
  },
  timeToGetTrial: function() {
    //find if he got a trial: null he didn't get a trial
    var schools = window.app.mySchools;
    var school = schools.get(this.schoolId);
    // console.log ('Prospect gets his school');
    // console.log(school);
    var trial = school.getTrials().where({'prospectId':this.id});
    //calculate the difference in days between that trial creation date and the prospect creation date
    if (trial.length>0){
        var days = new TimeSpan(Date.parseExact(trial[0].get('created'), 'yyyyMMdd') - Date.parseExact(this.get('created'), 'yyyyMMdd')).days;
        var dur = Math.round(days/30);
        return (dur <= 0)?1:dur;                
    } else {
        return null;
    }
  },
  timeToConvert: function() { //null: is not converted
    //If he is not converted return null
    if (this.get('status').indexOf("Signed")<0) {
        return null;
    } else {    //else return the last updated date - creation date 
        var days = new TimeSpan(Date.parseExact(this.get('updated'), 'yyyyMMdd') - Date.parseExact(this.get('created'), 'yyyyMMdd')).days;
        var dur = Math.round(days/30);
        return (dur <= 0)?1:dur;        
    }
  },
  convertedOn: function() {
    //return the date he was converted on
    //If he is not converted return 0
    if (this.get('status').indexOf("Signed")<0) {
        return null;
    } else {    //else return the last updated date - creation date 
        return this.get('updated');
    }
  },
  teacherTriedWith: function() {
    //return the teacher he had trial with
    var schools = window.app.mySchools;
    var school = schools.get(this.schoolId);
    // console.log ('Prospect gets his school');
    // console.log(school);
    var trial = school.getTrials().where({'prospectId':this.id});
    //calculate the difference in days between that trial creation date and the prospect creation date
    if (trial.length>0){
        return trial[0].get('teacherId');                
    } else {
        return null;
    }
  }
});

module.exports = Prospect;