/*
 * Copyright 2012 MMS
 *
 */

var _ = require('underscore');
var Backbone = require('backbone');
var request = require('request');
var async = require('async');

var Model = require('./Model');

Profile = Model.extend({
    init: function(options) {
        this.set({
            firstName: this.node.field_fname[0]['value'],
            lastName: this.node.field_lname[0]['value'],
            address: this.node.location,
            telephone: this.node.field_telephone[0]['value'],
            mobile: this.node.field_mobile[0]['value'],
            gender: this.node.field_gender ? this.node.field_gender[0]['value'] : null,
            dob: this.node.field_dob ? this.node.field_dob[0]['value'] : null,
            role: this.node.field_user_type ? this.node.field_user_type[0]['value'] : null,
            photo: this.node.field_profile_photo ? this.node.field_profile_photo[0] : null,
            newsletterPermission: this.node.field_newsletter_permission ? this.node.field_newsletter_permission[0]['value'] : null,
            language: this.node.field_language ? this.node.field_language[0]['value'] : null,
            //Addresses :  Locations module form.
            marketingSource: this.node.field_client_source ? this.node.field_client_source[0]['value'] : null,
            referencedByStudentId: this.node.field_referenced_by_student ? this.node.field_referenced_by_student[0]['nid'] : null,
            otherMarketingSource: this.node.field_client_source_other ? this.node.field_client_source_other[0]['value'] : null,
            prospectHistory: this.node.field_prospect_link ? this.node.field_prospect_link[0]['nid'] : null,
            teacherProfile: '',
            // franchiseeProfile : this.node.field_backref_8d2ef60c3a83d875d0 ? this.node.field_backref_8d2ef60c3a83d875d0[0]['nid']:null,

            // alerts : this.node.field_alerts[0]['value'] || '',
            // statistics : this.node.field_statistics[0]['value'],
            relatedSchools: this.node.field_related_franchises,
            isStaffIn: this.node.field_staff_in_mms,
            uid: this.node.uid,
            created: this.transformDate(new Date(this.node.created * 1000)), //"yyyyMM dd").replace(' ', ''),
            updated: this.transformDate(new Date(this.node.updated * 1000)),
            nid : this.node.nid
        });
        this.set('creationMonth', Date.parseExact(this.get('created'), 'yyyyMMdd').moveToFirstDayOfMonth().toString("yyyyMM dd").replace(' ', ''));
        this.id = this.node.nid;

    },
    // alerts: function() {
    //   var response = this.fetch({
    //     callback: 'get_teachers',
    //     mms_nid: this.id
    //   });
    //   return response;
    // },
    // roleInSchool: function(schoolId) {
    //   return this.fetch({
    //     callback: 'get_roles_in_school',
    //     nid: this.id,
    //     mms_nid: schoolId
    //   });
    // },
    schoolsOverview: function() {
        return this.get('statistics').get('overview');
    },
    getTeacherGroupLessonStats: function(school, fromDate, toDate, cb) {
        // var glstats = {};
        // if (this.collectionOpenPlaces) {
        //   return this.collectionOpenPlaces;
        // }
        // glstats = this.fetch({
        //   callback: 'get_group_lesson_stats',
        //   teacher_nid: this.id,
        //   mms_nid: school.id,
        //   from_date: fromDate,
        //   to_date: toDate
        // });
        // return glstats;



        var glstats = {};
        var self = this;

        if (this.collectionOpenPlaces) {
            if (_.isFunction(cb)) {
                cb.call(self, self.collectionOpenPlaces);
            }
            return;
        }

        request({
            headers: { 'content-type': 'application/x-www-form-urlencoded' },
            method: 'POST',
            uri: 'https://www.db-mms.com/erp/server/api',
            form: {
                callback: 'get_group_lesson_stats',
                teacher_nid: this.id,
                mms_nid: school.id,
                from_date: fromDate,
                to_date: toDate
            }
        }, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                var data = JSON.parse(body);
                self.collectionOpenPlaces = data.data;

                if (_.isFunction(cb)) {
                    cb.call(self, self.collectionOpenPlaces);
                }
            }

        });


    },
    contracts: function(school) {
        return school.getContracts().where({
            'studentId': this.id
        });
    },
    endDate: function(school) {
        var endDate;
        _(this.contracts(school).models).each(function(contract, index) {
            if (parseInt(endDate, 10) > parseInt(contract.get('endDate'), 10)) {
                endDate = contract.get('endDate');
            }
        });

        return endDate;
    },
    getTeacherProfile: function(school, cb) {
        // if (this.get('teacherProfile') == '') {
        //   this.set('teacherProfile', this.fetch({
        //     callback: 'get_teacher_profile',
        //     nid: this.id,
        //     mms_nid: school.id
        //   }));
        // }
        var self = this;

        if (this.get('teacherProfile') !== '') {
            if (_.isFunction(cb)) {
                cb.call(self, this.get('teacherProfile'));
            }
            return;
        }

        request({
            headers: { 'content-type': 'application/x-www-form-urlencoded' },
            method: 'POST',
            uri: 'https://www.db-mms.com/erp/server/api',
            form: {
                callback: 'get_teacher_profile',
                nid: this.id,
                mms_nid: school.id
            }
        }, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                var data = JSON.parse(body);
                self.set('teacherProfile', data.data);

                if (_.isFunction(cb)) {
                    cb.call(self, self.get('teacherProfile'));
                }
            }

        });

    },
    getAverageContractDuration: function(options, cb) {
        var self = this;
        options = options || {};

        if (!self.node || !self.node.nid) {
            console.log('no node id for getAverageContractDuration. Aborting');
            return;
        }
        var displayId = (options.status === 'cancelled') ? 'page_2' : 'page_1';


        var views_arguments = options.views_argunents || { teacher_nid: self.node.nid };

        request({
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            uri: 'https://www.db-mms.com/erp/server/api',
            form: {
                callback: 'erp_drupal_view',
                view: 'erp_contract_duration',
                display_id: displayId,
                arguments: views_arguments
            }
        }, function(error, response, body) {

            if (!error && response.statusCode == 200) {
                var data = JSON.parse(body);
                var formatted = _(data.nodes).map(function(node) {
                    return node.node;
                });
                var contracts = new Backbone.Collection(formatted);

                var average = 0;
                var renewals = 0;

                contracts.each(function(c) {
                    average += parseFloat(c.get('contract_duration'));
                    if (!_.isNaN(parseFloat(c.get('number_of_renewals')))) {
                        renewals += parseFloat(c.get('number_of_renewals'));
                    }
                });

                average = average / contracts.length;
                renewals = renewals / contracts.length;

                average = _.isNaN(average) ? 0 : average;
                renewals = _.isNaN(renewals) ? 0 : renewals;

                if (_.isFunction(cb)) {
                    cb.call(self, parseFloat(average.toFixed(2)), parseFloat(renewals.toFixed(2)), contracts);
                }
            }
        });
    },
    getAverageContractDurationPerSchool: function(options, callback) {
        var self = this;
        var schoolNids = _(self.get('relatedSchools')).pluck('nid');
        var output = [];

        var method = options.method ||  'getAverageContractDuration';

        async.eachLimit(schoolNids, 1, function(nid, cb) {
            self[method]({
                views_argunents: {
                    mms_nid: nid,
                    teacher_nid: self.node.nid
                }
            }, function(durationAverage) {
                output.push({
                    school_nid: nid,
                    average: durationAverage
                })
                cb();
            })
        }, function() {
            if (_.isFunction(callback)) callback(output);
        })
    },
    getAverageContractDurationPerInstrument: function(options, cb) {
        var self = this;
        options = options || {};

        if (!self.node || !self.node.nid) {
            console.log('no node id for getAverageContractDurationPerInstrument. Aborting');
            return;
        }

        var displayId = (options.status === 'cancelled') ? 'page_2' : 'page_1';
        var views_arguments = options.views_argunents || { teacher_nid: self.node.nid };

        request({
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            uri: 'https://www.db-mms.com/erp/server/api',
            form: {
                callback: 'erp_drupal_view',
                view: 'erp_contract_duration',
                display_id: displayId,
                arguments: views_arguments
            }
        }, function(error, response, body) {

            if (!error && response.statusCode == 200) {
                var data = JSON.parse(body);
                var formatted = _(data.nodes).map(function(node) {
                    return node.node;
                });

                var contracts = new Backbone.Collection(formatted);
                var grouped = contracts.groupBy('contract_instruments');
                var output = {};

                _.keys(grouped).forEach(function(instrument) {
                    if (instrument != 'undefined') {

                        var contractsPerInstrument = grouped[instrument];
                        var average = 0;
                        var renewals = 0;

                        _(contractsPerInstrument).each(function(c) {
                            average += parseFloat(c.get('contract_duration'));
                            if (!_.isNaN(parseFloat(c.get('number_of_renewals')))) {
                                renewals += parseFloat(c.get('number_of_renewals'));
                            }
                        });

                        average = average / contractsPerInstrument.length;
                        average = _.isNaN(average) ? 0 : average;
                        output[instrument] = parseFloat(average.toFixed(2))
                            // renewals = renewals / contracts.length;

                    }
                });
                if (cb) cb(output);
            } else {
                if (cb) cb([]);
            }
        });
    },
    getTeacherContracts: function(school) {
        return school.getContracts().where({
            'teacherId': this.id
        });
    },
    getAge: function() {
        var now = 2013;
        if (!this.get('dob')) {
            return 18;
        }
        var dob = parseInt(this.get('dob').substring(0, 4), 10);
        // console.log('age'+now+' '+dob);
        return now - dob;
    },
    getTeacherProspectsByStatus: function(options, cb) {
        var self = this;
        options = options || {};

        if (!self.node || !self.node.nid) {
            console.log('no node id for getTotalProspects. Aborting');
            return;
        }

        var displayId = 'page_4';
        if (options.status === 'signed') {
            displayId = 'page_1'
        } else if (options.status === 'archived') {
            displayId = 'page_2'
        }

        request({
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            uri: 'https://www.db-mms.com/erp/server/api',
            form: {
                callback: 'erp_drupal_view',
                view: 'erp_teacher_conversion_stats',
                display_id: displayId,
                arguments: {
                    mms_nid: options.mms_nid,
                    teacher_nid : self.node.nid,
                    month : options.month
                }
            }
        }, function(error, response, body) {

            if (!error && response.statusCode == 200) {
                var data = JSON.parse(body);
                var formatted = _(data.nodes).map(function(node) {
                    return node.node;
                });

                // filter by teacher
                var myProspects = _(formatted).filter(function(item) {
                    return item.teacher_nid === self.node.nid
                });
                var prospects = new Backbone.Collection(myProspects);

                if (_.isFunction(cb)) cb(error, prospects);
            } else {
                var statusCode = (response && response.statusCode) ? '[Error:' + response.statusCode + ']' : undefined;
                if (_.isFunction(cb)) cb(error || statusCode, [])
            }

        });
    },

});


module.exports = Profile;
