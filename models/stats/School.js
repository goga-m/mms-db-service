/*
 * Copyright 2012 MMS
 *
 */
//fdffdasfd

var _ = require('underscore');
var Backbone = require('backbone');
var request = require('request');

var Model = require('./Model');
var Prospect = require('./Prospect');
var TeacherPayment = require('./TeacherPayment');
var Lesson = require('./Lesson');
var Billing = require('./Billing');
var SchoolStatistics = require('./Statistics/SchoolStatistics');
var SchoolAccountingStatistics = require('./Statistics/SchoolAccountingStatistics');

School = Model.extend({
    init: function(options) {

        if (options.node) {
            this.node = options.node;
        }

        this.Populate();
    },

    Populate: function() {
        //  All node attributes from server are inside this.node object.
        //  Attribute Mapping.
        this.set({
            fax: (this.node.field_franchise_fax) ? this.node.field_franchise_fax[0]['value'] : null,
            title: this.node.title,
            emails: (this.node.field_franchise_emails) ? this.node.field_franchise_emails[0]['value'] : null,
            country: this.node.country,
            owned: 1, //calculate whether the school is owned -> extract franchise fee 10% vs 100%
            franchise_fee: (this.node.field_fee_percentage) ? this.node.field_fee_percentage[0]['value'] / 100 : 10 / 100,
            //if user is school.franchisee then this is 1 else it is 0.1
            mobiles: (this.node.field_franchise_mobiles) ? this.node.field_franchise_mobiles[0]['value'] : null,
            bankName: (this.node.field_bank_name) ? this.node.field_bank_name[0]['value'] : null,
            franchisee: /*franchisee*/ (this.node.field_franchise_owner) ? this.node.field_franchise_owner[0]['nid'] : null,
            telephone: (this.node.field_franchise_telephones) ? this.node.field_franchise_telephones[0]['value'] : null,
            addresses: this.node.locations,
            vataMount: (this.node.field_vat_amount) ? this.node.field_vat_amount[0]['value'] : null,
            bankRouting: (this.node.field_bank_routing_number) ? this.node.field_bank_routing_number[0]['value'] : null,
            schoolImages: this.node.field_franchise_images,
            receiptHeader: (this.node.field_receipt_header) ? this.node.field_receipt_header[0]['value'] : null,
            accountNumber: (this.node.field_bank_account_number) ? this.node.field_bank_account_number[0]['value'] : null,
            exemptFromVAT: (this.node.field_vat_exempt) ? this.node.field_vat_exempt[0]['value'] : null,
            availableTarifs: this.node.field_available_tarifs,
            fatherFranchise: (this.node.field_father_franchise) ? this.node.field_father_franchise[0]['nid'] : null,
            fileAttachments: this.node.files,
            masterFranchise: (this.node.field_is_master) ? this.node.field_is_master[0]['value'] : null,
            exemptFromVibraVAT: (this.node.field_vat_exempt_vibra) ? this.node.field_vat_exempt_vibra[0]['value'] : null,
            statistics: (this.node.field_statistics && this.node.field_statistics[0]['value']) ? this.node.field_statistics[0]['value'] : '[]',
            //  statistics : '[]',
            accountingStatistics: '[]',
            schoolName: this.node.title
        });

        this.id = this.node.nid;
    },
    getStatistics: function(ymd, cb) {

        try {
            if (!ymd) {
                console.log('no date(ymd) provided');
                return false;
            }
            // console.log('gettings statistics for ' + ymd + ' for ' + this.id);
            //create somewhere statistics collection.
            this.statistics = this.statistics || new Backbone.Collection();

            var stats = this.statistics.where({
                snapshotDate: ymd
            });

            if (stats.length > 0) {
                if (_.isFunction(cb)) {
                    cb.call(this, stats);
                }
                return;
            }

            //Create statistics model for this school and snapshotDate.
            var newStatistic = new SchoolStatistics(this, ymd);
            this.statistics.add(newStatistic);

            var self = this;
            newStatistic.on('ready', function() {
                if (_.isFunction(cb)) {
                    cb.call(self, [newStatistic]);
                }
            });

            // if(_.isFunction(cb)) {
            //   cb.call(this,[newStatistic]);
            // }
            return [newStatistic];

        } catch (err) {
            console.log('Calculate statistics parse error', err);
            return false;
        }
    },
    createStatistics: function(ymd, cb, error) {
        var self = this;

        if (!ymd) {
            if (_.isFunction(cb)) {
                cb.call(self, null, 'No ymd proviced');
            }
            return;
        }
        //Create statistics model for this school and snapshotDate.
        var newStatistic = new SchoolStatistics(this, ymd);

        newStatistic.on('ready', function() {
            if (_.isFunction(cb)) {
                cb.call(self, newStatistic);
            }
        });
        newStatistic.on('error', function(errormsg) {
            if (_.isFunction(error)) error.call(self, newStatistic);
        });
    },
    getSchoolGroupLessonStats: function(fromDate, toDate, cb) {
        var glstats = {};
        var self = this;

        if (this.schoolGroupLessonStats) {
            if (_.isFunction(cb)) {
                cb.call(self, self.schoolGroupLessonStats);
            }
            return;
        }

        request({
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            uri: 'https://www.db-mms.com/erp/server/api',
            form: {
                callback: 'get_school_group_lesson_stats',
                mms_nid: this.id,
                from_date: fromDate,
                to_date: toDate
            }
        }, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                var data = JSON.parse(body);
                self.schoolGroupLessonStats = data.data;

                if (_.isFunction(cb)) {
                    cb.call(self, self.schoolGroupLessonStats);
                }
            }

        });

    },
    updateMonthlyStatistics: function(ymd, statistic) {
        console.log('updating monthly statistics for ' + this.get('schoolName') + ' on ' + ymd);
        this.statistics = this.statistics || new Backbone.Collection();
        var stat;

        // When calling without statistic object parameter.
        if (!statistic) {
            stat = new SchoolStatistics(this, ymd, true);
            stat.save();
            this.statistics.add(stat);
            return;
        }
        //Check if stat exists in cache. That means if this.statistics.where(...) returned non empty array.
        // calculate and save statisticObject;
        stat = new SchoolStatistics(this, ymd, true); // true is for update_flag;
        console.log('statistics re calculated');
        console.log(stat);
        console.log('saving updated statistic...');
        stat.save({
            date: statistic.date,
            statid: statistic.statid,
            mmsnid: statistic.mmsnid || this.get('schoolId'),
            date: statistic.date,
            update_flag: 0,
            data: JSON.stringify(stat)
        });

        //Check if ymd is now.
        var now = Date.today().moveToFirstDayOfMonth().toString("yyyyMM dd").replace(' ', '');

        if (ymd === now) {
            var nextMonth = Date.today().add(1).months().moveToFirstDayOfMonth().toString("yyyyMM dd").replace(' ', '');
            stat.save({
                date: nextMonth,
                mmsnid: statistic.mmsnid || this.get('schoolId'),
                date: statistic.date,
                update_flag: 0,
                data: JSON.stringify(stat)
            });
        }

        this.statistics.add(stat);
    },
    getAccountingStatistics: function(ymd, cb) {
        var self = this;
        var monthlyStatistic = new SchoolAccountingStatistics(this, ymd);
        //console.log(monthlyStatistic);
        monthlyStatistic.on('ready', function() {
            if (_.isFunction(cb)) {
                cb.call(self, monthlyStatistic);
                return;
            };
        });

    },
    getTeacherPayments: function(teacherId, ymd, cb) {
        // var teacherPayments;
        // if (this.collectionTeacherPayments) {
        //   teacherPayments = this.collectionTeacherPayments.where({
        //     'teacherId': teacherId,
        //     'dateOfPayment': ymd
        //   });
        //   if (teacherPayments.length > 0) {
        //     return teacherPayments;
        //   }
        // }

        // teacherPayments = this.fetch({
        //   callback: 'get_teacher_payments',
        //   mms_nid: this.id,
        //   ymd: ymd
        // });

        // this.collectionTeacherPayments = self.addCollection({
        //   model: TeacherPayment,
        //   items: _.values(teacherPayments)
        // });

        // console.log("TPs returned from db");
        // console.log(this.collectionTeacherPayments);

        // teacherPayments = this.collectionTeacherPayments.where({
        //   'teacherId': teacherId,
        //   'dateOfPayment': ymd
        // });

        // return teacherPayments;

        var self = this;

        if (this.collectionTeacherPayments) {

            var teacherPayments = self.collectionTeacherPayments.where({
                'teacherId': teacherId,
                'dateOfPayment': ymd
            });

            if (_.isFunction(cb)) {
                cb.call(self, teacherPayments);
            };

            return teacherPayments;
        }



        request({
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            uri: 'https://www.db-mms.com/erp/server/api',
            form: {
                callback: 'get_teacher_payments',
                mms_nid: self.id,
                ymd: ymd
            }
        }, function(error, response, body) {
            console.log(error, body);
            if (!error && response.statusCode == 200) {
                var data = JSON.parse(body);
                var formatted = _.values(data.data);

                // self.collectionTeacherPayments.reset(formatted);
                self.collectionTeacherPayments = new Backbone.Collection(formatted, {
                    model: TeacherPayment
                });
                if (_.isFunction(cb)) {



                    var teacherPayments = self.collectionTeacherPayments.where({
                        'teacherId': teacherId,
                        'dateOfPayment': ymd
                    });
                    cb.call(self, teacherPayments);
                };
            }

        });

    },
    getTeachers: function(cb) {
        // if (this.collectionTeachers) {
        //   return this.collectionTeachers;
        // }
        // // Fetch from server.
        // var teachers = this.fetch({
        //   callback: 'get_teachers',
        //   mms_nid: this.id
        // });
        // this.collectionTeachers = self.addCollection({
        //   model: Profile,
        //   items: _.values(teachers)
        // });
        // return this.collectionTeachers;


        var self = this;

        if (this.collectionTeachers) {
            if (_.isFunction(cb)) {
                cb.call(self, self.collectionTeachers);
            };
            return;
        }

        this.collectionTeachers = new Backbone.Collection([], {
            model: Profile
        });


        request({
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            uri: 'https://www.db-mms.com/erp/server/api',
            form: {
                callback: 'get_teachers',
                mms_nid: self.id
            }
        }, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                var data = JSON.parse(body);
                var formatted = _.values(data.data);

                self.collectionTeachers = new Backbone.Collection(formatted, {
                    model: Profile
                });

                if (_.isFunction(cb)) {
                    cb.call(self, self.collectionTeachers);
                };
            }

        });

    },
    getContracts: function(cb) {
        var self = this;

        if (this.collectionContracts) {
            if (_.isFunction(cb)) {
                cb.call(self, self.collectionContracts);
            };
            return this.collectionContracts;
        }

        console.log('getting contacts', self.id)
        request({
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            uri: 'https://www.db-mms.com/erp/server/api',
            form: {
                callback: 'erp_api_get_contract_nodes',
                mms_nid: self.id
            }
        }, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                var data = JSON.parse(body);
                var formatted = _.values(data.data);

                self.collectionContracts = new Backbone.Collection(formatted, {
                    model: Contract
                });
                if (_.isFunction(cb)) {
                    cb.call(self, self.collectionContracts);
                }
            }
        });
    },
    getAverageContractDuration: function(options, cb) {
        var self = this;
        options = options || {};

        if (!self.node || !self.node.nid) {
            console.log('no node id for getAverageContractDuration. Aborting');
            return;
        }
        var displayId = (options.status === 'cancelled') ? 'page_2' : 'page_1';

        request({
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            uri: 'https://www.db-mms.com/erp/server/api',
            form: {
                callback: 'erp_drupal_view',
                view: 'erp_contract_duration',
                display_id: displayId,
                arguments: {
                    mms_nid: self.node.nid,
                }
            }
        }, function(error, response, body) {

            if (!error && response.statusCode == 200) {
                var data = JSON.parse(body);
                var formatted = _(data.nodes).map(function(node) {
                    return node.node;
                });
                var contracts = new Backbone.Collection(formatted);

                var average = 0;
                var renewals = 0;

                contracts.each(function(c) {
                    average += parseFloat(c.get('contract_duration'));
                    if (!_.isNaN(parseFloat(c.get('number_of_renewals')))) {
                        renewals += parseFloat(c.get('number_of_renewals'));
                    }
                });

                average = average / contracts.length;
                renewals = renewals / contracts.length;

                if (_.isFunction(cb)) {
                    cb.call(self, parseFloat(average.toFixed(2)), parseFloat(renewals.toFixed(2)), contracts);
                }
            }

        });
    },
    getAverageContractDurationPerInstrument: function(options, cb) {
        var self = this;
        options = options || {};

        if (!self.node || !self.node.nid) {
            console.log('no node id for getAverageContractDurationPerInstrument. Aborting');
            return;
        }
        var displayId = (options.status === 'cancelled') ? 'page_2' : 'page_1';

        request({
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            uri: 'https://www.db-mms.com/erp/server/api',
            form: {
                callback: 'erp_drupal_view',
                view: 'erp_contract_duration',
                display_id: displayId,
                arguments: {
                    mms_nid: self.node.nid,
                }
            }
        }, function(error, response, body) {

            if (!error && response.statusCode == 200) {
                var data = JSON.parse(body);
                var formatted = _(data.nodes).map(function(node) {
                    return node.node;
                });

                var contracts = new Backbone.Collection(formatted);
                var grouped = contracts.groupBy('contract_instruments');
                var output = {};

                _.keys(grouped).forEach(function(instrument) {
                    if (instrument != 'undefined') {

                        var contractsPerInstrument = grouped[instrument];
                        var average = 0;
                        var renewals = 0;

                        _(contractsPerInstrument).each(function(c) {
                            average += parseFloat(c.get('contract_duration'));
                            if (!_.isNaN(parseFloat(c.get('number_of_renewals')))) {
                                renewals += parseFloat(c.get('number_of_renewals'));
                            }
                        });

                        average = average / contractsPerInstrument.length;
                        output[instrument] = parseFloat(average.toFixed(2))
                            // renewals = renewals / contracts.length;

                    }
                });
                if (cb) cb(error, output);
            } else {
                if (cb) cb(error);
            }

        });
    },
    /**
     * Get prospects by status (all,signed,archived) for a school
     * Used on statistics calculation
     * 
     */
    getProspectsByStatus: function(options, cb) {
        var self = this;
        options = options || {};

        if (!self.node || !self.node.nid) {
            console.log('no node id for getTotalProspects. Aborting');
            return;
        }

        var displayId = 'page_4';
        if (options.status === 'signed') {
            displayId = 'page_1'
        } else if (options.status === 'archived') {
            displayId = 'page_2'
        } else if (options.status === 'pending') {
            displayId = 'page_4'
        }

        request({
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            uri: 'https://www.db-mms.com/erp/server/api',
            form: {
                callback: 'erp_drupal_view',
                view: 'erp_conversion_stats',
                display_id: displayId,
                arguments: {
                    mms_nid: self.node.nid,
                    month : options.month
                }
            }
        }, function(error, response, body) {

            if (!error && response.statusCode == 200) {
                var data = JSON.parse(body);
                var formatted = _(data.nodes).map(function(node) {
                    return node.node;
                });
                var prospects = new Backbone.Collection(formatted);
                if (_.isFunction(cb)) cb(error, prospects);
            } else {
                var statusCode = (response && response.statusCode) ? '[Error:' + response.statusCode + ']' : undefined;
                if (_.isFunction(cb)) cb(error || statusCode, [])
            }

        });
    },
    getLeads: function(options, cb) {
        var self = this;
        options = options || {};

        if (!self.node || !self.node.nid) {
            console.log('no node id for getLeads. Aborting');
            return;
        }

        request({
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            uri: 'https://www.db-mms.com/erp/server/api',
            form: {
                callback: 'erp_website_leads',
                mmsnid: self.node.nid,
                status: 1
            }
        }, function(error, response, body) {
            console.log('response', error);
            if (!error && response.statusCode == 200) {
                var data = JSON.parse(body);
                console.log('website leads for', self.node.nid, data)
                    // var leads = new Backbone.Collection(formatted);

                // if (_.isFunction(cb)) {
                //     cb.call(self, leads);
                // }

            }
        });
    },
    // getContracts: function () {
    //   // If contracts are in memory escape fetching from server.
    //   if(this.collectionContracts) {
    //     return this.collectionContracts;
    //   }
    //   var contracts = this.fetch({
    //     callback: 'get_contracts',
    //     mms_nid: this.id
    //   });
    //   this.collectionContracts = self.addCollection({
    //     model: Contract,
    //     items: _.values(contracts)
    //   });
    //   return this.collectionContracts;
    // },
    getStudents: function(cb) {
        // // If contracts are in memory escape fetching from server.
        // if (this.collectionStudents) {
        //   return this.collectionStudents;
        // }
        // var students = this.fetch({
        //   callback: 'get_students',
        //   mms_nid: this.id
        // });

        // this.collectionStudents = self.addCollection({
        //   model: Profile,
        //   items: _.values(students)
        // });
        // return this.collectionStudents;

        var self = this;

        if (this.collectionStudents) {
            if (_.isFunction(cb)) {
                cb.call(self, self.collectionStudents);
            };
            return this.collectionStudents;
        }



        request({
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            uri: 'https://www.db-mms.com/erp/server/api',
            form: {
                callback: 'get_students',
                mms_nid: self.id
            }
        }, function(error, response, body) {
            if (!error && response.statusCode == 200) {

                var parseError;
                var data;
                try {
                    data = JSON.parse(body);
                } catch (err) {
                    parseError = err;
                }

                if (parseError) {
                    console.log('error in getStudents', parseError, self.id,String(body))
                    return self.getStudents(cb);
                }

                var formatted = _.values(data.data);

                self.collectionStudents = new Backbone.Collection(formatted, {
                    model: Profile
                });

                if (_.isFunction(cb)) {
                    cb.call(self, self.collectionStudents);
                };
            }

        });

    },
    getProspects: function(cb) {
        // If contracts are in memory escape fetching from server.
        // if (this.collectionProspects) {
        //   return this.collectionProspects;
        // }
        // var prospects = this.fetch({
        //   callback: 'get_prospects',
        //   mms_nid: this.id
        // });
        // this.collectionProspects = self.addCollection({
        //   model: Prospect,
        //   items: _.values(prospects)
        // });
        // return this.collectionProspects;


        var self = this;


        if (this.collectionProspects) {
            if (_.isFunction(cb)) {
                cb.call(self, self.collectionProspects);
            };
            return this.collectionProspects;
        }
        try {

            request({
                headers: {
                    'content-type': 'application/x-www-form-urlencoded'
                },
                method: 'POST',
                uri: 'https://www.db-mms.com/erp/server/api',
                form: {
                    callback: 'get_prospects',
                    mms_nid: self.id
                }
            }, function(error, response, body) {
                if (!error && response.statusCode == 200) {

                    var data;
                    var parseError;

                    try {
                        data = JSON.parse(body);
                    } catch (err) {
                        parseError = err;
                    }

                    if (parseError) {
                        console.log('error in getProspects', parseError, self.id, String(body))
                        return self.getProspects(cb);
                    }


                    var formatted = _.values(data.data);

                    self.collectionProspects = new Backbone.Collection(formatted, {
                        model: Profile
                    });

                    if (_.isFunction(cb)) {
                        cb.call(self, self.collectionProspects);
                    }
                }

            });

        } catch (err) {

            if (_.isFunction(cb)) {
                cb.call(self, false);
            }
        }


    },
    getTrials: function(cb) {
        // If contracts are in memory escape fetching from server.
        // if (this.collectionTrials) {
        //   return this.collectionTrials;
        // }
        // var trials = this.fetch({
        //   callback: 'get_trials',
        //   mms_nid: this.id
        // });
        // this.collectionTrials = self.addCollection({
        //   model: Lesson,
        //   items: _.values(trials)
        // });
        // console.log("got trials");
        // console.log(this.collectionTrials);
        // return this.collectionTrials;

        var self = this;

        if (this.collectionTrials) {
            if (_.isFunction(cb)) {
                cb.call(self, self.collectionTrials);
            };
            return this.collectionTrials;
        }

        this.collectionTrials = new Backbone.Collection([], {
            model: Lesson
        });

        request({
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            uri: 'https://www.db-mms.com/erp/server/api',
            form: {
                callback: 'get_prospects',
                mms_nid: self.id
            }
        }, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                var data = JSON.parse(body);
                var formatted = _.values(data.data);
                self.collectionTrials.reset(formatted);

                if (_.isFunction(cb)) {
                    cb.call(self, self.collectionTrials);
                };
            }

        });
    },

    /**
     * Accounting functions
     */
    //amount received on a month. So, only what was paid to the franchisee
    accountingCashReceived: function(ymd, cb) {

        var self = this;

        request({
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            uri: 'https://www.db-mms.com/erp/server/api',
            form: {
                callback: 'accounting_cash_received',
                mms_nid: this.id,
                ymd: ymd
            }
        }, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                var data = JSON.parse(body);
                if (_.isFunction(cb)) {
                    cb.call(self, data.data);
                };
            }

        });

    },
    //amount received on a month. So, only what was paid to the franchisee
    accountingAutoPayReceived: function(ymd, cb) {
        // var openBills = this.fetch({
        //   callback: 'accounting_autopay_received',
        //   mms_nid: this.id,
        //   ymd: ymd
        // });

        // // console.log('accountingReceived');
        // // console.log(this.collectionAccountingReceived);
        // return this.collectionAccountingReceived;

        var self = this;

        request({
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            uri: 'https://www.db-mms.com/erp/server/api',
            form: {
                callback: 'accounting_autopay_received',
                mms_nid: self.id,
                ymd: ymd
            }
        }, function(error, response, body) {
            if (!error && response.statusCode == 200) {

                var data = JSON.parse(body);
                var formatted = _.values(data.data);

                var collectionAccountingReceived = self.addCollection({
                    model: Billing,
                    items: formatted
                });

                if (_.isFunction(cb)) {
                    cb.call(self, collectionAccountingReceived);
                };
            }

        });

    },
    //amount received on a month. So, only what was paid to the franchisee
    accountingReceived: function(ymd, cb) {
        // var openBills = this.fetch({
        //   callback: 'accounting_received',
        //   mms_nid: this.id,
        //   ymd: ymd
        // });

        // console.log('accountingReceived');
        // console.log(this.collectionAccountingReceived);
        var self = this;

        request({
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            uri: 'https://www.db-mms.com/erp/server/api',
            form: {
                callback: 'accounting_received',
                mms_nid: this.id,
                ymd: ymd
            }
        }, function(error, response, body) {
            if (!error && response.statusCode == 200) {

                var data = JSON.parse(body);
                var formatted = _.values(data.data);

                var openBills = self.addCollection({
                    model: Billing,
                    items: formatted
                });

                if (_.isFunction(cb)) {
                    cb.call(self, openBills);
                };
            }

        });

    },
    //Amount expected on a month, all money that should be paid to the franchisee on that month.
    accountingReceivable: function(ymd, cb) {
        // var openBills = this.fetch({
        //   callback: 'accounting_receivable',
        //   mms_nid: this.id,
        //   ymd: ymd
        // });
        // //TODO: this is being returned from a SQL query so it will NOT initiate Bill Models properly !!
        // this.collectionAccountingReceivable = self.addCollection({
        //   model: Billing,
        //   items: _.values(openBills)
        // });
        // // console.log('accountingReceivable');
        // // console.log(this.collectionAccountingReceivable);
        // return this.collectionAccountingReceivable;
        var self = this;

        request({
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            uri: 'https://www.db-mms.com/erp/server/api',
            form: {
                callback: 'accounting_receivable',
                mms_nid: this.id,
                ymd: ymd
            }
        }, function(error, response, body) {
            if (!error && response.statusCode == 200) {

                var data = JSON.parse(body);
                var formatted = _.values(data.data);

                var openBills = self.addCollection({
                    model: Billing,
                    items: formatted
                });

                if (_.isFunction(cb)) {
                    cb.call(self, openBills);
                };
            }

        });
    },
    //The total open accounting of past.
    accountingOverdue: function(ymd, cb) {
        // var openBills = this.fetch({
        //   callback: 'accounting_overdue',
        //   mms_nid: this.id,
        //   ymd: ymd
        // });
        // //TODO: this is being returned from a SQL query so it will NOT initiate Bill Models properly !!
        // this.collectionAccountingOverdue = self.addCollection({
        //   model: Billing,
        //   items: _.values(openBills)
        // });
        // // console.log('OpenBills');
        // // console.log(this.collectionAccountingOverdue);
        // return this.collectionAccountingOverdue;
        var self = this;

        request({
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            uri: 'https://www.db-mms.com/erp/server/api',
            form: {
                callback: 'accounting_overdue',
                mms_nid: this.id,
                ymd: ymd
            }
        }, function(error, response, body) {
            if (!error && response.statusCode == 200) {

                var data = JSON.parse(body);
                var formatted = _.values(data.data);

                var openBills = self.addCollection({
                    model: Billing,
                    items: formatted
                });

                if (_.isFunction(cb)) {
                    cb.call(self, openBills);
                };
            }

        });
    },
    //teacher payments
    accountingTeacherPayments: function(ymd) {
        return this.teacherPaymentsTotal;
    },


    /******/

    getOpenAccounting: function(ymd, cb) {
        // var openBills = this.fetch({
        //   callback: 'get_open_bills',
        //   mms_nid: this.id,
        //   ymd: ymd
        // });
        // //TODO: this is being returned from a SQL query so it will NOT initiate Bill Models properly !!
        // this.collectionOpenBills = self.addCollection({
        //   model: Billing,
        //   items: _.values(openBills)
        // });
        // // console.log('OpenBills');
        // // console.log(this.collectionOpenBills);
        // return this.collectionOpenBills;
        var self = this;

        request({
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            uri: 'https://www.db-mms.com/erp/server/api',
            form: {
                callback: 'get_open_bills',
                mms_nid: this.id,
                ymd: ymd
            }
        }, function(error, response, body) {
            if (!error && response.statusCode == 200) {

                var data = JSON.parse(body);
                var formatted = _.values(data.data);

                var openBills = self.addCollection({
                    model: Billing,
                    items: formatted
                });

                if (_.isFunction(cb)) {
                    cb.call(self, openBills);
                };
            }

        });
    },
    getPayedBills: function(ymd, cb) {
        // var payedBills = this.fetch({
        //   callback: 'get_payed_bills',
        //   mms_nid: this.id,
        //   ymd: ymd
        // });
        // //TODO: Payed Bills should check the receipts of payment too!!
        // this.collectionPayedBills = self.addCollection({
        //   model: Billing,
        //   items: _.values(payedBills)
        // });
        // return this.collectionPayedBills;

        var self = this;

        // if (this.collectionTrials) {
        //     if( _.isFunction(cb) ){
        //       cb.call(self,self.collectionTrials);
        //     };
        //   return this.collectionTrials;
        // }

        this.collectionPayedBills = new Backbone.Collection([], {
            model: Lesson
        });

        request({
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            uri: 'https://www.db-mms.com/erp/server/api',
            form: {
                callback: 'get_payed_bills',
                mms_nid: self.id
            }
        }, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                var data = JSON.parse(body);
                var formatted = _.values(data.data);
                self.collectionPayedBills.reset(formatted);

                if (_.isFunction(cb)) {
                    cb.call(self, self.collectionPayedBills);
                };
            }

        });
    },
    getMonthlyAccounting: function(ymd, cb) {
        // var payedBills = this.fetch({
        //   callback: 'get_monthly_accounting',
        //   mms_nid: this.id,
        //   ymd: ymd
        // });
        // this.collectionMonthlyBills = self.addCollection({
        //   model: Billing,
        //   items: _.values(payedBills)
        // });
        // return this.collectionMonthlyBills;
        var self = this;

        request({
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            uri: 'https://www.db-mms.com/erp/server/api',
            form: {
                callback: 'get_monthly_accounting',
                mms_nid: this.id,
                ymd: ymd
            }
        }, function(error, response, body) {
            if (!error && response.statusCode == 200) {

                var data = JSON.parse(body);
                var formatted = _.values(data.data);

                var payedBills = self.addCollection({
                    model: Billing,
                    items: formatted
                });

                if (_.isFunction(cb)) {
                    cb.call(self, payedBills);
                };
            }

        });
    },
    PayTeachers: function() {
        var response = this.fetch({
            callback: 'pay_teachers',
            mms_nid: this.id
        });
        console.debug('[Models.School] Pay Teachers Response');
        return response;
    },
    RunEom: function() {
        var response = this.fetch({
            callback: 'run_eom',
            mms_nid: this.id
        });
        console.debug('[Models.School] Run EOM Response');
        return response;
    }


});


module.exports = School;
