var request = require('request');
var _ = require('underscore');
var Backbone = require('backbone');
var async = require('async');
require('datejs');

var db = require('../db');

/**
 * School Model
 */
/**
 * School Statistic model for saving..
 * @param  {[type]} options){    }}
 * @return {[type]}
 */
var Directory = Backbone.Model.extend({
    idAttribute: '_id',
    save: function(cb, options) {
        var self = this;
        db.directories.update({
            nid: self.get('nid'),
        }, self.toJSON(), {
            upsert: true /*create a new doc if not found*/
        }, cb);
    },
    destroy: function(cb) {
        var self = this;
        db.directories.remove({
            nid: self.get('nid'),
        }, cb);
    }

});



module.exports = Directory;