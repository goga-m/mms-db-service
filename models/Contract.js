var request = require('request');
var _ = require('underscore');
var Backbone = require('backbone');
var async = require('async');


/**
 * Contract Model
 */
var Contract = Backbone.Model.extend({
    idAttribute: 'nid',
    runEom: function(options, cb) {
        var self = this;
        request({
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            uri: 'https://www.db-mms.com/erp/server/api',
            form: {
                callback: 'node_run_eom',
                mms_nid: options.mms_nid,
                contract_nid: self.id
            }
        }, function(error, response, body) {

            if (!error && response.statusCode == 200) {

                var data;
                var parseError;

                try {
                    data = JSON.parse(body);
                } catch (err) {
                    parseError = err;
                }

                if (parseError) {
                    
                    if (_.isFunction(options.log)) {
                        options.log('Contract.runEom error for :' + self.get('title') + '[' + self.id + ']', {
                            error: parseError,
                            body: body,
                            contract: self.toJSON()
                        });
                    }

                    return;
                }

                var formatted = _.values(data.data);

                if (_.isFunction(cb)) {
                    cb.call(self, error, formatted);
                }
            } else {
                if (_.isFunction(cb)) {
                    cb.call(self, error);
                }
            }

        });
    }
});

module.exports = Contract;
