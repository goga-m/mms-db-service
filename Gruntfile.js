module.exports = function(grunt) {


    /**
     * Run teacher payments task
     */
    grunt.registerTask('teacherpayments:run', 'Teacher payments batch run', function(arg1, arg2) {

        var school = grunt.option('school');

        var fs = require('fs-extra');
        var year = new Date().getFullYear();
        var month = ("0" + new Date().getMonth()).slice(-2);
        var winston = require('winston');
        var logpath = __dirname + '/logs/tp_' + year + '' + month + '_logs'

        fs.ensureDirSync(logpath);
        fs.emptyDirSync(logpath);

        var logger = new(winston.Logger)({
            transports: [
                new(winston.transports.Console)(),
                new(winston.transports.File)({
                    name: 'json',
                    filename: logpath + '/tp_' + year + '' + month + '.log',
                    handleExceptions: true,
                    humanReadableUnhandledException: true,
                    json: false
                }),
                new(winston.transports.File)({
                    name: 'human',
                    filename: logpath + '/tp_' + year + '' + month + '.json',
                    json: true
                })
            ]
        });

        // make this task asyncronous (grunt)
        var done = this.async();

        var runTeacherPayments = require('./modules/node_run_teacher_payment');

        runTeacherPayments({
            school: school,
            log: function(msg, meta) {
                logger.log('info', msg, meta || '');
            }
        }, function() {
            done('Finished');
        })

    });



    /**
     * Task to run eom for all schools
     */
    grunt.registerTask('eom:run', 'Run eom for all Schools', function(schoolNid, arg2) {

        var school = grunt.option('school');


        var done = this.async();

        var fs = require('fs-extra');
        var year = new Date().getFullYear();
        var month = ("0" + new Date().getMonth()).slice(-2);
        var winston = require('winston');
        var logpath = __dirname + '/logs/eom_' + year + '' + month + '_logs'

        fs.ensureDirSync(logpath);
        fs.emptyDirSync(logpath);

        var logger = new(winston.Logger)({
            transports: [
                new(winston.transports.Console)(),
                new(winston.transports.File)({
                    name: 'json',
                    filename: logpath + '/eom_' + year + '' + month + '.log',
                    handleExceptions: true,
                    humanReadableUnhandledException: true,
                    json: false
                }),
                new(winston.transports.File)({
                    name: 'human',
                    filename: logpath + '/eom_' + year + '' + month + '.json',
                    json: true
                })
            ]
        });


        var runEom = require('./modules/node_run_eom');
        runEom({
            school: school,
            log: function(msg, meta) {
                logger.log('info', msg, meta || '');
            }
        }, function() {
            done('Finished');
        })
    });


    /**
     * Statistics
     */

    // all School statistics calculation
    grunt.registerTask('statistics:total:run', 'Calculate and save total statistics for all Schools', function() {


        require('datejs');
        var done = this.async();
        var offset = grunt.option('offset') || 0;
        var snapshotDate = grunt.option('snapshotDate') || new Date().moveToFirstDayOfMonth().toString('yyyyMMdd');

        var Schools = require('./collections/Schools');
        var mmsSchools = new Schools();
        var db = require('./db.js');
        var async = require('async');

        console.log('Using snapshotDate:', snapshotDate, ', offset:', offset)

        mmsSchools.initializeSchools(function(err, schools) {
            mmsSchools.getStatisticsByOffset({
                offset: offset,
                snapshotDate: snapshotDate,
            }, function(stats) {
                async.each(stats, function(stat, callback) {
                    db.total_statistics.update({
                        snapshotDate: stat.get('snapshotDate'),
                    }, stat.toJSON(), {
                        upsert: true /*create a new doc if not found*/
                    }, function(err) {
                        console.log('saved statistic for:', stat.get('snapshotDate'), err);
                        callback();
                    });

                }, function() {
                    done('Finished')
                });
            })
        });
    });


    /**
     * Statistics
     */

    // Statistics calculation per school
    grunt.registerTask('statistics:run', 'Calculate and save statistics for schools', function() {

        var done = this.async();
        
        require('datejs');
        var async = require('async');
        var Schools = require('./collections/Schools');
        var db = require('./db');


        var mmsSchools = new Schools();
        var schoolId = grunt.option('mmsnid') || 'all';
        var ymd = grunt.option('snapshotDate') || new Date().moveToFirstDayOfMonth().toString('yyyyMMdd');

        async.series([

            function(callback) {
                /* 1. Fetch school nodes*/
                mmsSchools.getSchoolNodes(function(schools) {
                    console.log('fetched school Nodes', schools.length);
                    mmsSchools.each(function(s) {
                        // console.log(s.toJSON());

                        console.log(s.get('title'), s.get('nid'));
                    });
                    callback();
                });
            },
        ], function(err, results) {

            if (schoolId !== 'all') {
                var school = mmsSchools.get(schoolId);
                console.log(ymd);
                school.calculateStatistics({
                    ymd: ymd
                }, function(error, result) {
                    console.log('Finished calculation for ', school.get('title') + '[' + school.id + ']', ymd);
                    console.log('result', JSON.stringify(result));
                    if (error) console.log('Errors', error);
                });

            } else if (schoolId === 'all') {
                mmsSchools.calculateAllStatistics({
                    ymd: ymd
                }, function(err, results) {
                    res.end('Finished all statistics calculation');
                    done('Finished');
                });
            }
        });
    });



    grunt.registerTask('default')
};
