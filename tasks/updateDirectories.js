var request = require('request');
var _ = require('underscore');
var Backbone = require('backbone');
var async = require('async');

var Directories = require('../collections/Directories');
var Schools = require('../collections/Schools');



var start = new Date();
var directories = new Directories();

var schoolId = process.argv[2];
if (!schoolId) return;

var mmsSchools = new Schools();
// console.log('Starting Directory updating for school:', schoolId);

async.series([

    function(callback) {
        mmsSchools.getSchoolNodes(function(schools) {
            console.log('fetched school Nodes', schools.length);
            callback();
        });
    },
], function(err, results) {


    if (schoolId !== 'all') {

        console.log('Fetched school');
        var school = mmsSchools.get(schoolId);

        if (!school)
            return console.log('[Error: School: ' + schoolId + ' not found.');

        console.log('Updating directories for school', school.get('title'));
        directories.update({
            mms_nid: school.get('nid'),
            // profile_nid : '11',
        }, function(err, data) {
            if (err) return console.log(err);
            console.log('Updated directories for school:', school.get('title'), ', length:', directories.toJSON().length);
            console.log('Duration[sec]: ', (new Date().getTime() - start.getTime()));
        });



    } else if (schoolId === 'all') {
        console.log('Updating directories for all schools');

        async.eachLimit(mmsSchools.models, 2, function(school, callback) {

                console.log('Updating directory for school ',school.get('title'));
                directories.update({
                    mms_nid: school.get('nid'),
                }, function(err, data) {
                    if (err) return console.log(err);
                    console.log('Updated directories for school:', school.get('title'), ', length:', directories.toJSON().length);
                    console.log('Duration[sec]: ', (new Date().getTime() - start.getTime()));
                    callback();
                });

            },
            function(error, results) {
                console.log('*****');
                console.log('Updated directories for all schools',error || '');
            });
    }




});