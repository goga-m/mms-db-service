var db = require('../db');
require('datejs');
var _ = require('underscore');
var async = require('async');


// Snippet to update existing prospects in statistics

var month = process.argv[2] || '2014-12'
var ymd = '20141201';

console.log(ymd);

var StatsSchool = require('../models/stats/School');

var Schools = require('../collections/Schools');
var mmsSchools = new Schools();

async.series([

    function(callback) {
        /* 1. Fetch school nodes*/

        mmsSchools.getSchoolNodes(function(schools) {
            console.log('fetched school Nodes', schools.length);
            mmsSchools.each(function(s) {
                console.log(s.get('title'), s.get('nid'));
            });
            callback();
        });
    },
], function(err, results) {

    async.eachLimit(mmsSchools.models, 1, function(school, callback) {

        console.log('')
        console.log('')
        console.log('')
        console.log('Checking statistics for ', school.get('title'), school.id, ymd);
        school.getStatistics(ymd, function(result) {


            var statistic = result[0];
            var mms = new StatsSchool({
                node: {
                    nid: school.id
                }
            });


            if(!statistic) return callback();

            var statuses = { 'archived': 'numberOfArchivedProspects', 'signed': 'numberOfSignedProspects', 'all': 'numberOfTotalProspects', 'pending': 'numberOfPendingProspects' };
            async.eachLimit(_.keys(statuses), 1, function(status, cb) {

                mms.getProspectsByStatus({
                    status: status,
                    month: month
                }, function(err, data) {

                    console.log(_.pick(statistic, statuses[status]))
                    statistic[statuses[status]] = data.length;
                    console.log(_.pick(statistic, statuses[status]))
                    cb();
                })
            }, function() {

                db.statistics.update({
                    snapshotDate: ymd,
                    schoolId: school.id
                }, {
                    snapshotDate: ymd,
                    data: JSON.stringify(statistic),
                    schoolId: school.id
                }, {
                    upsert: false /*create a new doc if not found*/
                }, function(){
                    console.log('saved')
                    console.log('finished')
                    console.log('')
                    console.log('')
                    console.log('')
                    callback();
                });

            })


        }, function(error) {
            console.log('error');
            callback(error);
        });

    }, function(err) {
        console.log('');
        process.exit(1); /*process die*/
    });

});
