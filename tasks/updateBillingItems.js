// var request = require('request');
// var _ = require('underscore');
// var Backbone = require('backbone');
// var async = require('async');

// var BillingItems = require('../collections/BillingItems');
// var Schools = require('../collections/Schools');
var accounting = require('../modules/accounting.js');


// var start = new Date();
// var accounting = new BillingItems();

var schoolId = process.argv[2];

if (!schoolId) return;

accounting.updateAccounting(schoolId,function(errors) {
    console.log('*************')
    console.log('Updated accounting.', errors);
    console.log('*************')
});

// var mmsSchools = new Schools();

// async.series([

//     function(callback) {
//         /* 1. Fetch school nodes*/
//         console.log('getting school nodes');
//         mmsSchools.getSchoolNodes(function(schools) {
//             console.log('fetched school Nodes', schools.length);
//             callback();
//         });
//     },
// ], function(err, results) {

//     console.log('*********************');
//     console.log('Finished Initialization');
//     console.log('*********************');

//     var school = mmsSchools.get(schoolId);

//     if (!school) {
//         console.log('[Error: School with id ' + schoolId + ' not found.');
//         return;
//     }
//     school.getContractNodes(function() {

//         async.each(school.contractNodes.models, function(contract, cb) {

//             var start = new Date();
//             console.log('Updating billing for contract', contract.get('title'));
//             var nid = contract.get('nid');
//             accounting.updateBillingItems({
//                 mms_nid: school.get('nid'),
//                 field_contract_bill_ref: nid
//             }, function(err, data) {
//                 console.log(err, 'Updated billing for contract:', nid, data);
//                 console.log('Duration[sec]: ', (new Date().getTime() - start.getTime()));
//                 cb();
//             });
//         }, function() {
//             console.log('finished accounting update for ', school.get('title'));
//         });
//     });
// });
