var db = require('../db');
require('datejs');
var _ = require('underscore');
var async = require('async');
var request = require('request');

// Snippet to update existing prospects in statistics

var month = process.argv[2] || '2014-12'
var ymd = '20141201';

console.log(ymd);

var StatsSchool = require('../models/stats/School');

var Schools = require('../collections/Schools');
var mmsSchools = new Schools();


async.series([

    function(callback) {
        /* 1. Fetch school nodes*/

        mmsSchools.getSchoolNodes(function(schools) {
            console.log('fetched school Nodes', schools.length);
            mmsSchools.each(function(s) {
                console.log(s.get('title'), s.get('nid'));
            });
            callback();
        });
    },
], function(err, results) {

    async.eachLimit(mmsSchools.models, 1, function(school, callback) {

        console.log('')
        console.log('')
        console.log(school.get('title'), school.id, ymd);
        // if (String(school.id) !== '225') return callback();
        school.getActiveTeachers(function(err, teachers) {
            async.eachLimit(teachers.models, 1, function(teacher, teacherCb) {

                console.log('teacher', teacher.get('nid'), teacher.get('title'));
                // update teacher statistics
                request({
                    headers: {
                        'content-type': 'application/x-www-form-urlencoded'
                    },
                    method: 'POST',
                    uri: 'https://www.db-mms.com/erp/server/api',
                    form: {
                        callback: 'erp_drupal_view',
                        view: 'erp_teacher_trials',
                        display_id: 'page_1',
                        arguments: {
                            mms_nid: school.id,
                            teacher_nid: teacher.get('nid')
                        }
                    }
                }, function(error, response, body) {

                    if (!error && response.statusCode == 200) {
                        var data = JSON.parse(body);
                        var formatted = _(data.nodes).map(function(node) {
                            return node.node;
                        });

                        async.eachLimit(formatted, 1, function(trial, trialCb) {
                            console.log('-- updating trial prospect', trial.prospect_nid, '...');
                            if (trial.prospect_nid) {

                                request({
                                    headers: {
                                        'content-type': 'application/x-www-form-urlencoded'
                                    },
                                    method: 'POST',
                                    uri: 'https://www.db-mms.com/erp/server/api',
                                    form: {
                                        callback: 'erp_api_update_prospect_teacher_requested',
                                        teacher_nid: teacher.get('nid'),
                                        prospect_nid: trial.prospect_nid
                                    }
                                }, function(error, response, body) {

                                    if (!error && response.statusCode == 200) {
                                        var data = JSON.parse(body);
                                        console.log('--- prospect updated', JSON.stringify(data.data));
                                        console.log('--- https://www.db-mms.com/node/' + trial.prospect_nid + '/edit');
                                        console.log('--- saved', data.data.response.changed)
                                        console.log('')
                                        trialCb();

                                    } else {
                                        var statusCode = (response && response.statusCode) ? '[Error:' + response.statusCode + ']' : undefined;
                                    }

                                });
                            } else {
                                console.log('')
                                console.log('Error: No prospect for trial:', trial, 'Skipping...')
                                console.log('')
                                trialCb();
                            }

                        }, function(err) {
                            teacherCb();
                        });

                    } else {
                        var statusCode = (response && response.statusCode) ? '[Error:' + response.statusCode + ']' : undefined;
                        console.log('err');
                        teacherCb();
                    }
                });

            }, function() {
                console.log('finished ')
                callback();
            })

        });

    }, function(err) {
        console.log('');
        process.exit(1); /*process die*/
    });

});
