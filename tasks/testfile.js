var School = require("../models/stats/School.js");
var async = require('async');
var _ = require('underscore');

var io = new School({
    nid: 51979
})

io.getTeachers(function(data) {

    async.eachLimit(data.models,1,function(teacher,callback){
	    teacher.getTeacherProspectsByStatus({
	    	status: 'signed'
	    	// method : 'getAverageContractDurationPerInstrument',
	    },function(err,data){
	    	console.log('data',teacher.node.title, data.length);
	    	callback();
	    })

    },function(){
    	console.log('finished');
    })
})
