var Schools = require('../collections/Schools');
var mmsSchools = new Schools();
mmsSchools.getSchoolNodes(function(schools) {
   console.log('fetched schools');
   mmsSchools.getStatistics('20151101',function(err,data){
   		console.log('getStatistics',err,JSON.stringify(data));
   });
});
// var Backbone = require('backbone');
// var _ = require('underscore');

// var request = require('request');

// request({
//     headers: {
//         'content-type': 'application/x-www-form-urlencoded'
//     },
//     method: 'POST',
//     uri: 'https://www.db-mms.com/erp/server/api',
//     form: {
//         callback: 'erp_drupal_view',
//         view: 'erp_contract_duration',
//         display_id: 'page_2',
//         arguments: {
//             teacher_nid: '495'
//         }
//     }
// }, function(error, response, body) {

//     if (!error && response.statusCode == 200) {
//         var data = JSON.parse(body);
//         var formatted = _(data.nodes).map(function(node) {
//             return node.node;
//         });
//         console.log(JSON.stringify(formatted));
//         var contracts = new Backbone.Collection(formatted);

//         var average = 0;
//         var renewals = 0;

//         contracts.each(function(c) {
//         	console.log('contract duration: ',c.get('contract_duration'),'\t',c.get('Title'))
//             average += parseFloat(c.get('contract_duration'));
//             if (!_.isNaN(parseFloat(c.get('number_of_renewals')))) {
//             	console.log(c.get('number_of_renewals'));
//                 renewals += parseFloat(c.get('number_of_renewals'));
//             }
//         });

//         average = average / contracts.length;
//         renewals = renewals / contracts.length;

// 		console.log(parseFloat(average.toFixed(2)), parseFloat(renewals.toFixed(2)));
//     }

// });