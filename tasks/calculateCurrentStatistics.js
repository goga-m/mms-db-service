var async = require('async');

var Schools = require('../collections/Schools');
var db = require('../db');


var mmsSchools = new Schools();
var schoolId = process.argv[2];
var ymd = process.argv[3];

async.series([

    function(callback) {
        /* 1. Fetch school nodes*/
        mmsSchools.getSchoolNodes(function(schools) {
            console.log('fetched school Nodes', schools.length);
            mmsSchools.each(function(s){
                // console.log(s.toJSON());

                console.log(s.get('title'),s.get('nid'));
            });
            callback();
        });
    },
], function(err, results) {
    
    if (schoolId !== 'all') {
        var school = mmsSchools.get(schoolId);
        console.log(ymd);
        school.calculateStatistics({
            ymd : ymd
        },function(error, result) {
            console.log('Finished calculation for ',school.get('title') + '[' + school.id + ']',ymd);
            console.log('result',JSON.stringify(result));
            if(error) console.log( 'Errors', error);
        });

    } else if(schoolId === 'all') {
        mmsSchools.calculateAllStatistics({
            ymd : ymd
        },function(err, results) {
            res.end('Finished all statistics calculation');
        });
    }
});