var request = require('request');
var _ = require('underscore');
var Backbone = require('backbone');
var async = require('async');

var Schools = require('../collections/Schools');
var db = require('../db');


var mmsSchools = new Schools();
var schoolId = process.argv[2];
var ymd = process.argv[3];

async.series([

    function(callback) {
        /* 1. Fetch school nodes*/

        mmsSchools.getSchoolNodes(function(schools) {
            console.log('fetched school Nodes', schools.length);
            mmsSchools.each(function(s) {
                console.log(s.get('title'), s.get('nid'));
            });
            callback();
        });
    },
], function(err, results) {
    console.log(err, results, schoolId);
    async.eachLimit(mmsSchools.models, 1, function(school, callback) {

        console.log('Checking statistics for ',school.get('title'),school.id,ymd);
        school.getStatistics(ymd, function(result) {
            if (!result || result.length === 0) {
                console.log('Calculating statistics for ',school.get('title'),school.id,ymd);
                school.calculateStatistics({
                    ymd: ymd
                }, function(error, result) {
                    console.log(school.get('title') + '[' + school.id + ']', 'ready', error || '');
                    callback();
                });
            } else {
                callback();
            }
        }, function(error) {
            console.log('error');
            callback(error);
        });

    }, function(err) {
        console.log('finished');
        process.exit(1); /*process die*/
    });

});