var db = require('../db');
require('datejs');
var _ = require('underscore');
var async = require('async');


/**
 * Calculate benchmark from statistics for single ymd date
 * @param  {[type]}   snapshotDate [description]
 * @param  {Function} callback     [description]
 */
function calculateBenchmark(snapshotDate, callback) {
    db.statistics.find({
        snapshotDate: snapshotDate
    }, function(err, data) {
        var benchmark = {
            snapshotDate: snapshotDate
        };
        _.each(data, function(response) {
            var stat = JSON.parse(response.data);

            var attributeRanges = {
                groupRatio: {
                    min: 0.1,
                    max: 1
                },
                groupRating: {
                    min: 0.1,
                    max: 4
                },
                groupEfficiencyIndex: {
                    min: 1,
                    max: 4
                },
                signedConversion: {
                    min: 0,
                    max: 100
                },
                archived: {
                    min: 0,
                    max: 100
                }
            }


            if(stat.signedConversion === undefined){
                var conversion = (parseInt(stat.numberOfSignedProspects) / parseInt(stat.numberOfRealTotalProspects)) * 100;
                stat.signedConversion = (!_.isNaN(conversion)) ? conversion : 0;
            }

            if(stat.archivedProspects === undefined){
                var archivedProspects = (parseInt(stat.numberOfArchived) / parseInt(stat.numberOfRealTotalProspects)) * 100;
                stat.archivedProspects = (!_.isNaN(archivedProspects)) ? archivedProspects : 0;
            }


            // Add max values for all number-attributes
            _.each(_.keys(stat), function(key) {

                if (_.isNumber(stat[key])) {

                    if (benchmark[key] === undefined) {
                        benchmark[key] = stat[key];
                    }

                    if (benchmark !== undefined) {

                        if (_.contains(_.keys(attributeRanges), key)) {
                            if(stat[key] >= attributeRanges[key].min && stat[key] <= attributeRanges[key].max) {
                                benchmark[key] = (benchmark[key] < stat[key]) ? stat[key] : benchmark[key]
                            }
                        } else {
                            benchmark[key] = (benchmark[key] < stat[key]) ? stat[key] : benchmark[key];

                        }

                    }
                }
            });


            var contractStudentRatio = stat['totalNumberOfContracts'] / stat['totalNumberOfStudents'];

            if (benchmark['contractStudentRatio'] === undefined) {
                benchmark['contractStudentRatio'] = contractStudentRatio;
            }

            if (benchmark['contractStudentRatio'] !== undefined) {
                benchmark['contractStudentRatio'] = (benchmark['contractStudentRatio'] < contractStudentRatio) ? contractStudentRatio : benchmark['contractStudentRatio'];
            }





        });
        if (_.isFunction(callback)) callback(benchmark);
    });
}

/**
 * Calculate benchmarks for multiple dates
 * @param  {[type]}   snapshotDates [description]
 * @param  {Function} callback      [description]
 */
function calculateBenchmarks(snapshotDates, callback) {
    console.log(snapshotDates);
    var benchmarks = [];
    async.eachLimit(snapshotDates, 1, function(ymdDate, cb) {
        calculateBenchmark(ymdDate, function(benchmark) {
            benchmarks.push(benchmark);
            cb();
        });
    }, function() {
        callback(benchmarks);
    });
}


var ymd = process.argv[2] || new Date().toString('yyyyMMdd');
var offset = process.argv[3] || 0;

var date = Date.parseExact(ymd, 'yyyyMMdd').moveToFirstDayOfMonth();
var dateYmd = date.toString("yyyyMM dd").replace(' ', '');
var dates = [ymd];

for (var i = 0; i < offset; i++) {
    var d = date.moveToFirstDayOfMonth().add(-1).months();
    dates.push(d.toString("yyyyMM dd").replace(' ', ''));
}


calculateBenchmarks(dates, function(benchmarks) {

    _.each(benchmarks, function(benchmark) {
        if (_.keys(benchmark).length > 1) {

            db.benchmarks.update({
                snapshotDate: benchmark.snapshotDate,
            }, benchmark, {
                upsert: true /*create a new doc if not found*/
            }, function(err) {
                console.log(err, 'saved');
            });

        }
    });

});
