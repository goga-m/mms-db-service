var db = require('../db');
require('datejs');
var _ = require('underscore');
var async = require('async');
var Profile = require('../models/stats/Profile')
var request = require('request');
var Backbone = require('backbone');

// Snippet to update existing prospects in statistics

var month = process.argv[2] || '2016-02'
var ymd = '20160201';

var months = {
    '20160101': '2016-01',
    '20151201': '2015-12',
    '20151101': '2015-11',
    '20151001': '2015-10',
    '20150901': '2015-09',
    '20150801': '2015-08',
    '20150701': '2015-07',
    '20150601': '2015-06',
    '20150501': '2015-05',
    '20150401': '2015-04',
    '20150301': '2015-03',
    '20150201': '2015-02',
    '20150101': '2015-01',
    '20141201': '2014-12',
    '20141101': '2014-11',
    '20141001': '2014-10',
    '20140901': '2014-09',
    '20140801': '2014-08',
    '20140701': '2014-07',
    '20140601': '2014-06',
    '20140501': '2014-05',
    '20140401': '2014-04',
    // '20140301': '2014-03',
    // '20140201': '2014-02',
    // '20140101': '2014-01',
}




var StatsSchool = require('../models/stats/School');

var Schools = require('../collections/Schools');
var mmsSchools = new Schools();



var getTeacherProspectsByStatus = function(options, cb) {
    var self = this;
    options = options || {};


    var displayId = 'page_4';
    if (options.status === 'signed') {
        displayId = 'page_1'
    } else if (options.status === 'archived') {
        displayId = 'page_2'
    }

    request({
        headers: {
            'content-type': 'application/x-www-form-urlencoded'
        },
        method: 'POST',
        uri: 'https://www.db-mms.com/erp/server/api',
        form: {
            callback: 'erp_drupal_view',
            view: 'erp_teacher_conversion_stats',
            display_id: displayId,
            arguments: {
                mms_nid: options.mms_nid,
                teacher_nid: options.teacher_nid,
                month: options.month
            }
        }
    }, function(error, response, body) {

        if (!error && response.statusCode == 200) {
            var data = JSON.parse(body);
            var formatted = _(data.nodes).map(function(node) {
                return node.node;
            });

            // filter by teacher
            var myProspects = _(formatted).filter(function(item) {
                return item.teacher_nid === options.teacher_nid
            });
            var prospects = new Backbone.Collection(myProspects);

            if (_.isFunction(cb)) cb(error, prospects);
        } else {
            var statusCode = (response && response.statusCode) ? '[Error:' + response.statusCode + ']' : undefined;
            if (_.isFunction(cb)) cb(error || statusCode, [])
        }

    });
};


async.eachLimit(_.keys(months), 1, function(ymd, monthsCallback) {
    var month = months[ymd];
    console.log('Running for month', ymd, month)
    async.series([

        function(callback) {
            /* 1. Fetch school nodes*/

            mmsSchools.getSchoolNodes(function(schools) {
                console.log('fetched school Nodes', schools.length);
                mmsSchools.each(function(s) {
                    console.log(s.get('title'), s.get('nid'));
                });
                callback();
            });
        },
    ], function(err, results) {

        async.eachLimit(mmsSchools.models, 1, function(school, callback) {

            // if (String(school.id) !== '225') return callback();

            console.log('')
            console.log('')
            console.log('')
            console.log('Checking statistics for ', school.get('title'), school.id, ymd);
            school.getStatistics(ymd, function(result) {


                var statistic = result[0];

                if (!statistic) return callback();

                var statuses = { 'archived': 'numberOfArchivedProspects', 'signed': 'numberOfSignedProspects', 'all': 'numberOfTotalProspects', 'pending': 'numberOfPendingProspects' };

                var previous = _.extend({}, statistic);
                async.eachLimit(statistic.teacherCharts, 1, function(t, statCb) {
                    console.log('')
                    console.log('')
                    console.log('')
                    console.log(t.id, t.firstName, t.lastName)
                        // console.log('before:', JSON.stringify(t));
                    async.eachLimit(_.keys(statuses), 1, function(status, cb) {
                        console.log('status', statuses[status]);
                        getTeacherProspectsByStatus({
                            status: status,
                            mms_nid: school.id,
                            teacher_nid: t.id,
                            month: month
                        }, function(err, data) {
                            console.log('fetched data', err || data.length);

                            t[statuses[status]] = data.length;
                            console.log(statuses[status], '==', data.length);
                            cb();
                        })
                    }, function(err) {
                        console.log('updated all statuses')
                            // console.log('after:', JSON.stringify(t));
                        statCb();
                    })

                }, function() {

                    // console.log('snapshotDate',ymd)
                    // console.log('scholId',school.id)
                    console.log('')
                    console.log('')
                    console.log('')
                    console.log('')
                        // callback();
                        // update statistic to db
                    db.statistics.update({
                        snapshotDate: ymd,
                        schoolId: school.id
                    }, {
                        snapshotDate: ymd,
                        data: JSON.stringify(statistic),
                        schoolId: school.id
                    }, {
                        upsert: false /*create a new doc if not found*/
                    }, function() {
                        console.log('saved')
                        console.log('finished for school', school.id)
                        console.log('')
                        console.log('')
                        console.log('')
                        callback();
                    });
                });


                return;

            }, function(error) {
                console.log('error');
                callback(error);
            });

        }, function(err) {
            console.log('finished all schools for ', ymd);
            monthsCallback();
        });
    });
    // monthsCallback();
}, function() {
    console.log('finished for months', _.keys(months))
    process.exit(1);
});
