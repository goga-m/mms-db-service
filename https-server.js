var fs = require('fs');
var https = require('https');

// sudo openssl x509 -req -in server-csr.pem -signkey server-key.pem -out server-cert.pem
var privateKey  = fs.readFileSync('ssl/www.db-mms.com.key', 'utf8');
var certificate = fs.readFileSync('ssl/www.db-mms.com.crt', 'utf8');

var credentials = {key: privateKey, cert: certificate};

var express = require('express');
var app = express();

// your express configuration here
var httpsServer = https.createServer(credentials, app);

httpsServer.listen(8050);
module.exports = app;
