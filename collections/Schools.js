var request = require('request');
var _ = require('underscore');
var Backbone = require('backbone');
var async = require('async');

var School = require('../models/School');

/**
 * School Collection
 *
 */

var Schools = Backbone.Collection.extend({
    model: School,
    initialize: function() {
        console.log('initializing school list');
    },
    initializeSchools: function(cb) {
        var self = this;
        async.series([

            function(callback) {
                /* 1. Fetch school nodes*/
                console.log('getting school nodes');
                self.getSchoolNodes(function(schools) {
                    console.log('fetched school Nodes', schools.length);
                    callback();
                });
            },
            function(callback) {
                /* 2. Fetch contracts for all schools*/
                console.log('start cacheContracts');
                self.cacheContracts(function() {
                    console.log('end cacheContracts');
                    callback();
                });
            }
        ], function(err, results) {

            console.log('*********************');
            console.log('Finished Initialization of Schools');
            console.log('*********************');
            if (cb) cb(err, results);
        });

    },
    getSchoolNodes: function(callback) {
        var self = this;

        request({
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            uri: 'https://www.db-mms.com/erp/server/api',
            form: {
                callback: 'erp_api_get_school_nodes',
            }
        }, function(error, response, body) {

            if (!error && response.statusCode == 200) {
                var schools = JSON.parse(body);

                // var formatted = _.map(schools.nodes,function(school){
                //  return school.node;
                // });

                var formatted = _.values(schools.data);
                self.reset(formatted);

                if (_.isFunction(callback)) {
                    callback.call(self, self.toJSON());
                }
            }
        });
    },
    cacheContracts: function(cb) {

        var start = new Date();
        var self = this;
        async.eachLimit(this.models, 4, function(school, callback) {
            school.getContracts(function() {
                console.log('Processed ', school.get('title'), school.get('nid'));
                callback();
            });
        }, function(err) {
            if (err) {

            } else {
                var end = new Date();
                console.log('finished all schools in ', end - start, ' seconds');
                if (_.isFunction(cb)) cb.call(self, self);
            }
        });


    },
    fetchAllStatistics: function(cb) {
        var self = this;
        async.eachLimit(self.models, 2, function(school, callback) {
                school.fetchAllOldStatistics(function() {
                    console.log('*** Finished updating for school', school.id, school.get('title'), ' ***');
                    callback();
                });
            },
            function(error, results) {
                if (_.isFunction(cb)) cb.call(self, self);
            });
    },
    getStatistics: function(ymd, cb) {
        var self = this;
        var output = [];

        async.eachLimit(self.models, 2, function(school, callback) {
            school.getStatistics(ymd, function(result) {

                if (result && result[0]) {
                    output.push(result[0]);
                }
                callback();

            }, function(error) {
                console.log('error in school', ymd, school.get('nid'), school.get('title'));
                // callback(error);
                if (_.isFunction(cb)) cb(error, output);
            });
        }, function() {
            cb(null, output);
        });
    },
    calculateAllCurrentStatistics: function(cb) {
        var self = this;
        async.eachLimit(self.models, 2, function(school, callback) {
            console.log('**************************************************');
            console.log(school.get('title') + '[' + school.id + ']', 'starting statistics calculation');
            school.createthisMonthStatistics(function(error, result) {
                console.log(school.get('title') + '[' + school.id + ']', 'ready', error);
                callback();
            });
        }, function(err, results) {
            console.log('updated all current statistics for all schools');
            console.log('**************************************************');
            if (_.isFunction(cb)) cb.call(self, self);
        });
    },
    calculateAllStatistics: function(params, cb) {
        var self = this;
        async.eachLimit(self.models, 1, function(school, callback) {

            try {
                console.log('**************************************************');
                console.log(school.get('title') + '[' + school.id + ']', 'starting statistics calculation');
                school.calculateStatistics({
                    ymd: params.ymd
                }, function(error, result) {
                    console.log(school.get('title') + '[' + school.id + ']', 'ready', error || '');
                    setTimeout(function(){
                        callback();
                    },20000)
                });
            } catch (err) {
                console.log('[Statistics calculation error] ', school.get('title'), school.id, err);
                callback();
            }


        }, function(err, results) {
            console.log('updated all current statistics for all schools');
            console.log('**************************************************');
            if (_.isFunction(cb)) cb.call(self, self);
        });
    },
    getStatisticsByOffset: function(options, cb) {

        options = options || {};

        var self = this;

        var nowYmd = Date.today().moveToFirstDayOfMonth().toString("yyyyMM dd").replace(' ', '');
        var snapshotDate = options.snapshotDate || nowYmd;
        var dates = [snapshotDate];

        var last = Date.parseExact(snapshotDate, 'yyyyMMdd');
        for (var i = 0; i < options.offset; i++) {
            var d = last.moveToFirstDayOfMonth().add(-1).months();
            dates.push(d.toString("yyyyMM dd").replace(' ', ''));
        }

        var output = [];

        async.each(dates, function(ymd, callback) {

                self.getStatistics(ymd, function(err, stats) {
                    var s = self.calculateTotalStats(ymd, stats)
                    output.push(s)
                    callback(err);
                })
            },
            function(err) {
                cb(err || _.flatten(output))
            });
    },
    /**
     * Calculates total statistics
     */
    calculateTotalStats: function(ymd, stats, cb) {

        // sums
        var sumFields = {
            'numberOfADAC': 0,
            'numberOfAdvert': 0,
            'numberOfBandContracts': 0,
            'numberOfBassContracts': 0,
            'numberOfCanceledContracts': 0,
            'numberOfComfort': 0,
            'numberOfCommunity': 0,
            'numberOfContractsWithSpecialDiscounts': 0,
            'numberOfContractsWithTshirtVouchersRedeemed': 0,
            'numberOfCustom30': 0,
            'numberOfCustom45': 0,
            'numberOfCustom60': 0,
            'numberOfCustomplus30': 0,
            'numberOfCustomplus45': 0,
            'numberOfDJContracts': 0,
            'numberOfDrumsContracts': 0,
            'numberOfFacebookSources': 0,
            'numberOfFinishingContracts': 0,
            'numberOfFlyer': 0,
            'numberOfFriendSources': 0,
            'numberOfGroupon': 0,
            'numberOfGuitarContracts': 0,
            'numberOfHobbyCourse': 0,
            'numberOfHoursBefore': 0,
            'numberOfInternetSources': 0,
            'numberOfKeyboardContracts': 0,
            'numberOfKlingKlong': 0,
            'numberOfKlingKlongContracts': 0,
            'numberOfMMMag': 0,
            'numberOfMMSContracts': 0,
            'numberOfMembershipPartner': 0,
            'numberOfMmsDaySources': 0,
            'numberOfMusicTechnologyContracts': 0,
            'numberOfMusicTheoryContracts': 0,
            'numberOfNewspaper': 0,
            'numberOfNextMonthContracts': 0,
            'numberOfNextMonthFinishingContracts': 0,
            'numberOfOldComfort': 0,
            'numberOfOldCustom30': 0,
            'numberOfOldCustom45': 0,
            'numberOfOtherSources': 0,
            'numberOfPausedContracts': 0,
            'numberOfPayedTeachers': 0,
            'numberOfProContracts': 0,
            'numberOfProCourse': 0,
            'numberOfProfessionalProgram': 0,
            'numberOfProspects': 0,
            'numberOfTotalProspects': 0,
            'numberOfArchivedProspects': 0,
            'numberOfSignedProspects': 0,
            'numberOfRealTotalProspects': 0,
            'numberOfRadio': 0,
            'numberOfSClub': 0,
            'numberOfSaxophonContracts': 0,
            'numberOfSchoolContracts': 0,
            'numberOfStartingContracts': 0,
            'numberOfStatusActiveContracts': 0,
            'numberOfStatusCancelledContracts': 0,
            'numberOfStatusNoBillContracts': 0,
            'numberOfStatusPausedContracts': 0,
            'numberOfStatusToCancelContracts': 0,
            'numberOfStudentsWithMoreThanOneContracts': 0,
            'numberOfTrialLessons': 0,
            'numberOfV1030': 0,
            'numberOfV1045': 0,
            'numberOfVeryOldComfort': 0,
            'numberOfVeryOldCustom30': 0,
            'numberOfVeryOldCustom45': 0,
            'numberOfVibraContracts': 0,
            'numberOfVocalsContracts': 0,
            'numberOfVoucherContracts': 0,
            'totalNumberOfContracts': 0,
            'totalNumberOfStudents': 0,
            'totalSales': 0,
            'totalTeacherContractPayments': 0,
            'totalTeacherPayment': 0,
            'salesKK': 0,
            'salesMMS': 0,
            'salesVibra': 0,
            'femaleStudents': 0,
            'femalesAgeGroup1': 0,
            'femalesAgeGroup2': 0,
            'femalesAgeGroup3': 0,
            'femalesAgeGroup4': 0,
            'femalesAgeGroup5': 0,
            'femalesAgeGroup6': 0,
            'femalesAgeGroup7': 0,
            'maleStudents': 0,
            'malesAgeGroup1': 0,
            'malesAgeGroup2': 0,
            'malesAgeGroup3': 0,
            'malesAgeGroup4': 0,
            'malesAgeGroup5': 0,
            'malesAgeGroup6': 0,
            'malesAgeGroup7': 0,
            "groupContracts": 0,
            "receivable": 0,
            "received": 0,
            "vibraApplicationFees": 0,
            "vibraContractsSales": 0,
            "vibraOpenAccountingTM": 0,
            "vibraReceivable": 0,
            "outstandingAmounts": 0,
            "overdue": 0,
            "applicationFees": 0,
            "autoPayReceived": 0,
            "cashReceived": 0,
            "contractsSales": 0,
            "franchiseFee": 0,
            "groupLessons": 0,
            "kkApplicationFees": 0,
            "kkContractsSales": 0,
            "kkOpenAccountingTM": 0,
            "kkReceivable": 0,
            "mmsApplicationFees": 0,
            "mmsContractsSales": 0,
            "mmsOpenAccountingTM": 0,
            "mmsReceivable": 0,
            "monthlySales": 0,
            "monthyAccounting": 0,
            "openPlaces": 0
        }

        var averages = {
            "averageContractDuration": 0,
            "averageContractDurationActive": 0,
            "averageContractDurationCancelled": 0,
            "averageContractDurationPerInstrument": 0,
            "averageContractValue": 0,
            "averageNumberOfRenewals": 0,
            "averageNumberOfRenewalsActive": 0,
            "averageNumberOfRenewalsCancelled": 0,
            "averageTeacherPayment": 0,
            "averageTeacherPaymentPerContract": 0,
            "averageTeacherPaymentPercentage": 0,
            "groupEfficiencyIndex": 0,
            "groupRating": 0,
            "groupRatio": 0,
            "profitMargin": 0,
            "contractDuration": 0,
            "contractStudentRatio": 0,
        }

        var allFields = _.extend({}, sumFields, averages);
        var total = new Backbone.Model({
            snapshotDate: parseInt(stats[0].snapshotDate),
            schools: []
        });
        var calculatedSchools = [];

        /**
         * Initialization of predefined attributes
         */
        _.each(allFields, function(val, field) {

            var value = stats[0][field] || val;
            var data = {
                value: 0,
                min_value: value,
                min_school_name: null,
                min_school_id: null,
                max_value: value,
                max_school_name: null,
                max_school_id: null,
            }
            total.set(field, data);
        });

        // SumFields



        _.each(allFields, function(value, field) {
            _.each(stats, function(stat) {
                if (stat[field] !== null && stat[field] !== undefined) {
                    var totalVal = total.get(field);
                    // total
                    totalVal.value = parseFloat(totalVal.value) + parseFloat(stat[field]);
                    total.set(field, totalVal);
                    calculatedSchools.push(stat);
                }
            });

            var totalValue = total.get(field);

            // max values
            var maxStat = _.max(calculatedSchools, function(stat) {
                return parseFloat(stat[field]);
            });

            // min values
            var minStat = _.min(calculatedSchools, function(stat) {
                return parseFloat(stat[field]);
            });


            totalValue.max_value = parseFloat(maxStat[field])
            totalValue.max_school_name = maxStat.schoolName;
            totalValue.max_school_id = maxStat.schoolId;

            totalValue.min_value = parseFloat(minStat[field])
            totalValue.min_school_name = minStat.schoolName;
            totalValue.min_school_id = minStat.schoolId;


            // if field is an average field divide by calculatedSchools.length
            var averagesKeys = _.keys(averages)
            if (_.contains(averagesKeys, field)) {
                totalValue.value = parseFloat(totalValue.value) / calculatedSchools.length;
            }

            total.set(field, totalValue);
            calculatedSchools = [];
        });

        // save schools from which statistics were calculated
        // var schools = total.get('schools');
        // schools.push({
        //     name: stat.schoolName,
        //     id: stat.schoolId
        // });

        // averageContractDurationPerInstrument
        // otherMarketingSources



        // divide the averages by stats.length


        console.log('calculated total stats')
        if (cb) cb(total);
        return total;
    }


});

module.exports = Schools;
