var request = require('request');
var _ = require('underscore');
var Backbone = require('backbone');
var async = require('async');

var School = require('../models/School');
var db = require('../db');
var Schools = require('../collections/Schools');
/**
 * Directories Collection
 *
 */

var Directories = Backbone.Collection.extend({
    model: require('../models/Directory'),
    /**
     *
     * @param  {[type]}   params [description]
     * @param  {Function} cb     [description]
     * @return {[type]}          [description]
     */
    fetchItems: function(params, cb) {
        var self = this;

        request({
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            uri: 'https://www.db-mms.com/erp/server/api',
            form: {
                callback: 'erp_drupal_view',
                view: 'erp_api_directory',
                display_id: 'default',
                arguments: params
            }
        }, function(error, response, body) {
            if (error) console.log('Directory fetch items error', error);

            if (!error && response.statusCode == 200) {

                try {

                    // var items = JSON.parse(JSON.parse(body).nodes);
                    var mapped = _(JSON.parse(body).nodes).map(function(item) {

                        var profile = JSON.parse(item.node.data);
                        var user = JSON.parse(item.node.user);

                        if (profile.type === 'prospect_student') {
                            return self.mapProspectData(profile, user, item.node);
                        }

                        var contracts = (item.node.contracts !== 'null') ? _.values(JSON.parse(item.node.contracts)) : null;

                        var profileEmail = (profile.field_email && profile.field_email[0].value !== null) ? profile.field_email[0].value : user.email;
                        var teacher_in = _.pluck(JSON.parse(item.node.teacher_in), 'nid');
                        var student_in = _.uniq(_.pluck(_.flatten(_.pluck(contracts, 'field_lesson_franchise')), 'value'));

                        var ret = {
                            nid: profile.nid,
                            type: profile.type,
                            uid: profile.uid,
                            title: profile.title,
                            fname: (profile.field_fname) ? profile.field_fname[0].value : null,
                            lname: (profile.field_lname) ? profile.field_lname[0].value : null,
                            email: profileEmail,
                            gender: (profile.field_gender) ? profile.field_gender[0].value : null,
                            mobile: (profile.field_mobile) ? profile.field_mobile[0].value : null,
                            telephone: (profile.field_telephone) ? profile.field_telephone[0].value : null,
                            dob: (profile.field_dob) ? profile.field_dob[0].value : null,
                            created: profile.created,
                            published_status: profile.status,
                            tshirt_voucher_status: (profile.field_tshirt_voucher_status) ? profile.field_tshirt_voucher_status[0].value : null,
                            user_type: _.chain(profile.field_user_type)
                                .filter(function(item) {
                                    return (item.value);
                                })
                                .map(function(item) {
                                    return item.value;
                                }).value(),
                            related_franchises: _.chain(profile.field_related_franchises)
                                .filter(function(item) {
                                    return (item.nid);
                                })
                                .map(function(item) {
                                    return item.nid;
                                }).value(),
                            staff_in: _.chain(profile.field_staff_in_mms)
                                .filter(function(item) {
                                    return (item.nid);
                                })
                                .map(function(item) {
                                    return item.nid;
                                }).value(),
                            teacher_in: teacher_in,
                            student_in: student_in,
                            books_owned: _.chain(profile.field_books_owned)
                                .filter(function(item) {
                                    return (item.nid);
                                })
                                .map(function(item) {
                                    return item.nid;
                                }).value(),
                            bdaycard: _.chain(profile.field_bdaycard)
                                .filter(function(item) {
                                    return (item.value);
                                })
                                .map(function(item) {
                                    return item.value;
                                }).value(),
                            locations: _.chain(profile.locations)
                                .filter(function(item) {
                                    return (item);
                                }).value()
                        };
                        if (_.isArray(contracts) && contracts.length > 0) {
                            ret.contracts = _(contracts).map(function(contract) {

                                var contractFranchise =
                                    (contract.field_lesson_franchise[0] && contract.field_lesson_franchise[0].value) ? [contract.field_lesson_franchise[0].value] : [];

                                var contractObj = {
                                    title: contract.title,
                                    nid: contract.nid,
                                    email: (contract.field_email) ? contract.field_email[0].value : null,
                                    contract_status: (contract.field_contract_status) ? contract.field_contract_status[0].value : null,
                                    teacher: (contract.field_contract_teacher) ? contract.field_contract_teacher[0].nid : null,
                                    dob: (contract.field_dob) ? contract.field_dob[0].value : null,
                                    mobile: (contract.field_mobile) ? contract.field_mobile[0].value : null,
                                    telephone: (contract.field_telephone) ? contract.field_telephone[0].value : null,
                                    gender: (contract.field_gender) ? contract.field_gender[0].value : null,
                                    created: contract.created,
                                    instrument_nids: _.chain(contract.field_contract_instruments)
                                        .filter(function(item) {
                                            return (item.nid);
                                        })
                                        .map(function(item) {
                                            return item.nid;
                                        }).value(),
                                    instruments: _.chain(contract.instrument_nodes)
                                        .filter(function(item) {
                                            return (item.nid);
                                        })
                                        .map(function(item) {
                                            return {
                                                nid : item.nid,
                                                title : item.title
                                            };
                                        }).value(),                                                            
                                    franchise: _.chain(contract.field_lesson_franchise)
                                        .filter(function(item) {
                                            return (item.value);
                                        })
                                        .map(function(item) {
                                            return item.value;
                                        }).value(),
                                    type: contract.type,
                                };
                                return contractObj;
                            });
                        }
                        console.log('ret', ret);
                        return ret;

                    });

                    var withoutUnpublishedProspects = _.filter(mapped, function(item) {
                        if (item.type === 'prospect_student' && !(item.prospect_student_status) && item.published_status === '0') {
                            return false;
                        }
                        return true;
                    });

                    self.reset(withoutUnpublishedProspects);

                } catch (err) {
                    console.log('mapping error', err);
                }

                if (_.isFunction(cb)) {
                    cb.call(self, error, self);
                }

            } else {
                console.log('Directories update error ', error, body);
            }
        });
    },
    mapProspectData: function(node, user, data) {

        var nodeEmail = (node.field_email && node.field_email[0].value !== null) ? node.field_email[0].value : user.email;
        var prospectStatuses = {
            /*
                0|New
                1|Trial
                2|Follow up
                3|Signed
                4|Archived - Not interested
                5|Archived - Not satisfied
                6|Archived - Too Expensive
                7|Archived - Personal reasons
            */
            '0': 'New',
            '1': 'Trial',
            '2': 'Follow up',
            '3': 'Signed',
            '4': 'Archived - Not interested',
            '5': 'Archived - Not satisfied',
            '6': 'Archived - Too Expensive',
            '7': 'Archived - Personal reasons'
        };

        var ret = {
            nid: node.nid,
            type: node.type,
            uid: node.uid,
            title: node.title,
            fname: (node.field_fname) ? node.field_fname[0].value : null,
            lname: (node.field_lname) ? node.field_lname[0].value : null,
            email: nodeEmail,
            gender: (node.field_gender) ? node.field_gender[0].value : null,
            mobile: (node.field_mobile) ? node.field_mobile[0].value : null,
            telephone: (node.field_telephone) ? node.field_telephone[0].value : null,
            dob: (node.field_dob) ? node.field_dob[0].value : null,
            over_18: (node.field_over_18) ? node.field_over_18[0].value : null,
            prospect_student_status: (node.field_prospect_student_status) ? prospectStatuses[node.field_prospect_student_status[0].value] : null,
            guardian_email: (node.field_guardian_email) ? node.field_guardian_email[0].value : null,
            guardian_fname: (node.field_guardian_first) ? node.field_guardian_first[0].value : null,
            guardian_lname: (node.field_guardian_last) ? node.field_guardian_last[0].value : null,
            created: node.created,
            published_status: node.status,
            teacher_request: _.chain(node.field_prospect_teacher_request)
                .filter(function(item) {
                    return (item.nid);
                })
                .map(function(item) {
                    return item.nid;
                }).value(),
            user_type: _.chain(node.field_user_type)
                .filter(function(item) {
                    return (item.value);
                })
                .map(function(item) {
                    return item.value;
                }).value(),
            related_franchises: _.chain(node.field_related_franchises)
                .filter(function(item) {
                    return (item.nid);
                })
                .map(function(item) {
                    return item.nid;
                }).value(),
            staff_in: [],
            teacher_in: [],
            student_in: [],
            books_owned: [],
            bdaycard: [],
            locations: _.chain(node.locations)
                .filter(function(item) {
                    return (item);
                }).value()
        };

        // map locations for prospect
        // prospect location is in separate field,
        // so map location data as profile node's location data
        ret.locations = {
            additional: (node.field_prospect_address_addtional) ? node.field_prospect_address_addtional[0].value : null,
            city: (node.field_prospect_city) ? node.field_prospect_city[0].value : null,
            country: "",
            country_name: "",
            latitude: "",
            longitude: "",
            postal_code: (node.field_prospect_postal) ? node.field_prospect_postal[0].value : null,
            province: "",
            province_name: "",
            street: (node.field_prospect_street) ? node.field_prospect_street[0].value : ''
        };
        ret.locations.street += ' ' + (node.field_prospect_street_no) ? node.field_prospect_street_no[0].value : '';

        return ret;
    },

    update: function(params, cb) {
        var self = this;
        this.fetchItems(params, function(err) {
            self.save(cb);
        });
    },
    fetch: function(params, cb) {
        var self = this;
        db.directories.find(params, function(err, data) {
            if (!err) {
                self.reset(data);
            }
            if (_.isFunction(cb)) cb.call(self, err, data);
        });
    },
    destroy: function(params, cb) {
        var self = this;
        db.directories.remove(params, function(err) {
            // if (!err) {
            //     self.reset(data);
            // }
            if (_.isFunction(cb)) cb.call(self, err);
        });
    },
    save: function(cb) {
        var self = this;
        async.each(self.models, function(item, callback) {
            item.save(callback);
        }, cb);
    }
});

module.exports = Directories;