var request = require('request');
var _ = require('underscore');
var Backbone = require('backbone');
var async = require('async');

var School = require('../models/School');
var db = require('../db');
/**
 * School Collection
 *
 */

var BillingItems = Backbone.Collection.extend({
    model: require('../models/BillingItem'),
    /**
     * Fetch billing items from drupal
     *
     * @param  {[type]}   params [description]
     * @param  {Function} cb     [description]
     * @return {[type]}          [description]
     */
    fetchBillingItems: function(params, cb) {
        var self = this;

        request({
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            uri: 'https://www.db-mms.com/erp/server/api',
            form: {
                callback: 'erp_drupal_view',
                view: 'erp_api_accounting',
                arguments: params
            }
        }, function(error, response, body) {

            if (!error && response.statusCode == 200) {

                try {
                    var billingItems = JSON.parse(body);
                    // console.log('params',params);
                    // console.log(billingItems);
                    // console.log('');
                    var formatted = _.map(billingItems.nodes,function(node){

                        node.node.field_contract_bill_amount = node.node.field_contract_bill_amount || 0;
                        node.node.field_contract_bill_balance = node.node.field_contract_bill_balance || 0;

                        return node.node;
                    });
                    self.reset(formatted);

                } catch (err) {

                }

            }

            if (_.isFunction(cb)) {
                cb.call(self, error, self);
            }
        });
    },

    updateBillingItems: function(params, cb) {
        var self = this;
        this.fetchBillingItems(params, function(err) {
            self.save(cb);
        });

    },
    fetch: function(params, cb) {
        var self = this;
        db.accounting.find(params, function(err, data) {
            if (!err) {
                self.reset(data);
            }
            if (_.isFunction(cb)) cb.call(self, err, data);
        });
    },
    save: function(cb) {
        var self = this;
        async.each(self.models, function(billing, callback) {
            billing.save(callback);
        }, cb);
    }
});

module.exports = BillingItems;


