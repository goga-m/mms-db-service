var request = require('request');
var _ = require('underscore');
var Backbone = require('backbone');
var async = require('async');

var server = require('./https-server');
var Schools = require('./collections/Schools');
var db = require('./db');


/**
 * Get erp school contracts..
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */
server.get('/erp_api_school_contracts', function(req, res) {

    res.header('Access-Control-Allow-Origin', 'https://www.db-mms.com');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.setHeader('Content-Type', 'application/json');

    if (req.param('mms_nid')) {
        var school = mmsSchools.get(req.param('mms_nid'));
        if (school) {
            res.end(JSON.stringify(school.contracts.toJSON()));

        } else {
            res.end('[]');
        }
    } else {
        // res.header('Access-Control-Allow-Origin', '*');
        // res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
        // res.header('Access-Control-Allow-Headers', 'Csontent-Type');
        // res.setHeader('Content-Type', 'application/json');        
        var allContracts = mmsSchools.map(function(school) {
            return school.contracts.toJSON()
        })
        console.log('mmsSchools', JSON.stringify(allContracts))
        console.log('erp_api_school_contracts for all ')
        res.end(JSON.stringify(allContracts));
    }
});

/**
 * Refresh contracts collection from mms db
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */
server.get('/refresh', function(req, res) {

    // return true to the server to continue
    res.end('true');

    if (req.param('mms_nid')) {
        var school = mmsSchools.get(req.param('mms_nid'));
        if (school) {
            console.log('Refresh request for', school.id);
            // school.getContractNodes();
            school.getContracts(function() {
                console.log('Processed ', school.get('title'), school.get('nid'));
                res.end('finished updating school contracts for ' + school.get('title'));
            });
        }
        // res.end('true');

    }
});

/**
 * Get statistics for school
 * @param  {[type]} req [description]
 * @param  {[type]} res [description]
 * @return {[type]}     [description]
 */
server.get('/erp_api_school_statistics', function(req, res) {

    // output
    res.header('Access-Control-Allow-Origin', 'https://www.db-mms.com');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.setHeader('Content-Type', 'application/json');



    var offset = req.param('offset') || 0;

    // If an offset value is provided return an array of statistics
    // withs snapshotDates specified by offset.

    var dates = [req.param('snapshotDate')];
    var last = Date.parseExact(req.param('snapshotDate'), 'yyyyMMdd');
    for (var i = 0; i < offset; i++) {
        var d = last.moveToFirstDayOfMonth().add(-1).months();
        dates.push(d.toString("yyyyMM dd").replace(' ', ''));
    }

    var output = [];

    async.each(dates, function(ymd, callback) {

            if (req.param('mms_nid')) {

                var school = mmsSchools.get(req.param('mms_nid'));
                school.getStatistics(ymd, function(result) {
                    output.push(result[0]);
                    callback(null);
                }, function(error) {
                    console.log('error');
                    callback(error);
                });

            } else {
                // get statistics for all schools
                mmsSchools.getStatistics(ymd, function(err, stats) {
                    console.log('stats',JSON.stringify(err),JSON.stringify(stats));
                    output = stats;
                    callback(err);
                })
            }
        },
    function(err) {
        // console.log('get statistics for', dates);
        if (err) {
            res.end(error);
        } else {
            res.end(JSON.stringify(output));
        }
    });



});

server.get('/is_ready', function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.end('' + ready);
});


server.get('/erp_api_benchmarks', function(req, res) {


    res.header('Access-Control-Allow-Origin', 'https://www.db-mms.com');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.setHeader('Content-Type', 'application/json');

    var currentDate = new Date().moveToFirstDayOfMonth();
    var snapshotDate = req.param('snapshotDate') || currentDate.toString("yyyyMM dd").replace(' ', '');

    db.benchmarks.find({
        'snapshotDate': snapshotDate
    }, function(err, data) {
        res.end(JSON.stringify(data));
    });

});
server.get('/erp_api_accounting', function(req, res) {


    res.header('Access-Control-Allow-Origin', 'https://www.db-mms.com');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.setHeader('Content-Type', 'application/json');

    var BillingItems = require('./collections/BillingItems');
    var accounting = new BillingItems();

    accounting.fetch(req.query, function(err, data) {
        res.end(JSON.stringify(accounting.toJSON()));
    });

});


server.get('/refresh_accounting', function(req, res) {

    console.log('Request to create/update billing ', req.param('nid'), 'for ', req.param('mms_nid'));

    if (req.param('mms_nid') && req.param('nid')) {
        var BillingItems = require('./collections/BillingItems');
        var accounting = new BillingItems();

        accounting.updateBillingItems({
            mms_nid: req.param('mms_nid'),
            nids: req.param('nid')
                // field_contract_bill_ref: nid
        }, function(err, data) {
            console.log(err, 'Updated billing item:', req.param('nid'), 'for ', req.param('mms_nid'));
        });
    }

    res.end('true');

});

server.get('/update_accounting', function(req, res) {

    res.header('Access-Control-Allow-Origin', 'https://www.db-mms.com');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.setHeader('Content-Type', 'application/json');

    if (req.param('mms_nid')) {
        var accounting = require('./modules/accounting.js');
        accounting.updateAccounting(req.param('mms_nid'), function(errors) {

            console.log('*************')
            console.log('Updated accounting for ', req.param('mms_nid'), errors);
            console.log('*************')
            res.end('true');
        })
    } else {
        res.end('false');
    }
});
server.get('/delete_accounting', function(req, res) {

    console.log('Request to delete billing ', req.param('nid'), 'for ', req.param('mms_nid'));

    if (req.param('mms_nid') && req.param('nid')) {

        var BillingItem = require('./models/BillingItem');
        var billingItem = new BillingItem({
            nid: req.param('nid')
        });

        billingItem.destroy(function(err) {

            if (err) {
                console.log('error deleting billing item ', req.param.nid, 'for ', req.param('mms_nid'), err);
            } else {
                console.log('Successfully deleted billing item', req.param('nid'));
            }

        });
    }

    res.end('true');
});

/**
 * directories
 */
server.get('/erp_api_directories', function(req, res) {


    res.header('Access-Control-Allow-Origin', 'https://www.db-mms.com');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.setHeader('Content-Type', 'application/json');

    console.log('Request for directories');
    console.log(req.query);
    var where = req.query['$where'];
    var limit = (!_.isNaN(parseInt(req.query.limit, 10))) ? parseInt(req.query.limit, 10) : null;
    db.directories.find({
        '$where': where
    }, function(err, data) {
        res.end(JSON.stringify(data));
    }).limit(limit);

});

server.get('/erp_api_query_directories', function(req, res) {


    res.header('Access-Control-Allow-Origin', 'https://www.db-mms.com');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.setHeader('Content-Type', 'application/json');

    console.log('Request for directories');
    console.log(req.query);
    var limit = (!_.isNaN(parseInt(req.query.limit, 10))) ? parseInt(req.query.limit, 10) : null;
    db.directories.find({
        '$where': where
    }, function(err, data) {
        res.end(JSON.stringify(data));
    }).limit(limit);

});

server.get('/refresh_directory', function(req, res) {


    console.log('Request to create/update directory ', req.param('nid'));

    if (req.param('nid')) {

        var Directories = require('./collections/Directories');
        var directories = new Directories();

        directories.update({
            nid: req.param('nid'),
        }, function(err, data) {
            if (err) return console.log(err);
            console.log('Updated directory for profile:', req.param('nid'), data);
        });
    }
    res.end('true');
});

server.get('/delete_directory', function(req, res) {


    console.log('Request to delete directory ', req.param('nid'));

    if (req.param('nid')) {

        var Directories = require('./collections/Directories');
        var directories = new Directories();

        directories.destroy({
            nid: req.param('nid'),
        }, function(err) {
            console.log('Deleted directory for profile:', req.param('nid'), err);
        });
    }
    res.end('true');
});

/*Flow*/

/* 1. Fetch school nodes*/
/* 2. Fetch contracts for all schools*/
/* 3. Fetch old statistics from db*/
/* 4. Update current month statistics for all schools*/
/* 5. Cron job on every day to update current days statistics */

var mmsSchools = new Schools();
var ready = false;

async.series([

    function(callback) {
        /* 1. Fetch school nodes*/
        console.log('getting school nodes');
        mmsSchools.getSchoolNodes(function(schools) {
            console.log('fetched school Nodes', schools.length);
            callback();
        });
    },
    function(callback) {
        /* 2. Fetch contracts for all schools*/
        console.log('start cacheContracts');
        mmsSchools.cacheContracts(function() {
            console.log('end cacheContracts');
            callback();
        });
    },
    // function(callback) {
    //     /* 3. Fetch old statistics from mms mysql db */
    //     console.log('start fetchAllStatistics');
    //     mmsSchools.fetchAllStatistics(function() {
    //         console.log('end fetchAllStatistics');
    //         callback();
    //     });
    // },
    // function(callback) {
    //     /* 4. Update current month statistics for all schools*/
    //     console.log('start calculateAllCurrentStatistics');
    //     mmsSchools.calculateAllCurrentStatistics(function(err, results) {
    //         console.log('end calculateAllCurrentStatistics');
    //         callback();
    //     });
    // },
], function(err, results) {

    console.log('*********************');
    console.log('Finished Initialization');
    console.log('*********************');
    ready = true;
    // end.
});
